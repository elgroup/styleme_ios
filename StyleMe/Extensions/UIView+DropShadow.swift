//
//  UIView+DropShadow.swift
//  StyleMe
//
//  Created by EL INFO on 18/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

extension UIView {
    func dropShadow(scale: Bool = true, radius:CGFloat, opacity:Float) {
       layer.masksToBounds = false
       layer.shadowColor = UIColor.black.cgColor
       layer.shadowOpacity = opacity
       layer.shadowOffset = .zero
       layer.shadowRadius = radius
//       layer.shouldRasterize = true
//       layer.rasterizationScale = scale ? UIScreen.main.scale : 1
   }
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
       let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
       let mask = CAShapeLayer()
       mask.path = path.cgPath
       layer.mask = mask
   }
}
