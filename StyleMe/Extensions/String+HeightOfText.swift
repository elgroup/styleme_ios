//
//  String+HeightOfText.swift
//  StyleMe
//
//  Created by EL INFO on 18/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
//MARK: Height of Text
extension String {
func height(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
    let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.text = self
    label.font = font
    label.sizeToFit()

    return label.frame.height
 }
}
