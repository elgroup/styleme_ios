//
//  CategoryListTableViewController.swift
//  StyleMe
//
//  Created by EL INFO on 04/02/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import SDWebImage
protocol CategorySelectedDelegate{
    func getSelectedCategory(category:[Any])
}
class CategoryListTableViewController: UITableViewController {
    var activityIndicator:ActivityIndicator = ActivityIndicator()
    var categoryArray:[Any] = [Any]()
    var selectedIndexPath:IndexPath!
    var delegate:CategorySelectedDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.customNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.apiForCategory()
        selectedIndexPath = IndexPath.init(row: 100001, section: 0)
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CategoryListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CategoryListTableViewCell", for: indexPath) as! CategoryListTableViewCell
        let dict:[String:Any] = categoryArray[indexPath.row] as! [String:Any]
        let name:String = dict["name"] as! String
        cell.labelName.text = name
        let imageUrldict:[String:Any] = dict["image"] as! [String:Any]
        var imageUrlStr:String = imageUrldict["url"] as? String ?? ""
        imageUrlStr = Constants.apiConstants.baseUrlImage + imageUrlStr
        let imageURl:URL = URL.init(string: imageUrlStr)!
        cell.imageViewCategory!.sd_setImage(with: imageURl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
            if (cacheType == SDImageCacheType.none && image != nil) {
                cell.imageViewCategory.alpha = 0;
                UIView.animate(withDuration: 0.0, animations: { () -> Void in
                    cell.imageViewCategory.alpha = 1
                })
            } else {
                cell.imageViewCategory.alpha = 1;
            }
        })
        cell.buttonCheckBox.tag = indexPath.row
        cell.buttonCheckBox.addTarget(self, action: #selector(buttonCheckBoxClicked(_:)), for: .touchUpInside)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.getCell(index: indexPath.row)
        var dict:[String:Any] = categoryArray[indexPath.row] as! [String:Any]
        if dict["isSelected"] as! Bool == false{
            dict["isSelected"] = true
            categoryArray.remove(at: indexPath.row)
            categoryArray.insert(dict, at: indexPath.row)
            cell?.buttonCheckBox.setImage(UIImage.init(named: "check-box-tick"), for: .normal)
        }
        else{
            dict["isSelected"] = false
            categoryArray.remove(at: indexPath.row)
            categoryArray.insert(dict, at: indexPath.row)
            cell?.buttonCheckBox.setImage(UIImage.init(named: "check-box"), for: .normal)
        }
    }
}

extension CategoryListTableViewController{
    
    //MARK: Custom Navigation Bar
    
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.navigationController?.navigationItem.hidesBackButton = true
        
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        backButton.setImage(UIImage.init(named: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(CategoryListTableViewController.backButtonPressed(_sender:)), for: .touchUpInside)
        let leftItem = UIBarButtonItem()
        leftItem.customView = backButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "Type of Business"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem, fixedSpace,leftItem2]
        
        let nextButton = UIButton.init()
        nextButton.setTitle("DONE", for: .normal)
        nextButton.setTitleColor(.red, for: .normal)
        nextButton.titleLabel?.font = UIFont.PoppinsSemiBold(ofSize: 15)
        nextButton.addTarget(self, action: #selector(CategoryListTableViewController.nextButtonPressed(_:)), for: .touchUpInside)
        let rightItem = UIBarButtonItem()
        rightItem.customView = nextButton
        
        self.navigationItem.rightBarButtonItem = rightItem
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    //MARK: Check Box Button action
    
    @objc func buttonCheckBoxClicked(_ sender:Any){
        let tag:Int = (sender as AnyObject).tag
        let index:IndexPath = IndexPath.init(row: tag, section: 0)
        self.tableView(self.tableView, didSelectRowAt: index)
        
    }
    //MARK: Back Button Action
    
    @objc func backButtonPressed(_sender :Any){
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK: Done Button Action
    
    @objc func nextButtonPressed(_ sender:Any){
        let predicate:NSPredicate =  NSPredicate(format: "isSelected = %d", true)
        let array:NSArray = (categoryArray as NSArray).filtered(using: predicate) as NSArray
        
        if array.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Sub Category must be selected to continue", Controller: self, Time: 1)
        }
        else{
            self.delegate.getSelectedCategory(category: array as! [Any])
            self.navigationController?.navigationBar.isHidden = true
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func apiForCategory(){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            self.activityIndicator.showActivityIndicator(uiView: self.view)
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            APIManager.getRequestForURLString(true, Constants.URLConstants.getCategories, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if Success{
                        let resultDict = result as! [String:Any]
                        print(resultDict)
                        self.categoryArray = [Any]()
                        let arr = resultDict["categories"] as! [Any]
                        for n in arr {
                            var dict:[String:Any] = n as! [String:Any]
                            dict["isSelected"] = false
                            self.categoryArray.append(dict)
                        }
                        self.tableView.reloadData()
                    }
                    else{
                        AppCustomClass.showToastAlert(Title:"", Message:"No Address found.", Controller: self, Time: 1)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    
    //MARK: Getting TableView Cells
    func getCell(index: Int) -> CategoryListTableViewCell? {
        guard let cell = self.tableView.cellForRow(at: IndexPath(row : index, section : 0)) as? CategoryListTableViewCell else {return nil}
        return cell
    }
}

