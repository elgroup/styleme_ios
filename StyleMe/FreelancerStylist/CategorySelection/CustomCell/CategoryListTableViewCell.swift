//
//  CategoryListTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 05/02/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class CategoryListTableViewCell: UITableViewCell {

    @IBOutlet weak var labelName:UILabel!
    @IBOutlet weak var imageViewCategory:UIImageView!
    @IBOutlet weak var buttonCheckBox:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
