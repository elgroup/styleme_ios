//
//  AddFreelancerTableViewController.swift
//  StyleMe
//
//  Created by EL INFO on 30/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import SDWebImage


class AddFreelancerTableViewController: UITableViewController {
    
    
    @IBOutlet weak var headerSubCategory:UIView!
    @IBOutlet weak var headerViewProfile:UIView!
    @IBOutlet weak var headerAddHours:UIView!
    @IBOutlet weak var imageViewStylist: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelStylistName: UILabel!
    @IBOutlet weak var labelEmailId: UILabel!
    @IBOutlet weak var labelPhoneNumber: UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var headerViewCategory:UIView!
    @IBOutlet weak var backButton:UIButton!
    var activityIndicator:ActivityIndicator = ActivityIndicator()
    var selectedRowIndexForAddingHours:NSIndexPath = NSIndexPath.init(row: -1, section: 3)
    var thereIsCellTapped = false
    var arrSubCategory:[Any]!
    var dictProfile:[String:Any]!
    private var selectedCategory:[Any]!
    var arrayWorkingHours:[Any]!
    var day = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    
    //MARK: View Controllers method
    override func viewDidLoad() {
        super.viewDidLoad()
        labelEmailId.text = self.dictProfile["email"] as? String
        let countryCode:Int = Int(dictProfile["mobile_code"] as! String)!
        let number:String = dictProfile["mobile_number"] as! String
        self.labelStylistName.text = dictProfile["user_name"] as? String
        self.labelEmailId.text = dictProfile["email"] as? String
        self.labelPhoneNumber.text = "+\(countryCode) " + number
        
        let imageUrl:String = (self.dictProfile["profile_image"] as? String)!
        let profileImageUrl:URL = URL.init(string: imageUrl)!
        self.imageViewStylist.sd_setImage(with: profileImageUrl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
            if (cacheType == SDImageCacheType.none && image != nil) {
                self.imageViewStylist.alpha = 0;
                UIView.animate(withDuration: 0.0, animations: { () -> Void in
                    self.imageViewStylist.alpha = 1
                })
            } else {
                self.imageViewStylist.alpha = 1;
            }
        })
        self.buttonEdit.isHidden = true
        let date = NSDate(timeIntervalSince1970: 0)
        print(date)
        arrayWorkingHours = [Any]()
        for n in 0...6{
            var dict:[String:Any] = [String:Any]()
            dict["isSelected"] = false
            dict["startTime"] = date
            dict["endTime"] = date
            dict["day"] = day[n]
            dict["isFullDay"] = false
            dict["isClosed"] = true
            arrayWorkingHours.append(dict)
        }
        
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.containerView.layer.cornerRadius = self.containerView.frame.size.width/2
        self.containerView.clipsToBounds = true
        self.imageViewStylist.layer.cornerRadius = self.imageViewStylist.frame.size.width/2
        self.imageViewStylist.clipsToBounds = true
        
    }
    
}

// MARK: - Extension Table view data source and dalegate

extension AddFreelancerTableViewController{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            return headerViewProfile
        }
        else if section == 1{
            
            self.headerViewCategory.layer.cornerRadius = 5.0
            self.headerViewCategory.clipsToBounds = true
            return headerViewCategory
        }
        else if section == 2{
            self.headerSubCategory.layer.cornerRadius = 5.0
            self.headerSubCategory.clipsToBounds = true
            return headerSubCategory
        }
        else if section == 3{
            self.headerAddHours.layer.cornerRadius = 5.0
            self.headerAddHours.clipsToBounds = true
            return headerAddHours
        }
        else{
            let header:UIView = UIView.init()
            return header
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 199
        }
        else if section == 1 || section == 2 || section == 3{
            return 60
        }
        else{
            return 0
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 0
        }
        else if section == 1{
            if selectedCategory != nil{
                return selectedCategory.count
            }
            else{
                return 1
            }
        }
        else if section == 2{
            if arrSubCategory != nil{
                return arrSubCategory.count
            }
            else{
                return 1
            }
        }
        else if section == 3{
            return 7
        }
        else if section == 4{
            return 1
        }
        else{
            return 0
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 3{
            //            return 140
            if indexPath.row == selectedRowIndexForAddingHours.row && thereIsCellTapped{
                return 140
            }
            else{
                return 50
            }
        }
        else{
            return 60
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1{
            let cell:CategorySelectionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CategorySelectionTableViewCell", for: indexPath) as! CategorySelectionTableViewCell
            cell.textFieldCategoryName.tag = indexPath.section
            if selectedCategory != nil{
                let dict:[String:Any] = selectedCategory[indexPath.row] as! [String:Any]
                let name:String = dict["name"] as! String
                cell.textFieldCategoryName.text = name
            }else{
                cell.textFieldCategoryName.placeholder = "Select Business"
            }
            cell.delegate = self
            return cell
        }
        else if indexPath.section == 2{
            let cell:CategorySelectionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CategorySelectionTableViewCell", for: indexPath) as! CategorySelectionTableViewCell
            cell.textFieldCategoryName.tag = indexPath.section
            if arrSubCategory != nil{
                let dict:[String:Any] = self.arrSubCategory[indexPath.row] as! [String:Any]
                let name:String = dict["name"] as! String
                cell.textFieldCategoryName.text = name
            }
            else{
                cell.textFieldCategoryName.placeholder = "Select Services"
            }
            cell.delegate = self
            return cell
            
        }
        else if indexPath.section == 3{
            let cell:AddHoursTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AddHoursTableViewCell", for: indexPath) as! AddHoursTableViewCell
            cell.labelDay.text = day[indexPath.row]
            cell.buttonTick.tag = indexPath.row
            cell.buttonTick.addTarget(self, action: #selector(buttonCheckBoxPressed(_:)), for: .touchUpInside)
            cell.buttonTick.setTitle(day[indexPath.row], for: .normal)
            cell.button24HoursCheck.tag = indexPath.row
            cell.button24HoursCheck.addTarget(self, action: #selector(button24HoursPressed(_:)), for: .touchUpInside)
            cell.textFieldTo.tag = indexPath.row
            cell.textFieldFrom.tag = indexPath.row
            cell.delegate = self
            return cell
        }
        else if indexPath.section == 4{
            let cell:AddStylistTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AddStylistTableViewCell", for: indexPath) as! AddStylistTableViewCell
            cell.buttonAddStylist.addTarget(self, action: #selector(AddFreelancerTableViewController.addStylist(_:)), for:.touchUpInside )
            return cell
        }
        else{
            return UITableViewCell.init()
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 3{
            if selectedRowIndexForAddingHours.row != indexPath.row {
                // avoid paint the cell is the index is outside the bounds
                if self.selectedRowIndexForAddingHours.row != -1 {
                    self.tableView.cellForRow(at: self.selectedRowIndexForAddingHours as IndexPath)
                    let selectedCell = self.getAddHoursCell(index: self.selectedRowIndexForAddingHours as IndexPath)
                    
                    let dictSelected:[String:Any] = arrayWorkingHours[selectedRowIndexForAddingHours.row] as! [String:Any]
                    let isFullDay:Bool = dictSelected["isFullDay"] as! Bool
                    if selectedCell?.textFieldFrom.text == "" && !isFullDay {
                        selectedCell?.buttonTick.setImage(UIImage.init(named: "check-box"), for: .normal)
                        selectedCell?.labelClosed.attributedText = self.withoutStrikeThroughText(string: (selectedCell?.labelClosed.text)!)
                        self.selectedRowIndexForAddingHours = NSIndexPath(row: -1, section: 3)
                    }
                }
                if selectedRowIndexForAddingHours.row != indexPath.row {
                    self.thereIsCellTapped = true
                    self.selectedRowIndexForAddingHours = NSIndexPath.init(row: indexPath.row, section: 3)
                    let selectedCell = self.getAddHoursCell(index: self.selectedRowIndexForAddingHours as IndexPath)
                    selectedCell?.buttonTick.setImage(UIImage.init(named: "check-box-tick"), for: .normal)
                    selectedCell?.labelClosed.attributedText = self.withStrikeThroughText(attrStr: (selectedCell?.labelClosed.text)!)
                    var dict:[String:Any] = arrayWorkingHours[self.selectedRowIndexForAddingHours.row] as! [String:Any]
                    dict["isClosed"] = false
                    arrayWorkingHours.remove(at: self.selectedRowIndexForAddingHours.row)
                    arrayWorkingHours.insert(dict, at: self.selectedRowIndexForAddingHours.row)
                }
                else {
                    // there is no cell selected anymore
                    self.thereIsCellTapped = false
                    self.selectedRowIndexForAddingHours = NSIndexPath(row: -1, section: 3)
                }
                // update the height for all the cells
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
            }
            else{
                self.thereIsCellTapped = false
                let selectedCell = self.getAddHoursCell(index: self.selectedRowIndexForAddingHours as IndexPath)
                var dict:[String:Any] = arrayWorkingHours[indexPath.row] as! [String:Any]
                let isFullDay = dict["isFullDay"] as! Bool
                if selectedCell?.textFieldFrom.text == "" &&  !isFullDay{
                    selectedCell?.buttonTick.setImage(UIImage.init(named: "check-box"), for: .normal)
                    selectedCell?.labelClosed.attributedText = self.withoutStrikeThroughText(string: (selectedCell?.labelClosed.text)!)
                    self.selectedRowIndexForAddingHours = NSIndexPath(row: -1, section: 3)
                }
                else{
                    self.selectedRowIndexForAddingHours = NSIndexPath(row: -1, section: 3)
                }
                dict["isClosed"] = true
                arrayWorkingHours.remove(at: indexPath.row)
                arrayWorkingHours.insert(dict, at: indexPath.row)
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
            }
        }
    }
}


extension AddFreelancerTableViewController{
    @IBAction func backButtonPressed(){
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Getting TableView Cells
    func getAddHoursCell(index: IndexPath) -> AddHoursTableViewCell? {
        guard let cell = self.tableView.cellForRow(at: IndexPath(row : index.row, section : index.section)) as? AddHoursTableViewCell else {return nil}
        return cell
    }
    func withoutStrikeThroughText(string:String) -> NSAttributedString {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: string)
        attributeString.addAttribute(NSAttributedString.Key.kern, value: 0, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
    func withStrikeThroughText(attrStr:String) -> NSAttributedString{
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: attrStr)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
    @objc func buttonCheckBoxPressed(_ sender: Any){
        let button:UIButton = sender as! UIButton
        let tag = button.tag
        print(button.isSelected)
        print(tag)
        
        let dict:[String:Any] = arrayWorkingHours[tag] as! [String:Any]
        let isClosed:Bool = dict["isClosed"] as! Bool
        if isClosed{
            //            selectedRowIndexForAddingHours = NSIndexPath.init(row: -1, section: 3)
            tableView(self.tableView, didSelectRowAt: NSIndexPath.init(row: tag, section: 3) as IndexPath)
        }
        else{
            let date = NSDate(timeIntervalSince1970: 0)
            var dict:[String:Any] = [String:Any]()
            dict["isSelected"] = false
            dict["startTime"] = date
            dict["endTime"] = date
            dict["day"] = day[tag]
            dict["isFullDay"] = false
            dict["isClosed"] = true
            
            arrayWorkingHours.remove(at: tag)
            arrayWorkingHours.insert(dict, at: tag)
            let cell = self.getAddHoursCell(index: NSIndexPath.init(row: tag, section: 3) as IndexPath)
            cell?.buttonTick.setImage(UIImage.init(named: "check-box"), for: .normal)
            cell?.textFieldTo.text = ""
            cell?.textFieldFrom.text = ""
            cell?.button24HoursCheck.setImage(UIImage.init(named: "check-box"), for: .normal)
            cell?.labelClosed.attributedText = self.withoutStrikeThroughText(string:((cell?.labelClosed.text)!) )
            tableView(self.tableView, didSelectRowAt: NSIndexPath.init(row: tag, section: 3) as IndexPath)
        }
    }
    
    @objc func button24HoursPressed(_ sender: Any){
        let button:UIButton = sender as! UIButton
        let tag = button.tag
        print(button.isSelected)
        print(tag)
        var dictHours:[String:Any] = arrayWorkingHours[tag] as! [String:Any]
        let isFullDay:Bool = dictHours["isFullDay"] as! Bool
        let selectedCell = getAddHoursCell(index: NSIndexPath.init(row: tag, section: 3) as IndexPath)
        selectedCell?.textFieldFrom.text = ""
        selectedCell?.textFieldTo.text = ""
        if isFullDay{
            selectedCell?.button24HoursCheck.setImage(UIImage.init(named: "check-box"), for: .normal)
            dictHours["isFullDay"] = false
            dictHours["isSelected"] = false
        }
        else{
            selectedCell?.button24HoursCheck.setImage(UIImage.init(named: "check-box-tick"), for: .normal)
            dictHours["isFullDay"] = true
            dictHours["isSelected"] = true
        }
        let date = NSDate(timeIntervalSince1970: 0)
        dictHours["startTime"] = date
        dictHours["endTime"] = date
        dictHours["day"] = day[tag]
        dictHours["isClosed"] = false
        arrayWorkingHours.remove(at: tag)
        arrayWorkingHours.insert(dictHours, at: tag)
    }
    @objc func addStylist(_ sender:Any){
        let predicate:NSPredicate =  NSPredicate(format: "isClosed = %d", true)
        let array:NSArray = (arrayWorkingHours as NSArray).filtered(using: predicate) as NSArray
        
        if array.count == 7{
            AppCustomClass.showToastAlert(Title: "", Message: "Add working hours", Controller: self, Time: 1)
            return
        }
        else if selectedCategory == nil{
            AppCustomClass.showToastAlert(Title: "", Message: "Select type of business", Controller: self, Time: 1)
        }
        else if arrSubCategory == nil{
            AppCustomClass.showToastAlert(Title: "", Message: "Select services", Controller: self, Time: 1)
        }
        else{
            var workingHours:[Any] = [Any]()
            for n in arrayWorkingHours{
                let dictHours:[String:Any] = n as! [String:Any]
                var dict:[String:Any] = [String:Any]()
                let isClosed = dictHours["isClosed"] as! Bool
                let isFullDay = dictHours["isFullDay"] as! Bool
                if isClosed{
                    dict["start_time"] = ""
                    dict["end_time"] = ""
                    dict["day"] =  dictHours["day"] as! String
                    dict["is_24hours"] = false
                    dict["is_active"] = true
                }
                else if isFullDay{
                    dict["start_time"] = ""
                    dict["end_time"] = ""
                    dict["day"] =  dictHours["day"] as! String
                    dict["is_24hours"] = true
                    dict["is_active"] = false
                }
                else{
                    dict["start_time"] = dictHours["startTime"] as! String
                    dict["end_time"] = dictHours["endTime"] as! String
                    dict["day"] =  dictHours["day"] as! String
                    dict["is_24hours"] = dictHours["isFullDay"] as! Bool
                    dict["is_active"] = dictHours["isClosed"] as! Bool
                }
                
                workingHours.append(dict)
            }
            var param:[String:Any] = [String:Any]()
            param["user_services"] = arrSubCategory
            param["service_available"] = workingHours
            self.apiForAddingStylist(param: param)
        }
    }
    func apiForAddingStylist(param:[String:Any]){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            self.activityIndicator.showActivityIndicator(uiView: self.view)
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            print(param)
            let strUrl:String = Constants.URLConstants.joinStylist
            APIManager.postRequestForURLString(true, strUrl, headerInfo, param) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if Success{
                        let resultDict:[String:Any] = result as! [String:Any]
                        AppCustomClass.showToastAlert(Title: "", Message: "Pro has been added successfully.", Controller: self, Time: 1)
                        Timer.scheduledTimer(withTimeInterval: 1.1, repeats: false) { (timer) in
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else{
                        AppCustomClass.showToastAlert(Title:"", Message: message!, Controller: self, Time: 1)
                    }
                }
            }
        }
            
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
        
    }
}

extension AddFreelancerTableViewController:categorySelectionDelegate{
    
    
    func gotoCategorySelection(selectedSection: Int) {
        if selectedSection == 1{
            let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.StylistStoryBoard, bundle: nil)
            var controller:CategoryListTableViewController!
            if #available(iOS 13.0, *) {
                controller = story.instantiateViewController(identifier: "CategoryListTableViewController") as? CategoryListTableViewController
            } else {
                controller = story.instantiateViewController(withIdentifier:"CategoryListTableViewController") as? CategoryListTableViewController
            }
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if selectedSection == 2{
            if selectedCategory != nil{
                let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.StylistStoryBoard, bundle: nil)
                var controller:SubCategorySelectionTableViewController!
                if #available(iOS 13.0, *) {
                    controller = story.instantiateViewController(identifier: "SubCategorySelectionTableViewController") as? SubCategorySelectionTableViewController
                } else {
                    controller = story.instantiateViewController(withIdentifier:"SubCategorySelectionTableViewController") as? SubCategorySelectionTableViewController
                }
                var arraySelectedId:[Any] = [Any]()
                for item in selectedCategory{
                    let dict:[String:Any] = item as! [String:Any]
                    let selectedCategoryId:Int = dict["id"] as! Int
                    arraySelectedId.append(selectedCategoryId)
                }
                
                controller.arrCategoryId = arraySelectedId
                controller.delegate = self
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else{
                AppCustomClass.showToastAlert(Title: "", Message: "Select Business then you can select the Services", Controller: self, Time: 2)
            }
        }
    }
}

extension AddFreelancerTableViewController:CategorySelectedDelegate{
    func getSelectedCategory(category: [Any]) {
        self.selectedCategory = category
        self.tableView.reloadData()
    }
    
    //    func getSelectedCategory(category:[Any]) {
    //        self.selectedCategory = category
    //        print(self.selectedCategory!)
    //        self.tableView.reloadData()
    //    }
}

extension AddFreelancerTableViewController:SubCategorySelectedDelegate{
    func selectedSubCategory(arrSubCat: [Any]) {
        self.arrSubCategory = arrSubCat
        self.tableView.reloadData()
    }
}

extension AddFreelancerTableViewController:getWorkingHoursDelegate{
    func getEndTime(date: String, textField: UITextField) {
        // let textField:UITextField = textField as! UITextField
        print(textField.tag)
        var dict:[String:Any] = arrayWorkingHours[textField.tag] as! [String:Any]
        let isFullDay:Bool = dict["isFullDay"] as! Bool
        dict["isClosed"] = false
        if isFullDay{
            dict["isFullDay"] = false
            let cell:AddHoursTableViewCell = self.getAddHoursCell(index: NSIndexPath.init(row: textField.tag, section: 3) as IndexPath)!
            cell.button24HoursCheck.setImage(UIImage.init(named: "check-box"), for: .normal)
        }
        dict["isSelected"] = true
        dict["endTime"] = date
        dict["day"] = day[textField.tag]
        
        arrayWorkingHours.remove(at: textField.tag)
        arrayWorkingHours.insert(dict, at: textField.tag)
    }
    func getStartingTime(date: String, textField: UITextField) {
        //let textField:UITextField = textField as! UITextField
        print(textField.tag)
        var dict:[String:Any] = arrayWorkingHours[textField.tag] as! [String:Any]
        dict["isSelected"] = true
        dict["startTime"] = date
        dict["day"] = day[textField.tag]
        dict["isClosed"] = false
        let isFullDay:Bool = dict["isFullDay"] as! Bool
        if isFullDay{
            dict["isFullDay"] = false
            let cell:AddHoursTableViewCell = self.getAddHoursCell(index: NSIndexPath.init(row: textField.tag, section: 3) as IndexPath)!
            cell.button24HoursCheck.setImage(UIImage.init(named: "check-box"), for: .normal)
        }
        
        arrayWorkingHours.remove(at: textField.tag)
        arrayWorkingHours.insert(dict, at: textField.tag)
    }
    
}
