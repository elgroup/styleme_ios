    //
    //  SubCategorySelectionTableViewController.swift
    //  StyleMe
    //
    //  Created by EL INFO on 07/02/20.
    //  Copyright © 2020 Admin. All rights reserved.
    //
    
    import UIKit
    import SDWebImage
    protocol SubCategorySelectedDelegate {
        func selectedSubCategory(arrSubCat:[Any])
    }
    
    class SubCategorySelectionTableViewController: UITableViewController,SelectedSubCategoryDetailsDelegate {
        
        var activityIndicator:ActivityIndicator = ActivityIndicator()
        var subCategoryArray:[Any] = [Any]()
        var arrCategoryId:[Any]!
        var selectedSubCategory:[Any]!
        var selectedSubCategoryDetails:[Any] = [Any]()
        var delegate:SubCategorySelectedDelegate!
        var selectedIndex:NSIndexPath = NSIndexPath.init(row: -1, section: 0)
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.apiForSubCategory()
        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.customNavigationBar()
            
            
        }
        // MARK: - Table view data source
        
        override func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return subCategoryArray.count
        }
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell:SubCategorySelectionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SubCategorySelectionTableViewCell", for: indexPath) as! SubCategorySelectionTableViewCell
            let dict:[String:Any] = subCategoryArray[indexPath.row] as! [String:Any]
            let name:String = dict["name"] as! String
            cell.labelName.text = name
            let isSelected = dict["isSelected"] as! Bool
            if isSelected{
                cell.buttonCheckBox.setImage(UIImage.init(named: "check-box-tick"), for: .normal)
            }
            else{
                cell.buttonCheckBox.setImage(UIImage.init(named: "check-box"), for: .normal)
            }
            let imageUrldict:[String:Any] = dict["image"] as! [String:Any]
            var imageUrlStr:String = imageUrldict["url"] as? String ?? ""
            imageUrlStr = Constants.apiConstants.baseUrlImage + imageUrlStr
            let imageURl:URL = URL.init(string: imageUrlStr)!
            cell.imageViewCategory!.sd_setImage(with: imageURl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
                if (cacheType == SDImageCacheType.none && image != nil) {
                    cell.imageViewCategory.alpha = 0;
                    UIView.animate(withDuration: 0.0, animations: { () -> Void in
                        cell.imageViewCategory.alpha = 1
                    })
                } else {
                    cell.imageViewCategory.alpha = 1;
                }
            })
            cell.buttonCheckBox.tag = indexPath.row
            cell.buttonCheckBox.addTarget(self, action: #selector(buttonCheckBoxClicked(_:)), for: .touchUpInside)
            return cell
        }
        
        override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 70
        }
        
        override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            selectedIndex = NSIndexPath.init(row: indexPath.row, section: 0)
            let dict:[String:Any] = subCategoryArray[selectedIndex.row] as! [String:Any]
            let isSelected = dict["isSelected"] as! Bool
            let id:Int = dict["id"] as! Int
            let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.StylistStoryBoard, bundle: nil)
            var controller:ServiceDetailTableViewController!
            if #available(iOS 13.0, *) {
                controller = story.instantiateViewController(identifier: "ServiceDetailTableViewController") as? ServiceDetailTableViewController
            } else {
                controller = story.instantiateViewController(withIdentifier:"ServiceDetailTableViewController") as? ServiceDetailTableViewController
            }
            if  isSelected{
                let predicate:NSPredicate =  NSPredicate(format: "sub_category_id = %d", id)
                let array:NSArray = (selectedSubCategoryDetails as NSArray).filtered(using: predicate) as NSArray
                controller.dictDetails = array[0] as? [String:Any]
                controller.isSelected = true
            }
            else{
                controller.isSelected = false
            }
            controller.selectedSubCategory = dict
            controller.detailsDelegate = self
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    extension SubCategorySelectionTableViewController{
        
        //MARK: Custom Navigation Bar
        
        func customNavigationBar(){
            self.navigationController?.navigationBar.isHidden = false
            self.navigationItem.setHidesBackButton(true, animated: true);
            self.navigationController?.navigationBar.barTintColor = .white
            let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
            fixedSpace.width = 20.0
            
            let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            backButton.setImage(UIImage.init(named: "back"), for: .normal)
            backButton.addTarget(self, action: #selector(SubCategorySelectionTableViewController.backButtonPressed(_sender:)), for: .touchUpInside)
            let leftItem = UIBarButtonItem()
            leftItem.customView = backButton
            
            let labelTitle = UILabel()
            labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
            labelTitle.text = "Services"
            let leftItem2 = UIBarButtonItem()
            leftItem2.customView = labelTitle
            self.navigationItem.leftBarButtonItems = [leftItem, fixedSpace,leftItem2]
            
            let nextButton = UIButton.init()
            nextButton.setTitle("DONE", for: .normal)
            nextButton.setTitleColor(.red, for: .normal)
            nextButton.titleLabel?.font = UIFont.PoppinsSemiBold(ofSize: 15)
            nextButton.addTarget(self, action: #selector(SubCategorySelectionTableViewController.nextButtonPressed(_:)), for: .touchUpInside)
            let rightItem = UIBarButtonItem()
            rightItem.customView = nextButton
            
            self.navigationItem.rightBarButtonItem = rightItem
            
            self.navigationController?.navigationBar.shouldRemoveShadow(true)
            self.navigationController?.navigationBar.barTintColor = .white
        }
        
        //MARK: Check Box Button action
        
        @objc func buttonCheckBoxClicked(_ sender:Any){
            let tag:Int = (sender as AnyObject).tag
            let index:IndexPath = IndexPath.init(row: tag, section: 0)
            self.tableView(self.tableView, didSelectRowAt: index)
            
        }
        //MARK: Back Button Action
        @objc func backButtonPressed(_sender :Any){
            self.navigationController?.navigationBar.isHidden = true
            self.navigationController?.popViewController(animated: true)
        }
        
        //MARK: Done Button Action
        @objc func nextButtonPressed(_ sender:Any){
//            let predicate:NSPredicate =  NSPredicate(format: "isSelected = %d", true)
//            let array:NSArray = (subCategoryArray as NSArray).filtered(using: predicate) as NSArray
            
            if selectedSubCategoryDetails.count == 0{
                AppCustomClass.showToastAlert(Title: "", Message: "Sub Category must be selected to continue", Controller: self, Time: 1)
            }
            else{
                self.delegate.selectedSubCategory(arrSubCat: selectedSubCategoryDetails)
                self.navigationController?.navigationBar.isHidden = true
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        func apiForSubCategory(){
            let isReachable = Reachability.isConnectedToNetwork()
            if isReachable{
                self.activityIndicator.showActivityIndicator(uiView: self.view)
                var headerInfo:[String:String] = [String:String]()
                headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
                headerInfo["device-type"] = "ios"
                headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
                headerInfo["locale"] = "en"
                var param:[String:Any] = [String:Any]()
                //let id:Int = arrCategoryId//[0] as! Int
                param["category_id"] = arrCategoryId
                
                APIManager.postRequestForURLString(true, Constants.URLConstants.getSubCategory, headerInfo, param) { (Done, error, Success, result, message, isActiveSession) in
                    DispatchQueue.main.async {
                        self.activityIndicator.hideActivityIndicator(uiView: self.view)
                        if Success{
                            let resultDict = result as! [String:Any]
                            print(resultDict)
                            self.subCategoryArray = [Any]()
                            let arr:[Any] = resultDict["sub_categories"] as! [Any]
                            for n in arr {
                                var dict:[String:Any] = n as! [String:Any]
                                dict["isSelected"] = false
                                self.subCategoryArray.append(dict)
                            }
                            self.tableView.reloadData()
                        }
                        else{
                            AppCustomClass.showToastAlert(Title:"", Message:"No Address found.", Controller: self, Time: 1)
                        }
                    }
                }
            }
            else{
                AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
            }
        }
        
        //MARK: Getting TableView Cells
        func getCell(index: Int) -> SubCategorySelectionTableViewCell? {
            guard let cell = self.tableView.cellForRow(at: IndexPath(row : index, section : 0)) as? SubCategorySelectionTableViewCell else {return nil}
            return cell
        }
        
        func subCategoryDetails(dictDetails: [String : Any]) {
            selectedSubCategoryDetails.append(dictDetails)
            let cell = self.getCell(index: selectedIndex.row)
            var dict:[String:Any] = subCategoryArray[selectedIndex.row] as! [String:Any]
            if dict["isSelected"] as! Bool == false{
                dict["isSelected"] = true
                subCategoryArray.remove(at: selectedIndex.row)
                subCategoryArray.insert(dict, at: selectedIndex.row)
                cell?.buttonCheckBox.setImage(UIImage.init(named: "check-box-tick"), for: .normal)
            }
            else{
                dict["isSelected"] = false
                subCategoryArray.remove(at: selectedIndex.row)
                subCategoryArray.insert(dict, at: selectedIndex.row)
                cell?.buttonCheckBox.setImage(UIImage.init(named: "check-box"), for: .normal)
            }
        }
    }
    
