//
//  ServiceDetailTableViewController.swift
//  StyleMe
//
//  Created by EL INFO on 06/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
protocol SelectedSubCategoryDetailsDelegate {
    func subCategoryDetails(dictDetails:[String:Any])
}

class ServiceDetailTableViewController: UITableViewController {
    
    @IBOutlet weak var textFieldServiceName: UITextField!
    @IBOutlet weak var textFieldServiceType: UITextField!
    @IBOutlet var textViewDescription: UITextView!
    @IBOutlet weak var textFieldGender: UITextField!
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var extraTimeDurationTextField: UITextField!
    @IBOutlet weak var extraTimeTypeTextField: UITextField!
    @IBOutlet weak var textFieldServiceDuration: UITextField!
    @IBOutlet weak var textFieldRetailPrice: UITextField!
    @IBOutlet weak var textFieldPricingName: UITextField!
    @IBOutlet weak var textFieldSpecialPrice: UITextField!
    var detailsDelegate:SelectedSubCategoryDetailsDelegate!
    var dictDetails:[String:Any]!
    var isSelected:Bool!
    var selectedTextField:UITextField!
    var pickerView:UIPickerView!
    var serviceAvailabityForArray = ["Male", "Female", "Others"]
    var timeDurationArray:[String] =  [ "10 min", "20 min","30 min","40 min","50 min","1 hr 0 min","1 hr 10 min","1 hr 20 min","1 hr 30 min","1 hr 40 min", "1 hr 50 min", "2 hrs 0 min"]
    var processingTimeAfterArray = ["Process Time after","Service Time","Travelling Time"]
    var selectedSubCategory:[String:Any]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customNavigationBar()
        self.drawBorder(view: textFieldPricingName)
        self.drawBorder(view: textFieldRetailPrice)
        self.drawBorder(view: textFieldSpecialPrice)
        self.drawBorder(view: textFieldServiceDuration)
        pickerView = UIPickerView.init()
        pickerView.delegate = self
        pickerView.dataSource = self
        self.textFieldServiceType.text = selectedSubCategory["name"] as? String
        self.textFieldServiceDuration.inputView = pickerView
        self.textFieldGender.inputView = pickerView
        self.extraTimeTypeTextField.inputView = pickerView
        self.extraTimeDurationTextField.inputView = pickerView
        
        self.textFieldGender.inputAccessoryView = self.addToolBar()
        self.textFieldServiceDuration.inputAccessoryView = self.addToolBar()
        self.extraTimeDurationTextField.inputAccessoryView = self.addToolBar()
        self.extraTimeTypeTextField.inputAccessoryView = self.addToolBar()
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
}

extension ServiceDetailTableViewController{
    
    //MARK: Custom Navigation Bar
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        backButton.setImage(UIImage.init(named: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(ServiceDetailTableViewController.backButtonPressed(_sender:)), for: .touchUpInside)
        let leftItem = UIBarButtonItem()
        leftItem.customView = backButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "Add Service Detail"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem, fixedSpace,leftItem2]
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    //MARK: Back Button Action
    @objc func backButtonPressed(_sender :Any){
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(){
        if textFieldServiceName.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Please enter Service Name", Controller: self, Time: 1)
            return
        }
        else if textViewDescription.text.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Please enter Service Decription", Controller: self, Time: 1)
            return
        }
        else if textFieldGender.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Please select Gender for your services", Controller: self, Time: 1)
            return
        }
        else if extraTimeTypeTextField.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Please select extra time type", Controller: self, Time: 1)
            return
        }
        else if extraTimeDurationTextField.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Please select extra time duration", Controller: self, Time: 1)
            return
        }
        else if textFieldServiceDuration.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Please enter service duration", Controller: self, Time: 1)
            return
        }
        else if textFieldRetailPrice.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Please enter retail pricing for this service", Controller: self, Time: 1)
            return
        }
        else{
            if !isSelected{
                dictDetails = [String:Any]()
            }
            dictDetails["name"] = selectedSubCategory["name"] as! String
            dictDetails["category_id"] = selectedSubCategory["parent_id"] as! Int
            dictDetails["sub_category_id"] = selectedSubCategory["id"] as! Int
            dictDetails["service_name"] = textFieldServiceName.text
            dictDetails["description"] = textViewDescription.text
            dictDetails["service_duration"] = textFieldServiceDuration.text
            dictDetails["retail_price"] = textFieldRetailPrice.text
            dictDetails["is_discount"] = false
            dictDetails["is_gift"] = false
            dictDetails["available_for"] = textFieldGender.text
            dictDetails["enable_commission"] = false
            dictDetails["extra_time_type"] = extraTimeTypeTextField.text
            dictDetails["extra_time_duration"] = extraTimeDurationTextField.text
            dictDetails["is_active"] = true
            dictDetails["enable_online_booking"] = true
            self.detailsDelegate.subCategoryDetails(dictDetails: dictDetails)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func drawBorder(view:UIView){
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.white.cgColor
    }
}
extension ServiceDetailTableViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
        
        if textField == textFieldServiceDuration{
            pickerView.reloadAllComponents()
        }
        else if textField == extraTimeTypeTextField{
            pickerView.reloadAllComponents()
        }
        else if textField == textFieldGender{
            pickerView.reloadAllComponents()
        }
        else if textField == extraTimeDurationTextField{
            pickerView.reloadAllComponents()
        }
    }
}

extension ServiceDetailTableViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if selectedTextField == textFieldServiceDuration{
            return timeDurationArray.count
        }
        else if selectedTextField == extraTimeTypeTextField{
            return processingTimeAfterArray.count
        }
        else if selectedTextField == textFieldGender{
            return serviceAvailabityForArray.count
        }
        else{
            return timeDurationArray.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if selectedTextField == textFieldServiceDuration{
            return timeDurationArray[row]
        }
        else if selectedTextField == extraTimeTypeTextField{
            return processingTimeAfterArray[row]
        }
        else if selectedTextField == textFieldGender{
            return serviceAvailabityForArray[row]
        }
        else{
            return timeDurationArray[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if selectedTextField == textFieldServiceDuration{
            selectedTextField.text = timeDurationArray[row]
        }
        else if selectedTextField == extraTimeTypeTextField{
            selectedTextField.text = processingTimeAfterArray[row]
        }
        else if selectedTextField == textFieldGender{
            selectedTextField.text = serviceAvailabityForArray[row]
        }
        else{
            selectedTextField.text = timeDurationArray[row]
        }
    }
}


@IBDesignable extension UITextField{
    
    @IBInspectable var setImageName: String {
        get{
            return ""
        }
        set{
            let containerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width:30, height: self.frame.height))
            let imageView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
            imageView.image = UIImage(named: newValue)!
            containerView.addSubview(imageView)
            imageView.center = containerView.center
            self.rightView = containerView
            self.rightViewMode = UITextField.ViewMode.always
        }
    }
}


extension ServiceDetailTableViewController{
    func addToolBar()->UIToolbar{
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelNumberPad)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneWithNumberPad))]
        numberToolbar.sizeToFit()
        return numberToolbar
    }
    
    @objc func cancelNumberPad() {
        //Cancel with number pad
        selectedTextField.resignFirstResponder()
    }
    
    @objc func doneWithNumberPad() {
        //        let dateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "hh:mm a"
        //        selectedTextField.text = dateFormatter.string(from: datePicker.date)
        //        selectedTextField.resignFirstResponder()
        selectedTextField.resignFirstResponder()
        
    }
}
