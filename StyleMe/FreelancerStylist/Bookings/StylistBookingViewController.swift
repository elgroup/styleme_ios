//
//  StylistBookingViewController.swift
//  StyleMe
//
//  Created by EL INFO on 24/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class StylistBookingViewController: UIViewController {

    @IBOutlet weak var bookingsTableView:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.customNavigationBar()
        self.bookingsTableView.register(UINib(nibName: "OrdersTableViewCell", bundle: nil), forCellReuseIdentifier: "OrdersTableViewCell")

        // Do any additional setup after loading the view.
    }
}

extension StylistBookingViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.bookingsTableView.dequeueReusableCell(withIdentifier: "OrdersTableViewCell", for: indexPath as IndexPath) as! OrdersTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension StylistBookingViewController{
    
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.navigationController?.navigationItem.hidesBackButton = true
        
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        backButton.setImage(UIImage.init(named: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(StylistBookingViewController.backButtonPressed(_sender:)), for: .touchUpInside)
        let leftItem = UIBarButtonItem()
        leftItem.customView = backButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "ORDERS"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem, fixedSpace,leftItem2]
        
        let nextButton = UIButton.init()
        nextButton.frame.size.width = 100
        nextButton.setTitle("History", for: .normal)
        nextButton.setTitleColor(.gray, for: .normal)
        nextButton.titleLabel?.font = UIFont.PoppinsRegular(ofSize: 13)
        nextButton.layer.cornerRadius = 10.0
        nextButton.layer.borderColor = UIColor.black.cgColor
        nextButton.layer.borderWidth = 1.0
        nextButton.clipsToBounds = true
        nextButton.addTarget(self, action: #selector(StylistBookingViewController.historyButtonPressed(_:)), for: .touchUpInside)
        let rightItem = UIBarButtonItem()
        rightItem.customView = nextButton
        
        self.navigationItem.rightBarButtonItem = rightItem
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    @objc func backButtonPressed(_sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func historyButtonPressed(_ sender: Any){
        
    }
}
