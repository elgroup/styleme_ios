//
//  AddStylistTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 17/02/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AddStylistTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonAddStylist:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        buttonAddStylist.layer.cornerRadius = 5.0
        buttonAddStylist.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
