//
//  CategorySelectionTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 05/02/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
protocol categorySelectionDelegate{
    func gotoCategorySelection(selectedSection:Int)
}
class CategorySelectionTableViewCell: UITableViewCell {
    var delegate:categorySelectionDelegate!
    @IBOutlet weak var textFieldCategoryName:UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension CategorySelectionTableViewCell:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let tag = textField.tag
        textField.resignFirstResponder()
        self.delegate.gotoCategorySelection(selectedSection: tag)
    }
}
