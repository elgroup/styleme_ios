//
//  AddHoursTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 17/02/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
protocol getWorkingHoursDelegate{
    func getStartingTime(date:String,textField:UITextField)
    func getEndTime(date:String,textField:UITextField)
    
}
class AddHoursTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelDay:UILabel!
    @IBOutlet weak var buttonTick:UIButton!
    @IBOutlet weak var labelClosed:UILabel!
    @IBOutlet weak var textFieldFrom:UITextField!
    @IBOutlet weak var textFieldTo:UITextField!
    @IBOutlet weak var button24HoursCheck:UIButton!
    var delegate:getWorkingHoursDelegate!
    var selectedTextField:UITextField!
    var datePicker:UIDatePicker!
    override func awakeFromNib() {
        super.awakeFromNib()
        datePicker = UIDatePicker()
        datePicker.datePickerMode = .time
        datePicker.backgroundColor = .white
        textFieldFrom.inputView = datePicker
        textFieldTo.inputView = datePicker
        textFieldFrom.inputAccessoryView = addToolBar()
        textFieldTo.inputAccessoryView = addToolBar()
        datePicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        selectedTextField.text = dateFormatter.string(from: sender.date)
    }
}

extension AddHoursTableViewCell:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textFieldFrom{
            if textField.text?.count != 0{
                self.delegate.getStartingTime(date: textField.text!, textField: textField)
            }
        }
        else if textField == textFieldTo{
            if textField.text?.count != 0{
                self.delegate.getEndTime(date: textField.text!, textField: textField)
            }
        }
    }
}

extension AddHoursTableViewCell{
    func addToolBar()->UIToolbar{
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelNumberPad)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneWithNumberPad))]
        numberToolbar.sizeToFit()
        return numberToolbar
    }
    
    @objc func cancelNumberPad() {
        //Cancel with number pad
        selectedTextField.resignFirstResponder()
    }
    
    @objc func doneWithNumberPad() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        selectedTextField.text = dateFormatter.string(from: datePicker.date)
        selectedTextField.resignFirstResponder()
    }
}

