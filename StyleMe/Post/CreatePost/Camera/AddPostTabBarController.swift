//
//  AddPostTabBarController.swift
//  StyleMe
//
//  Created by EL INFO on 09/09/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AddPostTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBar.appearance().backgroundColor = .white
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().backgroundImage = UIImage()
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -15)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let controller:PostTabBarController = self.tabBarController as! PostTabBarController
        controller.hideCustomTabBar()
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Constants.UIViewControllerIdentifier.PostTabControllerStoryBoard, bundle: nil)
        let galleryController = mainStoryboard.instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.GalleryViewController)
        
        let cameraController : CameraViewController = mainStoryboard.instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.CameraViewController) as! CameraViewController
        let videoController : VideoViewController = mainStoryboard.instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.VideoViewController) as! VideoViewController
        
        let viewControllers: NSMutableArray  =  [galleryController,cameraController,videoController]
        self.viewControllers = viewControllers.map { UINavigationController(rootViewController: $0 as! UIViewController)}//viewControllers as? [UIViewController]
        guard let items = tabBar.items else { return }
        
        items[0].title = "Library"
        items[1].title = "Camera"
        items[2].title = "Video"
        
        let appearance = UITabBarItem.appearance(whenContainedInInstancesOf: [AddPostTabBarController.self])
        appearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.gray, NSAttributedString.Key.font:UIFont.PoppinsLight(ofSize: 16)], for: .normal)
        appearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black,NSAttributedString.Key.font: UIFont.PoppinsSemiBold(ofSize: 15)], for: .selected)
    }
    
}
