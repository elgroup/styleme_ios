//
//  CameraViewController.swift
//  StyleMe
//
//  Created by EL INFO on 09/09/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class CameraViewController: UIViewController {
     var delegate: CameraAssetsDelegate?
    ///Allows the user to put the camera in photo mode.
    @IBOutlet fileprivate var toggleCameraButton: UIButton!
    @IBOutlet fileprivate var toggleFlashButton: UIButton!
    let cameraController = CameraController()
    
    override var prefersStatusBarHidden: Bool { return false }
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var buttonCapture: UIButton!
    
//    var timer:Timer!
//    var timeMin = 0
//    var timeSec = 0
//    var isPhotoMode: Bool = true
//    var isRecording = false
//
    
    enum CameraControllerError: Swift.Error {
        case captureSessionAlreadyRunning
        case captureSessionIsMissing
        case inputsAreInvalid
        case invalidOperation
        case noCamerasAvailable
        case unknown
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        func configureCameraController() {
            cameraController.prepare {(error) in
                if let error = error {
                    print(error)
                }
                try? self.cameraController.displayPreview(on: self.previewView)
            }
        }
        configureCameraController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        cameraController.captureSession?.startRunning()
        self.customNavigationBar()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraController.captureSession?.stopRunning()
    }
}

extension CameraViewController{
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        menuButton.setImage(UIImage.init(named: "cross"), for: .normal)
        menuButton.addTarget(self, action: #selector(AddPostViewController.crossButtonClicked(_:)), for: .touchUpInside)
        let leftItem1 = UIBarButtonItem()
        leftItem1.customView = menuButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "PHOTO"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    @objc func crossButtonClicked(_ sender: Any){
        let controller:AddPostTabBarController = self.tabBarController as! AddPostTabBarController
        let postController:PostTabBarController = controller.tabBarController as! PostTabBarController
        postController.showCustomTabBar()
        postController.buttonHomePressed(postController.buttonHome as Any)
    }
    
    @IBAction func toggleFlash(_ sender: UIButton) {
        if cameraController.flashMode == .on {
            cameraController.flashMode = .off
            toggleFlashButton.setImage(#imageLiteral(resourceName: "ic_flash_off"), for: .normal)
        }
            
        else {
            cameraController.flashMode = .on
            toggleFlashButton.setImage(#imageLiteral(resourceName: "Flash On Icon"), for: .normal)
        }
    }
    
    @IBAction func switchCameras(_ sender: UIButton) {
        do {
            try cameraController.switchCameras()
        }
        catch {
            print(error)
        }
    }
    
    @IBAction func captureImage(_ sender: UIButton) {
        cameraController.captureImage {(image, currentCameraPosition, error) in
            guard let image = image else {
                print(error ?? "Image capture error")
                return
            }
            
            DispatchQueue.main.async {
                let stroyboard: UIStoryboard = UIStoryboard.init(name: "Post", bundle: nil)
                let vc:PhotoPreviewViewController = stroyboard.instantiateViewController(withIdentifier: "PhotoPreviewViewController") as! PhotoPreviewViewController
                if currentCameraPosition == CameraController.CameraPosition.front {
                    vc.isFrontCamera = true
                }
                else{
                    vc.isFrontCamera = false
                }
                
                vc.img =  image//image.crop(to: self.previewView.frame.size, fromX: self.previewView.frame.origin.x, fromY: self.previewView.frame.origin.y)
                vc.delegate = self.delegate
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    
}

extension UINavigationBar {
    
    func shouldRemoveShadow(_ value: Bool) -> Void {
        if value {
            self.setValue(true, forKey: "hidesShadow")
        } else {
            self.setValue(false, forKey: "hidesShadow")
        }
    }
}

extension UIImage {
    
    func crop(to:CGSize, fromX:CGFloat, fromY:CGFloat) -> UIImage {
        
        guard let cgimage = self.cgImage else { return self }
        
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        
        guard let newCgImage = contextImage.cgImage else { return self }
        
        let contextSize: CGSize = contextImage.size
        
        //Set to square
        
        var posX: CGFloat = fromX
        var posY: CGFloat = fromY
        let cropAspect: CGFloat = to.width / to.height
        
        var cropWidth: CGFloat = to.width
        var cropHeight: CGFloat = to.height
        
        if to.width > to.height { //Landscape
            cropWidth = contextSize.width
            cropHeight = contextSize.width / cropAspect
            posY = (contextSize.height - cropHeight) / 2
        } else if to.width < to.height { //Portrait
            cropHeight = contextSize.height
            cropWidth = contextSize.height * cropAspect
            posX = (contextSize.width - cropWidth) / 2
        } else { //Square
            if contextSize.width >= contextSize.height { //Square on landscape (or square)
                cropHeight = contextSize.height
                cropWidth = contextSize.height * cropAspect
                posX = (contextSize.width - cropWidth) / 2
            }else{ //Square on portrait
                cropWidth = contextSize.width
                cropHeight = contextSize.width / cropAspect
                posY = (contextSize.height - cropHeight) / 2
            }
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cropWidth, height: cropHeight)
        
        // Create bitmap image from context using the rect
        guard let imageRef: CGImage = newCgImage.cropping(to: rect) else { return self}
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let cropped: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
        
        UIGraphicsBeginImageContextWithOptions(to, false, self.scale)
        cropped.draw(in: CGRect(x: 0, y: 0, width: to.width, height: to.height))
        let resized = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resized ?? self
    }
}
