//
//  AddPostViewController.swift
//  StyleMe
//
//  Created by EL INFO on 16/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVFoundation
import Photos


class AddPostViewController: UIViewController {
    ///Displays a preview of the video output generated by the device's cameras.
    @IBOutlet fileprivate var capturePreviewView: UIView!
    
    ///Allows the user to put the camera in photo mode.
    @IBOutlet fileprivate var photoModeButton: UIButton!
    @IBOutlet fileprivate var toggleCameraButton: UIButton!
    @IBOutlet fileprivate var toggleFlashButton: UIButton!
    
    ///Allows the user to put the camera in video mode.
    @IBOutlet fileprivate var videoModeButton: UIButton!
    
    //Allows user to put in gallery mode
    @IBOutlet fileprivate var libraryModeButton:UIButton!
    
    let cameraController = CameraController()
    
    override var prefersStatusBarHidden: Bool { return false }
    
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var viewCaptureBorder: UIView!
    @IBOutlet weak var buttonCapture: UIButton!
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var showTimerLabel: UILabel!
    @IBOutlet weak var buttonBlink: UIButton!
    @IBOutlet weak var viewOptionsImages: UIView!
    
    var timer:Timer!
    var timeMin = 0
    var timeSec = 0
    var isPhotoMode: Bool = true
    var isRecording = false
    
    
    enum CameraControllerError: Swift.Error {
        case captureSessionAlreadyRunning
        case captureSessionIsMissing
        case inputsAreInvalid
        case invalidOperation
        case noCamerasAvailable
        case unknown
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        func configureCameraController() {
            cameraController.prepare {(error) in
                if let error = error {
                    print(error)
                }
                try? self.cameraController.displayPreview(on: self.previewView)
            }
        }
        photoModeButton.titleLabel?.font = UIFont.PoppinsSemiBold(ofSize: 15)
        configureCameraController()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.timerView.isHidden = true
        self.buttonBlink.layer.cornerRadius = 5.0
        self.buttonBlink.clipsToBounds = true
        var bottomPadding:CGFloat = 0.0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomPadding = (window?.safeAreaInsets.bottom)!
        }
        self.bottomLayoutConstraint.constant = bottomPadding
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let controller:PostTabBarController = self.tabBarController as! PostTabBarController
        controller.hideCustomTabBar()
        self.customNavigationBar()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
}

extension AddPostViewController{
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        menuButton.setImage(UIImage.init(named: "cross"), for: .normal)
        menuButton.addTarget(self, action: #selector(AddPostViewController.crossButtonClicked(_:)), for: .touchUpInside)
        let leftItem1 = UIBarButtonItem()
        leftItem1.customView = menuButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "PHOTO"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white//UIColor.init(red: 250.0/255.0, green: 250.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    }
    
    @objc func crossButtonClicked(_ sender: Any){
        let controller:PostTabBarController = self.tabBarController as! PostTabBarController
        controller.showCustomTabBar()
        controller.buttonHomePressed(controller.buttonHome as Any)
    }
    
    @IBAction func toggleFlash(_ sender: UIButton) {
        if cameraController.flashMode == .on {
            cameraController.flashMode = .off
            toggleFlashButton.setImage(#imageLiteral(resourceName: "ic_flash_off"), for: .normal)
        }
            
        else {
            cameraController.flashMode = .on
            toggleFlashButton.setImage(#imageLiteral(resourceName: "Flash On Icon"), for: .normal)
        }
    }
    
    @IBAction func switchCameras(_ sender: UIButton) {
        do {
            try cameraController.switchCameras()
        }
            
        catch {
            print(error)
        }
    }
    
    @IBAction func captureImage(_ sender: UIButton) {
//        if isPhotoMode {
//            cameraController.captureImage {(image, error) in
//                guard let image = image else {
//                    print(error ?? "Image capture error")
//                    return
//                }
//                try? PHPhotoLibrary.shared().performChangesAndWait {
//                    PHAssetChangeRequest.creationRequestForAsset(from: image)
//                }
//            }
//        }
//        else {
//            if isRecording {
//                do {
//                    self.hideBottomBar()
//                    try cameraController.startVideoRecording()
//                    self.timerView.isHidden = false
//                    self.buttonCapture.setImage(#imageLiteral(resourceName: "video_recording"), for: .normal)
//                    self.isRecording = false
//                    self.startTimer()
//                }
//                catch {
//                    print(error)
//                    self.timerView.isHidden = true
//                }
//            }
//            else {
//                self.showBottomBar()
//                self.cameraController.stopRecording()
//                self.buttonCapture.setImage(#imageLiteral(resourceName: "video_still"), for: .normal)
//                self.stopTimer()
//                self.timerView.isHidden = true
//                self.isRecording = true
//                self.resetTimerToZero()
//            }
//        }
    }
    
    
    @IBAction func videoMode(_ sender: UIButton){
        self.isPhotoMode = false
        self.isRecording = true
        
        self.buttonCapture.setImage(#imageLiteral(resourceName: "video_still"), for: .normal)
        photoModeButton.titleLabel?.font = UIFont.PoppinsRegular(ofSize: 15)
        videoModeButton.titleLabel?.font = UIFont.PoppinsSemiBold(ofSize: 15)
        libraryModeButton.titleLabel?.font = UIFont.PoppinsRegular(ofSize: 15)
    }
    
    @IBAction func photoMode(_ sender: UIButton){
        self.isPhotoMode = true
        self.isRecording = false
        self.buttonCapture.setImage(#imageLiteral(resourceName: "cam nor"), for: .normal)
        videoModeButton.titleLabel?.font = UIFont.PoppinsRegular(ofSize: 15)
        photoModeButton.titleLabel?.font = UIFont.PoppinsSemiBold(ofSize: 15)
        libraryModeButton.titleLabel?.font = UIFont.PoppinsRegular(ofSize: 15)
        
    }
    
    @IBAction func libraryMode(_ sender: UIButton){
        self.isPhotoMode = false
        self.isRecording = false
        
        photoModeButton.titleLabel?.font = UIFont.PoppinsRegular(ofSize: 15)
        videoModeButton.titleLabel?.font = UIFont.PoppinsRegular(ofSize: 15)
        libraryModeButton.titleLabel?.font = UIFont.PoppinsSemiBold(ofSize: 15)
    }
    // MARK:- Timer Functions
    fileprivate func startTimer(){
        
        // if you want the timer to reset to 0 every time the user presses record you can uncomment out either of these 2 lines
        
        // timeSec = 0
        // timeMin = 0
        buttonBlink.alpha = 0.0
        UIView.animate(withDuration: 1, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {self.buttonBlink.alpha = 1.0}, completion: nil)
        // If you don't use the 2 lines above then the timer will continue from whatever time it was stopped at
        let timeNow = String(format: "%02d:%02d", timeMin, timeSec)
        showTimerLabel.text = timeNow
        
        stopTimer() // stop it at it's current time before starting it again
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] _ in
            self?.timerTick()
        }
    }
    @objc fileprivate func timerTick(){
        timeSec += 1
        
        if timeSec == 60{
            timeSec = 0
            timeMin += 1
        }
        
        let timeNow = String(format: "%02d:%02d", timeMin, timeSec)
        
        showTimerLabel.text = timeNow
    }
    @objc fileprivate func resetTimerToZero(){
        timeSec = 0
        timeMin = 0
        stopTimer()
    }
    
    // if you need to reset the timer to 0 and yourLabel.txt back to 00:00
    @objc fileprivate func resetTimerAndLabel(){
        
        resetTimerToZero()
        showTimerLabel.text = String(format: "%02d:%02d", timeMin, timeSec)
    }
    
    // stops the timer at it's current time
    @objc fileprivate func stopTimer(){
        timer?.invalidate()
    }
    
    fileprivate func hideBottomBar(){
        UIView.animate(withDuration: 0.3, animations: {
            var bottomPadding:CGFloat = 0.0
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                bottomPadding = (window?.safeAreaInsets.bottom)!
            }
            self.bottomLayoutConstraint.constant = -(40 + bottomPadding)
            self.view.layoutIfNeeded()
            
        }) { (success) in
        }
    }
    
    fileprivate func showBottomBar(){
        UIView.animate(withDuration: 0.3, animations: {
            var bottomPadding:CGFloat = 0.0
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                bottomPadding = (window?.safeAreaInsets.bottom)!
            }
            self.bottomLayoutConstraint.constant = bottomPadding
            self.view.layoutIfNeeded()
            
        }) { (success) in
            
        }
    }
}
