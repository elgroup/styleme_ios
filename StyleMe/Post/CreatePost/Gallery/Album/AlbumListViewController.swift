//
//  AlbumListViewController.swift
//  StyleMe
//
//  Created by EL INFO on 18/09/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Photos

protocol SelectedAlbumDelegate {
    func selectedAlbum(collection:PHAssetCollection)
}
class AlbumListViewController: UIViewController {
    
    @IBOutlet weak var tableViewAlbumList: UITableView!
    private var albums: [PHAssetCollection] = []
    var delegate:SelectedAlbumDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.customNavigationBar()
        self.checkAutorizationStatus()
    }
    // MARK: - Private methods
    
    private func checkAutorizationStatus() {
        PHPhotoLibrary.checkAuthorizationStatus {
            if $0 {
                self.fetchAlbums()
            } else {
                AppCustomClass.showToastAlert(Title: "Error", Message: "Please authorize gallery access.", Controller: self, Time: 2)
            }
        }
        
    }
    
    private func fetchAlbums() {
        self.albums.removeAll()
        let result = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .any, options: nil)
        result.enumerateObjects({ (collection, _, _) in
            if (collection.hasAssets()) {
                self.albums.append(collection)
            }
        })
        self.tableViewAlbumList.reloadData()
        
    }
}

extension AlbumListViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AlbumListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AlbumListTableViewCell", for: indexPath) as! AlbumListTableViewCell
        cell.setAlbum(albums[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let album = albums[indexPath.row]
        delegate.selectedAlbum(collection: album)
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension AlbumListViewController{
    @IBAction func crossButtonClicked(_ sender: Any){
       self.navigationController?.popViewController(animated: true)
    }
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        menuButton.setImage(UIImage.init(named: "back"), for: .normal)
        menuButton.addTarget(self, action: #selector(AlbumListViewController.crossButtonClicked(_:)), for: .touchUpInside)
        let leftItem1 = UIBarButtonItem()
        leftItem1.customView = menuButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "Albums"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
}
