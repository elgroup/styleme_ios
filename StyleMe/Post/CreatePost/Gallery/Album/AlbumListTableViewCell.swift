//
//  AlbumListTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 18/09/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Photos

class AlbumListTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbNailAlbumListImageView: UIImageView!
    @IBOutlet weak var labelAlbumName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setAlbum(_ album: PHAssetCollection) {
        labelAlbumName.text = album.localizedTitle!
        thumbNailAlbumListImageView.image = album.getCoverImgWithSize(thumbNailAlbumListImageView!.frame.size)
        thumbNailAlbumListImageView.contentMode = .scaleAspectFill
    }

}
