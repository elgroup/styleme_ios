//
//  GalleryViewController.swift
//  StyleMe
//
//  Created by EL INFO on 10/09/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Photos
import AVKit

class GalleryViewController: UIViewController,SelectedAlbumDelegate {
    
    
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var canvasView: UIView!
    var isVideo:Bool!
    var selectedCollection: PHAssetCollection?
    private var albums: [PHAssetCollection] = []
    private var photos: PHFetchResult<PHAsset>!
    var allPhotos: PHFetchResult<PHAsset> = PHFetchResult<PHAsset>()
    var outfitImageScrollView:UIScrollView!
    var imageViewShow: UIImageView!
    var selectedIndex:IndexPath!
    var selectedAsset:PHAsset!
    
    var player:AVPlayer!
    var playerViewController:AVPlayerViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkAutorizationStatus()
        self.customNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if self.player != nil{
            self.player.pause()
        }
    }
    private func checkAutorizationStatus() {
        PHPhotoLibrary.checkAuthorizationStatus {
            if $0 {
                self.fetchAlbums()
            } else {
                AppCustomClass.showToastAlert(Title: "Error", Message: "Please authorize gallery access.", Controller: self, Time: 2)
            }
        }
    }
    
    private func fetchAlbums() {
        self.albums.removeAll()
        let result = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .any, options: nil)
        result.enumerateObjects({ (collection, _, _) in
            if (collection.hasAssets()) {
                self.albums.append(collection)
            }
        })
        if self.albums.count > 0{
            selectedCollection = self.albums[0]
            self.fetchImagesFromGallery(collection: selectedCollection)
            DispatchQueue.main.async {
                self.setNavigationTitle()
            }
        }
    }
    
    func selectedAlbum(collection: PHAssetCollection) {
        selectedCollection = collection
        self.setNavigationTitle()
        self.fetchImagesFromGallery(collection: selectedCollection)
    }
    
    private func fetchImagesFromGallery(collection: PHAssetCollection?) {
        DispatchQueue.main.async {
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false),NSSortDescriptor(key: "modificationDate", ascending: false)]
            fetchOptions.predicate = NSPredicate(format: "mediaType = %d || mediaType = %d", PHAssetMediaType.image.rawValue,PHAssetMediaType.video.rawValue)
            self.isVideo = false
            if let collection = collection {
                self.allPhotos = PHAsset.fetchAssets(in: collection, options: fetchOptions)
                if self.allPhotos.count > 0 {
                    let asset = self.allPhotos.object(at: 0)
                    self.selectedIndex = IndexPath.init(item: 0, section: 0)
                    self.selectedAsset = asset
                    if asset.mediaType == .video{
                        self.playVideo(view: self, phAsset: asset)
                    }
                    else if asset.mediaType == .image{
                        asset.getOrginalImage { (image) in
                            self.createCameraImage(outFitImage: image)
                        }
                    }
                }
                
            } else {
                self.allPhotos = PHAsset.fetchAssets(with: fetchOptions)
            }
            self.galleryCollectionView.reloadData()
        }
    }
    
    
}
extension GalleryViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:GalleryPhotosCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryPhotosCollectionViewCell", for: indexPath) as! GalleryPhotosCollectionViewCell
        let asset = allPhotos.object(at: indexPath.row)
        if asset.mediaType == .video{
            cell.labelTime.isHidden = false
            cell.labelTime.text = asset.duration.stringFromTimeInterval()
        }
        else{
            cell.labelTime.isHidden = true
        }
        // cell.imageViewGalleryList.fetchImage(asset: asset, contentMode: .aspectFill, targetSize: cell.imageViewGalleryList.frame.size)
        cell.imageViewGalleryList.image = asset.getAssetThumbnail(size: cell.imageViewGalleryList.frame.size)
        cell.backgroundColor = .red
        
        if selectedIndex.row == indexPath.row{
            cell.viewOverlaySelected.isHidden = false
        }
        else{
            cell.viewOverlaySelected.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedIndex.row == indexPath.row{
            
        }
        else{
            let unselectedCell = getCell(index: selectedIndex.row)
            unselectedCell?.viewOverlaySelected.isHidden = true
            let selectedCell = getCell(index: indexPath.row)!
            selectedCell.viewOverlaySelected.isHidden = false
            let asset = allPhotos.object(at: indexPath.row)
            selectedAsset = asset
            if asset.mediaType == .video{
                self.playVideo(view: self, phAsset: asset)
            }
            else{
                asset.getOrginalImage { (image) in
                    self.createCameraImage(outFitImage: image)
                }
            }
            selectedIndex.row = indexPath.row
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width/4 - 1
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 1.0
    }
}
//extension UIImageView{
//    //    func fetchImage(asset: PHAsset, contentMode: PHImageContentMode, targetSize: CGSize) {
//    //        let options = PHImageRequestOptions()
//    //        options.version = .original
//    //        PHImageManager.default().requestImage(for: asset, targetSize: targetSize, contentMode: contentMode, options: options) { image, _ in
//    //            guard let image = image else { return }
//    //            switch contentMode {
//    //            case .aspectFill:
//    //                self.contentMode = .scaleAspectFill
//    //                break
//    //            case .aspectFit:
//    //                self.contentMode = .scaleAspectFit
//    //                break
//    //            @unknown default:
//    //                break
//    //            }
//    //            self.image = image
//    //        }
//    //    }
//}

extension GalleryViewController{
    func setNavigationTitle(){
        let albumNameButton = UIButton.init()
        albumNameButton.setTitle(selectedCollection?.localizedTitle, for: .normal)
        albumNameButton.setTitleColor(.black, for: .normal)
        albumNameButton.titleLabel?.font = UIFont.PoppinsMedium(ofSize: 15)
        albumNameButton.addTarget(self, action: #selector(GalleryViewController.selectAlbum(_:)), for: .touchUpInside)
        self.navigationItem.titleView = albumNameButton
    }
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        menuButton.setImage(UIImage.init(named: "cross"), for: .normal)
        menuButton.addTarget(self, action: #selector(GalleryViewController.crossButtonClicked(_:)), for: .touchUpInside)
        let leftItem1 = UIBarButtonItem()
        leftItem1.customView = menuButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = ""
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
        
        let nextButton = UIButton.init()
        nextButton.setTitle("NEXT", for: .normal)
        nextButton.setTitleColor(.red, for: .normal)
        nextButton.titleLabel?.font = UIFont.PoppinsSemiBold(ofSize: 15)
        nextButton.addTarget(self, action: #selector(GalleryViewController.nextButtonPressed(_:)), for: .touchUpInside)
        let rightItem = UIBarButtonItem()
        rightItem.customView = nextButton
        
        self.navigationItem.rightBarButtonItem = rightItem
        self.navigationController?.navigationBar.shouldRemoveShadow(false)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    @objc func crossButtonClicked(_ sender: Any){
        let controller:AddPostTabBarController = self.tabBarController as! AddPostTabBarController
        let postController:PostTabBarController = controller.tabBarController as! PostTabBarController
        postController.showCustomTabBar()
        postController.buttonHomePressed(postController.buttonHome as Any)
    }
    //MARK: Next Button Action
    @objc func nextButtonPressed(_ sender:Any){
        let asset = self.allPhotos.object(at: selectedIndex.row)
        if asset.mediaType == .image{
            var number = 1
            //            asset.getOrginalImage { (image) in
            let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.PostTabControllerStoryBoard, bundle: nil)
            let controller:PhotoPreviewViewController!
            if #available(iOS 13.0, *) {
                controller = story.instantiateViewController(identifier: "PhotoPreviewViewController") as? PhotoPreviewViewController
            } else {
                controller = story.instantiateViewController(withIdentifier: "PhotoPreviewViewController") as? PhotoPreviewViewController
            }
            let img =  UIImage.init(view: self.canvasView)
            controller.img = img //selectedImage.crop(to: self.canvasView.frame.size, fromX: self.canvasView.frame.origin.x, fromY: self.canvasView.frame.origin.y)
            controller.isFrontCamera = false
            
            self.navigationController?.pushViewController(controller, animated: true)
            print(number)
            number = number + 1
            //   }
        }
        else if asset.mediaType == .video{
            let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.PostTabControllerStoryBoard, bundle: nil)
            let controller:VideoPreviewViewController!
            if #available(iOS 13.0, *) {
                controller = story.instantiateViewController(identifier: "VideoPreviewViewController") as? VideoPreviewViewController
            } else {
                controller = story.instantiateViewController(withIdentifier: "VideoPreviewViewController") as? VideoPreviewViewController
            }
            controller.asset = selectedAsset
            controller.isVideoRecording = false
            self.navigationController?.pushViewController(controller, animated: true)

        }
        
    }
    
    @objc func selectAlbum(_ sender:Any){
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.PostTabControllerStoryBoard, bundle: nil)
        let controller = story.instantiateViewController(withIdentifier: "AlbumListViewController") as! AlbumListViewController
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    //MARK: Get Cell
    func getCell(index: Int) -> GalleryPhotosCollectionViewCell? {
        guard let cell = self.galleryCollectionView.cellForItem(at: IndexPath(row : index, section : 0)) as? GalleryPhotosCollectionViewCell else {return nil}
        return cell
    }
    //MARK: Method for showing selected image
    
    func createCameraImage(outFitImage : UIImage){
        outfitImageScrollView?.removeFromSuperview()
        if self.player != nil{
            self.player.pause()
        }
        if allPhotos.count > 0 {
            outfitImageScrollView = UIScrollView()
            outfitImageScrollView?.delegate = self
            let width:CGFloat = canvasView.frame.size.width
            let itemImageHeight = ((outFitImage.size.height) * width)/(outFitImage.size.width)
            let frame = CGRect(x: 0, y: 0, width: width, height: itemImageHeight)
            outfitImageScrollView?.frame = CGRect(x: 0, y: 0, width: width, height: canvasView.frame.size.height)
            outfitImageScrollView?.contentSize = CGSize(width: width, height: itemImageHeight)
            outfitImageScrollView?.backgroundColor = .clear
            outfitImageScrollView?.minimumZoomScale = 1.0
            outfitImageScrollView?.maximumZoomScale = 2.0
            self.imageViewShow = UIImageView(frame: frame)
            self.imageViewShow?.image = outFitImage
            self.imageViewShow?.backgroundColor = UIColor.clear
            self.imageViewShow?.contentMode = UIView.ContentMode.scaleAspectFill
            self.imageViewShow?.clipsToBounds = true
            self.imageViewShow?.isUserInteractionEnabled = false
            outfitImageScrollView?.isUserInteractionEnabled = true
            outfitImageScrollView.showsVerticalScrollIndicator = false
            outfitImageScrollView.showsHorizontalScrollIndicator = false
            outfitImageScrollView!.addSubview(self.imageViewShow!)
            canvasView.addSubview(outfitImageScrollView!)
        }
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageViewShow
    }
    func playVideo (view:UIViewController, phAsset:PHAsset) {
        let option = PHVideoRequestOptions()
        option.version = .original
        
        PHCachingImageManager().requestAVAsset(forVideo: phAsset, options: nil) { (avAsset, audioMix, args) in
            let isSlomo = (avAsset is AVComposition)
            if isSlomo{
                let asset = avAsset as! AVComposition
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                let documentsDirectory: NSString? = paths.first as NSString?
                if documentsDirectory != nil {
                    let random = Int(arc4random() % 1000)
                    let pathToAppend = String(format: "mergeSlowMoVideo-%d.mov", random)
                    let myPathDocs = documentsDirectory!.strings(byAppendingPaths: [pathToAppend])
                    let myPath = myPathDocs.first
                    if myPath != nil {
                        let url = URL(fileURLWithPath: myPath!)
                        let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetMediumQuality)
                        if exporter != nil {
                            exporter!.outputURL = url
                            exporter!.outputFileType = AVFileType.mov
                            exporter!.shouldOptimizeForNetworkUse = true
                            exporter!.exportAsynchronously(completionHandler: {
                                DispatchQueue.main.async {
                                    let url = exporter!.outputURL
                                    if url != nil {
                                        if self.player != nil{
                                            self.player.pause()
                                        }
                                        self.player = AVPlayer(url: url!)
                                        let playerFrame = CGRect(x: 0, y: 0, width: self.canvasView.frame.width, height: self.canvasView.frame.height)
                                        self.playerViewController = AVPlayerViewController()
                                        self.playerViewController.player = self.player
                                        self.playerViewController.videoGravity = .resizeAspectFill
                                        self.playerViewController.view.frame = playerFrame
                                        self.playerViewController.showsPlaybackControls = false
                                        self.addChild(self.playerViewController)
                                        self.canvasView.addSubview(self.playerViewController.view)
                                        self.playerViewController.didMove(toParent: self)
                                        self.playerViewController.player?.play()
                                        
                                    }
                                }
                            })
                        }
                    }
                }
            }
            else{
                let asset = avAsset as! AVURLAsset
                DispatchQueue.main.async {
                    if self.player != nil{
                        self.player.pause()
                    }
                    self.player = AVPlayer(url: asset.url)
                    self.player.rate = 1.0 //auto play
                    let playerFrame = CGRect(x: 0, y: 0, width: self.canvasView.frame.width, height: self.canvasView.frame.height)
                    self.playerViewController = AVPlayerViewController()
                    self.playerViewController.player = self.player
                    self.playerViewController.videoGravity = .resizeAspectFill
                    self.playerViewController.view.frame = playerFrame
                    self.playerViewController.showsPlaybackControls = false
                    self.addChild(self.playerViewController)
                    self.canvasView.addSubview(self.playerViewController.view)
                    self.playerViewController.didMove(toParent: self)
                }
            }
        }
    }
}


extension TimeInterval{
    
    func stringFromTimeInterval() -> String {
        
        let time = NSInteger(self)
        let seconds = time % 60
        let minutes = (time / 60) % 60
        let hours = (time / 3600)
        
        if hours == 0 && minutes == 0 {
            return String(format: "%0.1d:%0.2d",minutes,seconds)
        }
        else if hours == 0 && minutes > 0{
            return String(format: "%0.1d:%0.2d",minutes,seconds)
        }
        else{
            return String(format: "%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
        }
        
    }
}
extension UIImage{
    convenience init(view: UIView) {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
        
    }
}
