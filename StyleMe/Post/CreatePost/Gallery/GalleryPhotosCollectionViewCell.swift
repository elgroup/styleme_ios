//
//  GalleryPhotosCollectionViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 18/09/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class GalleryPhotosCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewGalleryList:UIImageView!
    @IBOutlet weak var viewOverlaySelected: UIView!
    @IBOutlet weak var labelTime: UILabel!
}
