//
//  NewPostTVC.swift
//  StyleMe
//
//  Created by EL INFO on 17/09/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import GooglePlaces
import Photos
import AVKit

class NewPostTVC: UITableViewController {
    @IBOutlet weak var imageViewPlay:UIImageView!
    @IBOutlet weak var containerView:UIView!
    @IBOutlet weak var imageViewPost: UIImageView!
    @IBOutlet weak var textViewCaption: UITextView!
    @IBOutlet weak var textFieldCategory: UITextField!
    @IBOutlet weak var textFieldLocation: UITextField!
    @IBOutlet weak var buttonShare: UIButton!
    @IBOutlet weak var heightLC: NSLayoutConstraint!
    var imagePreview:UIImage!
    var arraycategoryList:[Any]!
    var height:CGFloat!
    var selectedCategoryID:Int!
    var selectedTextField:UITextField!
    var activityIndicator: ActivityIndicator! = ActivityIndicator()
    var isVideo:Bool!
    var videoUrl:URL!
    var player:AVPlayer!
    var playerViewController:AVPlayerViewController!
    var avAsset:AVAsset!
    var startTime:Double!
    var endtime:Double!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.arraycategoryList = [Any]()
        self.apiForCategoryList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isVideo{
            DispatchQueue.main.async {
                self.imageViewPlay.isHidden = false
                self.player = AVPlayer(url: self.videoUrl)
                self.player.rate = 1.0 //auto play
                let playerFrame = CGRect(x: 0, y: 0, width: self.containerView.frame.width, height: self.containerView.frame.height)
                self.playerViewController = AVPlayerViewController()
                self.playerViewController.player = self.player
                self.player.pause()
                self.playerViewController.videoGravity = .resizeAspectFill
                self.playerViewController.view.frame = playerFrame
                self.playerViewController.showsPlaybackControls = false
                self.addChild(self.playerViewController)
                self.containerView.addSubview(self.playerViewController.view)
                self.playerViewController.didMove(toParent: self)
            }
        }
        else{
            self.imageViewPlay.isHidden = true
            imageViewPost.image = imagePreview
        }
        self.customNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        buttonShare.roundCorners([.allCorners], radius: 5.0)
    }
    @IBAction func buttonSharePressed(_ sender: Any) {
        if isVideo{
            self.cropVideo(avAsset: avAsset, startTime: startTime, endTime: endtime)
        }
        else{
            self.apiForNewPost()
            
        }
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return height
        }
        else{
            return UITableView.automaticDimension
        }
    }
}

//MARK: Extension for Google places api
extension NewPostTVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(String(describing: place.name))")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place address component: \(String(describing: place.addressComponents))")
        
        print("Place ID: \(String(describing: place.placeID))")
        print("Place attributions: \(String(describing: place.attributions))")
        dismiss(animated: true, completion: nil)
        //        if place.addressComponents!.count > 0{
        //            for n in place.addressComponents!{
        //                let dictTypeLocality = n as! [String:Any]
        //
        //            }
        //        }
        textFieldLocation.text = place.name
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
//MARK: Extension for Textfield delegate and textViewDelegate
extension NewPostTVC:UITextViewDelegate,UITextFieldDelegate{
    //MARK: Textfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFieldLocation{
            self.autocompleteClicked()
        }
        else if textField == self.textFieldCategory{
            selectedTextField = textField
            if self.arraycategoryList.count > 0{
            }
            else{
                self.apiForCategoryList()
                
            }
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
        
    }
    //MARK: Textview Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textViewCaption.text == "Write a caption"{
            self.textViewCaption.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return true
    }
}

//MARK: Functions for NewPostTVC
extension NewPostTVC{
    
    func cropVideo(avAsset: AVAsset, startTime: Double, endTime: Double, completion: ((_ outputUrl: URL) -> Void)? = nil){
        let fileManager = FileManager.default
        let documentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        //
        let asset = avAsset
        let length = Float(asset.duration.value) / Float(asset.duration.timescale)
        //        print("video length: \(length) seconds")
        let date = Date().ticks
        var outputURL = documentDirectory.appendingPathComponent("\(date)")
        do {
            try fileManager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
            outputURL = outputURL.appendingPathComponent(".mp4")
        }catch let error {
            print(error)
        }
        
        //Remove existing file
        try? fileManager.removeItem(at: outputURL)
        
        let timeRange = CMTimeRange(start: CMTime(seconds: startTime, preferredTimescale: 1000),
                                    end: CMTime(seconds: endTime, preferredTimescale: 1000))
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetMediumQuality) else { return }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4
        exportSession.timeRange = timeRange
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                print("exported at \(outputURL)")
                completion?(outputURL)
                self.apiForNewPost()
                //                self.save(videoUrl:outputURL as NSURL , toAlbum: "StyleUp") { (Success, error) in
                //                    if Success{
                //                        print("Done")
                //                    }
                //                    else{
                //                        print("Not Done")
                //                    }
            //                }
            case .failed:
                print("failed \(exportSession.error.debugDescription)")
            case .cancelled:
                print("cancelled \(exportSession.error.debugDescription)")
            default: break
            }
        }
    }
    
    func addPickerView()->UIPickerView{
        let picker: UIPickerView
        picker = UIPickerView.init()
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        return picker
    }
    
    
    func addToolBar()->UIToolbar{
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelNumberPad)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneWithNumberPad))]
        numberToolbar.sizeToFit()
        return numberToolbar
    }
    
    @objc func cancelNumberPad() {
        //Cancel with number pad
        self.view.endEditing(true)
    }
    
    @objc func doneWithNumberPad() {
        if selectedTextField == textFieldCategory as UITextField?{
            if selectedCategoryID == nil{
                let dict:[String:Any] = self.arraycategoryList[0] as! [String:Any]
                let countryId:Int = dict["id"] as! Int
                selectedCategoryID = countryId
            }
            else{
                selectedTextField.resignFirstResponder()
                
            }
        }
        else{
            selectedTextField.resignFirstResponder()
        }
    }
    
    
    //MARK: Custom Navigation Bar
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        backButton.setImage(UIImage.init(named: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(NewPostViewController.backButtonPressed(_:)), for: .touchUpInside)
        let leftItem = UIBarButtonItem()
        leftItem.customView = backButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "New Post"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem, fixedSpace,leftItem2]
        self.navigationController?.navigationBar.barTintColor = .white
    }
    //MARK: Back Button Action
    @objc func backButtonPressed(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    // Present the Autocomplete view controller when the button is pressed.
    @objc func autocompleteClicked() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.formattedAddress.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.name.rawValue) | UInt(GMSPlaceField.addressComponents.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func apiForNewPost(){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            var arrayImages:[Any] = [Any]()
            var data:Data!
            if isVideo{
                do{
                    try  data = Data.init(contentsOf: videoUrl)
                }catch{
                    print(error)
                }
                
            }
            else{
                data = (imageViewPost.image?.jpegData(compressionQuality: 0.5))!
            }
            var postData:[String:Any] = [String:Any]()
            arrayImages.append(data)
            DispatchQueue.main.async {
                self.activityIndicator.showActivityIndicator(uiView: self.view)
            }
            postData["post_location"] = self.textFieldLocation.text
            postData["description"] = self.textViewCaption.text
            postData["category_id"] =  selectedCategoryID
            postData["is_video"] = isVideo
            postData["post_attachments_attributes"] = arrayImages
            APIManager.postRequestWithMultipartForImages(postData: postData) { (successStatus, error, resultSuccessStatus, result, msg, isActiveSession) in
                DispatchQueue.main.async {
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if successStatus{
                        
                        AppCustomClass.showToastAlert(Title: "Success", Message: "Post has been successfully posted", Controller: self, Time: 1)
                        let _ = Timer.scheduledTimer(withTimeInterval: 1.1, repeats: false) { (timer) in
                            let controller:AddPostTabBarController = self.tabBarController as! AddPostTabBarController
                            let postController:PostTabBarController = controller.tabBarController as! PostTabBarController
                            postController.showCustomTabBar()
                            postController.buttonHomePressed(postController.buttonHome as Any)
                        }
                    }
                    else{
                        AppCustomClass.showCustomAlert(alertTitle: "Failure", Message: "Something went wrong. Press Try Again for trying again else press Cancel to exit", buttonTitle: "Try Again", Controller: self) { (Success) in
                            if Success{
                                self.apiForNewPost()
                            }
                            else{
                                let controller:AddPostTabBarController = self.tabBarController as! AddPostTabBarController
                                let postController:PostTabBarController = controller.tabBarController as! PostTabBarController
                                postController.showCustomTabBar()
                                postController.buttonHomePressed(postController.buttonHome as Any)
                            }
                        }
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    
    func apiForCategoryList(){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            APIManager.getRequestForURLString(true, Constants.URLConstants.getCategories, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    if Success{
                        let resultDict = result as! [String:Any]
                        self.arraycategoryList = [Any]()
                        self.arraycategoryList = resultDict["categories"] as? [Any]
                        self.textFieldCategory.inputView = self.addPickerView()
                        self.textFieldCategory.inputAccessoryView = self.addToolBar()
                        print(self.arraycategoryList!)
                    }
                    else{
                        AppCustomClass.showToastAlert(Title:"", Message: message!, Controller: self, Time: 1)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
}
extension NewPostTVC:UIPickerViewDelegate,UIPickerViewDataSource{
    //MARK: UIPickerView Delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return  self.arraycategoryList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dict:[String:Any] = self.arraycategoryList[row] as! [String:Any]
        let categoryName:String = dict["name"] as! String
        textFieldCategory.text = categoryName
        return categoryName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let dict:[String:Any] = self.arraycategoryList[row] as! [String:Any]
        let categoryId:Int = dict["id"] as! Int
        let categoryName:String = dict["name"] as! String
        textFieldCategory.text = categoryName
        selectedCategoryID = categoryId
    }
}
