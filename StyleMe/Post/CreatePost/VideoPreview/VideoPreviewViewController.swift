//
//  VideoPreviewViewController.swift
//  StyleMe
//
//  Created by EL INFO on 03/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Photos
import AVKit
import MobileCoreServices

class VideoPreviewViewController: UIViewController {
    @IBOutlet var videoRangeSlider: ABVideoRangeSlider!
    @IBOutlet weak var canvasView: UIView!
    @IBOutlet weak var buttonPlay:UIButton!
    var player:AVPlayer!
    var playerViewController:AVPlayerViewController!
    var asset:PHAsset!
    var avAsset:AVAsset!
    var frames:[UIImage] = [UIImage]()
    private var generator:AVAssetImageGenerator!
    var isVideoRecording:Bool!
    var videoUrl:URL!
    var timeObserver: AnyObject!
    var startTime = 0.0
    var endTime = 0.0
    var progressTime = 0.0
    var shouldUpdateProgressIndicator = true
    var isSeeking = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        if isVideoRecording{
            self.playRecordedVideo(view: self, videoUrl: self.videoUrl)
            let asset:AVAsset = AVAsset.init(url: videoUrl)
            self.avAsset = asset
        }
        else{
            DispatchQueue.main.async {
                self.playVideo(view: self, phAsset: self.asset)
            }
            PHCachingImageManager().requestAVAsset(forVideo: asset, options: nil) { (avAsset, audioMix, args) in
                self.avAsset = avAsset
            }
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customNavigationBar()
        self.buttonPlay.isHidden = true
        if isVideoRecording{
            DispatchQueue.main.async {
                self.SliderView(avAsset: self.avAsset)
                
            }
        }
        else{
            guard (asset.mediaType == PHAssetMediaType.video)
                else {
                    print("Not a valid video media type")
                    return
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isVideoRecording{
            
        }
        else{
            DispatchQueue.main.async {
                self.SliderView(avAsset: self.avAsset)
            }
        }
    }
}

extension VideoPreviewViewController{
    func playRecordedVideo(view:UIViewController, videoUrl:URL){
        DispatchQueue.main.async {
            self.playerView(videoUrl: videoUrl)
        }
    }
    
    func playVideo (view:UIViewController, phAsset:PHAsset) {
        let option = PHVideoRequestOptions()
        option.version = .original
        
        PHCachingImageManager().requestAVAsset(forVideo: phAsset, options: nil) { (avAsset, audioMix, args) in
            let isSlomo = (avAsset is AVComposition)
            self.SliderView(avAsset: avAsset!)
            if isSlomo{
                let asset = avAsset as! AVComposition
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                let documentsDirectory: NSString? = paths.first as NSString?
                if documentsDirectory != nil {
                    let random = Int(arc4random() % 1000)
                    let pathToAppend = String(format: "mergeSlowMoVideo-%d.mov", random)
                    let myPathDocs = documentsDirectory!.strings(byAppendingPaths: [pathToAppend])
                    let myPath = myPathDocs.first
                    if myPath != nil {
                        let url = URL(fileURLWithPath: myPath!)
                        let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetMediumQuality)
                        if exporter != nil {
                            exporter!.outputURL = url
                            exporter!.outputFileType = AVFileType.mov
                            exporter!.shouldOptimizeForNetworkUse = true
                            exporter!.exportAsynchronously(completionHandler: {
                                DispatchQueue.main.async {
                                    let url = exporter!.outputURL
                                    if url != nil {
                                        self.playerView(videoUrl: url!)
                                    }
                                }
                            })
                        }
                    }
                }
            }
            else{
                let asset = avAsset as! AVURLAsset
                DispatchQueue.main.async {
                    
                    let url:URL = asset.url
                    self.playerView(videoUrl: url)
                }
                
            }
        }
    }
    
    
    func playerView(videoUrl:URL){
        
        self.player = AVPlayer(url: videoUrl)
        self.player.rate = 1.0 //auto play
        let playerFrame = CGRect(x: 0, y: 0, width: self.canvasView.frame.width, height: self.canvasView.frame.height)
        self.playerViewController = AVPlayerViewController()
        self.playerViewController.player = self.player
        self.player.play()
        self.playerViewController.videoGravity = .resizeAspectFill
        self.playerViewController.view.frame = playerFrame
        self.playerViewController.showsPlaybackControls = false
        self.addChild(self.playerViewController)
        self.canvasView.addSubview(self.playerViewController.view)
        self.endTime = CMTimeGetSeconds((self.player.currentItem?.duration)!)
       
        self.playerViewController.didMove(toParent: self)
        self.player?.actionAtItemEnd = .none
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.playerItemDidReachEnd(notification:)),
                                               name: .AVPlayerItemDidPlayToEndTime,
                                               object: self.player?.currentItem)
        let timeInterval: CMTime = CMTimeMakeWithSeconds(0.01, preferredTimescale: 100)
        self.timeObserver = self.player.addPeriodicTimeObserver(forInterval: timeInterval,
                                                                queue: DispatchQueue.main) { (elapsedTime: CMTime) -> Void in
                                                                    self.observeTime(elapsedTime: elapsedTime) } as AnyObject?
    }
    private func observeTime(elapsedTime: CMTime) {
        let elapsedTime = CMTimeGetSeconds(elapsedTime)
        if (self.player.currentTime().seconds > self.endTime){
            self.player.pause()
            buttonPlay.isHidden = false
        }
        if self.shouldUpdateProgressIndicator{
            self.videoRangeSlider.updateProgressIndicator(seconds: elapsedTime)
        }
    }
    @objc func playerItemDidReachEnd(notification: Notification) {
        if let playerItem = notification.object as? AVPlayerItem {
            let timescale = self.player.currentItem!.asset.duration.timescale
            let time = CMTimeMakeWithSeconds(self.startTime, preferredTimescale: timescale)

            playerItem.seek(to: time) { (success) in
                self.player.pause()
            }
            // playerItem.seek(to: CMTime.zero, completionHandler: nil)
            self.buttonPlay.isHidden = false
        }
    }
    
    //MARK: Custom Navigation Bar
    
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        backButton.setImage(UIImage.init(named: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(VideoPreviewViewController.backButtonPressed(_:)), for: .touchUpInside)
        let leftItem = UIBarButtonItem()
        leftItem.customView = backButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "Trim"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem, fixedSpace,leftItem2]
        
        let nextButton = UIButton.init()
        nextButton.setTitle("NEXT", for: .normal)
        nextButton.setTitleColor(.red, for: .normal)
        nextButton.titleLabel?.font = UIFont.PoppinsSemiBold(ofSize: 15)
        nextButton.addTarget(self, action: #selector(VideoPreviewViewController.nextButtonPressed(_:)), for: .touchUpInside)
        let rightItem = UIBarButtonItem()
        rightItem.customView = nextButton
        
        self.navigationItem.rightBarButtonItem = rightItem
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    //MARK: Back Button Action
    
    @objc func backButtonPressed(_ sender: Any){
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func nextButtonPressed(_ sender: Any){
        self.player.pause()
        //self.cropVideo(avAsset: avAsset, startTime: self.startTime, endTime: self.endTime)
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.PostTabControllerStoryBoard, bundle: nil)
        let controller = story.instantiateViewController(withIdentifier: "NewPostTVC") as! NewPostTVC
        controller.isVideo = true
        controller.avAsset = avAsset
        let asset:AVURLAsset = avAsset as! AVURLAsset
        let url:URL = asset.url
        controller.videoUrl = url
        controller.height = self.canvasView.frame.size.height
        controller.startTime = startTime
        controller.endtime = endTime
        self.navigationController?.pushViewController(controller, animated: true)

    }
    
    @IBAction func buttonPlayPressed(_ sender:Any){
        self.player.play()
        shouldUpdateProgressIndicator = true
        buttonPlay.isHidden = true
        buttonPlay.isEnabled = true
    }
    func SliderView(avAsset:AVAsset){
        
        videoRangeSlider.setVideoAsset(avAsset: avAsset)
        videoRangeSlider.delegate = self
        videoRangeSlider.minSpace = 2.0
        videoRangeSlider.maxSpace = 60.0
        // Customize starTimeView
        DispatchQueue.main.async {
            var customView = UIView.init()
            customView = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 40))
            customView.backgroundColor = .black
            customView.alpha = 0.5
            customView.layer.borderColor = UIColor.black.cgColor
            customView.layer.borderWidth = 1.0
            customView.layer.cornerRadius = 8.0
            self.videoRangeSlider.startTimeView.backgroundView = customView
            self.videoRangeSlider.startTimeView.marginLeft = 1.0
            self.videoRangeSlider.startTimeView.marginRight = 1.0
            self.videoRangeSlider.startTimeView.timeLabel.textColor = .white
            self.videoRangeSlider.endTimeView.timeLabel.textColor = .white
            self.videoRangeSlider.maxSpace = 60
            self.videoRangeSlider.isUpdatingThumbnails = true
        }
    }
    
//    func cropVideo(avAsset: AVAsset, startTime: Double, endTime: Double, completion: ((_ outputUrl: URL) -> Void)? = nil){
//        let fileManager = FileManager.default
//        let documentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
//        //
//        let asset = avAsset
//        let length = Float(asset.duration.value) / Float(asset.duration.timescale)
//        //        print("video length: \(length) seconds")
//        let date = Date().ticks
//        var outputURL = documentDirectory.appendingPathComponent("\(date)")
//        do {
//            try fileManager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
//            outputURL = outputURL.appendingPathComponent(".mp4")
//        }catch let error {
//            print(error)
//        }
//
//        //Remove existing file
//        try? fileManager.removeItem(at: outputURL)
//
//        let timeRange = CMTimeRange(start: CMTime(seconds: startTime, preferredTimescale: 1000),
//                                    end: CMTime(seconds: endTime, preferredTimescale: 1000))
//        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetMediumQuality) else { return }
//        exportSession.outputURL = outputURL
//        exportSession.outputFileType = .mp4
//        exportSession.timeRange = timeRange
//        exportSession.exportAsynchronously {
//            switch exportSession.status {
//            case .completed:
//                print("exported at \(outputURL)")
//                completion?(outputURL)
//                let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.PostTabControllerStoryBoard, bundle: nil)
//                let controller = story.instantiateViewController(withIdentifier: "NewPostTVC") as! NewPostTVC
//                controller.isVideo = true
//                controller.videoUrl = outputURL
//                controller.height = self.canvasView.frame.size.height
//                DispatchQueue.main.async {
//                }
////                self.save(videoUrl:outputURL as NSURL , toAlbum: "StyleUp") { (Success, error) in
////                    if Success{
////                        print("Done")
////                    }
////                    else{
////                        print("Not Done")
////                    }
////                }
//            case .failed:
//                print("failed \(exportSession.error.debugDescription)")
//            case .cancelled:
//                print("cancelled \(exportSession.error.debugDescription)")
//            default: break
//            }
//        }
//    }
    func createAlbum(withTitle title: String, completionHandler: @escaping (PHAssetCollection?) -> ()) {
        DispatchQueue.global(qos: .background).async {
            var placeholder: PHObjectPlaceholder?
            
            PHPhotoLibrary.shared().performChanges({
                let createAlbumRequest = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: title)
                placeholder = createAlbumRequest.placeholderForCreatedAssetCollection
            }, completionHandler: { (created, error) in
                var album: PHAssetCollection?
                if created {
                    let collectionFetchResult = placeholder.map { PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [$0.localIdentifier], options: nil) }
                    album = collectionFetchResult?.firstObject
                }
                
                completionHandler(album)
            })
        }
    }
    
    func getAlbum(title: String, completionHandler: @escaping (PHAssetCollection?) -> ()) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            let fetchOptions = PHFetchOptions()
            fetchOptions.predicate = NSPredicate(format: "title = %@", title)
            let collections = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
            
            if let album = collections.firstObject {
                completionHandler(album)
            } else {
                self?.createAlbum(withTitle: title, completionHandler: { (album) in
                    completionHandler(album)
                })
            }
        }
    }
    
    func save(videoUrl: NSURL, toAlbum titled: String, completionHandler: @escaping (Bool, Error?) -> ()) {
        getAlbum(title: titled) { (album) in
            DispatchQueue.global(qos: .background).async {
                PHPhotoLibrary.shared().performChanges({
                    let assetRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoUrl as URL)
                    //PHAssetChangeRequest.creationRequestForAsset(from: photo)
                    let assets = assetRequest?.placeholderForCreatedAsset
                        .map { [$0] as NSArray } ?? NSArray()
                    let albumChangeRequest = album.flatMap { PHAssetCollectionChangeRequest(for: $0) }
                    albumChangeRequest?.addAssets(assets)
                }, completionHandler: { (success, error) in
                    completionHandler(success, error)
                })
            }
        }
    }
}

extension VideoPreviewViewController:ABVideoRangeSliderDelegate{
    // MARK: ABVideoRangeSlider Delegate - Returns time in seconds
    
    func didChangeValue(videoRangeSlider: ABVideoRangeSlider, startTime: Float64, endTime: Float64) {
        self.endTime = endTime
        if startTime != self.startTime{
            self.startTime = startTime
            let timescale = self.player.currentItem?.asset.duration.timescale
            let time = CMTimeMakeWithSeconds(self.startTime, preferredTimescale: timescale!)
            if !self.isSeeking{
                self.isSeeking = true
                self.player.seek(to: time, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero){_ in
                    self.isSeeking = false
                }
            }
        }
    }
    
    func indicatorDidChangePosition(videoRangeSlider: ABVideoRangeSlider, position: Float64) {
        self.shouldUpdateProgressIndicator = false
        
        // Pause the player
        self.player.pause()
        buttonPlay.isHidden = false
        buttonPlay.isEnabled = true
        if self.progressTime != position {
            self.progressTime = position
            let timescale = self.player.currentItem!.asset.duration.timescale
            let time = CMTimeMakeWithSeconds(self.progressTime, preferredTimescale: timescale)
            if !self.isSeeking{
                self.isSeeking = true
                self.player.seek(to: time, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero){_ in
                    self.isSeeking = false
                }
            }
        }
    }
}


extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}

