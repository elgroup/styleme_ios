//
//  VideoViewController.swift
//  StyleMe
//
//  Created by EL INFO on 10/09/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class VideoViewController: UIViewController {
    var delegate: CameraAssetsDelegate?
    ///Allows the user to put the camera in photo mode.
    @IBOutlet fileprivate var toggleCameraButton: UIButton!
    @IBOutlet fileprivate var toggleFlashButton: UIButton!
    let cameraController = CameraController()
    
    override var prefersStatusBarHidden: Bool { return false }
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var buttonCapture: UIButton!
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var showTimerLabel: UILabel!
    @IBOutlet weak var buttonBlink: UIButton!
    
    var timer:Timer!
    var timeMin = 0
    var timeSec = 0
    var isPhotoMode: Bool = true
    var isRecording = false
    
    
    enum CameraControllerError: Swift.Error {
        case captureSessionAlreadyRunning
        case captureSessionIsMissing
        case inputsAreInvalid
        case invalidOperation
        case noCamerasAvailable
        case unknown
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        func configureCameraController() {
            cameraController.prepare {(error) in
                if let error = error {
                    print(error)
                }
                try? self.cameraController.displayPreview(on: self.previewView)
            }
        }
        configureCameraController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cameraController.captureSession?.startRunning()
        self.customNavigationBar()
        self.timerView.isHidden = true
        self.buttonBlink.layer.cornerRadius = 5.0
        self.buttonBlink.clipsToBounds = true
        self.isRecording = false
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraController.captureSession?.stopRunning()
    }
    
}

extension VideoViewController{
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        menuButton.setImage(UIImage.init(named: "cross"), for: .normal)
        menuButton.addTarget(self, action: #selector(AddPostViewController.crossButtonClicked(_:)), for: .touchUpInside)
        let leftItem1 = UIBarButtonItem()
        leftItem1.customView = menuButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "VIDEO"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    @objc func crossButtonClicked(_ sender: Any){
        if isRecording{
            cameraController.stopRecording {(url, error) in
                guard url != nil else {
                    print(error ?? "Image capture error")
                    return
                }
            }
        }
        let controller:AddPostTabBarController = self.tabBarController as! AddPostTabBarController
        let postController:PostTabBarController = controller.tabBarController as! PostTabBarController
        postController.showCustomTabBar()
        postController.buttonHomePressed(postController.buttonHome as Any)
    }
    
    @IBAction func toggleFlash(_ sender: UIButton) {
        if cameraController.flashMode == .on {
            cameraController.flashMode = .off
            toggleFlashButton.setImage(#imageLiteral(resourceName: "ic_flash_off"), for: .normal)
        }
            
        else {
            cameraController.flashMode = .on
            toggleFlashButton.setImage(#imageLiteral(resourceName: "Flash On Icon"), for: .normal)
        }
    }
    
    @IBAction func switchCameras(_ sender: UIButton) {
        do {
            try cameraController.switchCameras()
        }
            
        catch {
            print(error)
        }
    }
    
    @IBAction func captureImage(_ sender: UIButton) {
        if !isRecording {
            do {
                try cameraController.startVideoRecording()
                self.timerView.isHidden = false
                self.buttonCapture.setImage(#imageLiteral(resourceName: "video_recording"), for: .normal)
                self.isRecording = true
                self.startTimer()
                self.tabBarController?.tabBar.isHidden = true
            }
            catch {
                print(error)
                self.timerView.isHidden = true
            }
        }
        else {
            
            cameraController.stopRecording {(url, error) in
                guard url != nil else {
                    print(error ?? "Image capture error")
                    return
                }
                self.buttonCapture.setImage(#imageLiteral(resourceName: "video_still"), for: .normal)
                self.isRecording = false
                self.stopTimer()
                self.timerView.isHidden = true
                self.isRecording = true
                self.resetTimerToZero()
                self.tabBarController?.tabBar.isHidden = false
                DispatchQueue.main.async {
                    let stroyboard: UIStoryboard = UIStoryboard.init(name: "Post", bundle: nil)
                    let vc:VideoPreviewViewController = stroyboard.instantiateViewController(withIdentifier: "VideoPreviewViewController") as! VideoPreviewViewController
                    vc.isVideoRecording = true
                    vc.videoUrl = url
                    //                    vc.delegate = self.delegate
                    self.navigationController?.pushViewController(vc, animated: false)
                    
                }
            }
        }
    }
    
    
    @IBAction func videoMode(_ sender: UIButton){
        self.isPhotoMode = false
        self.isRecording = true
        self.buttonCapture.setImage(#imageLiteral(resourceName: "video_still"), for: .normal)
    }
    
    // MARK:- Timer Functions
    fileprivate func startTimer(){
        
        // if you want the timer to reset to 0 every time the user presses record you can uncomment out either of these 2 lines
        
        // timeSec = 0
        // timeMin = 0
        buttonBlink.alpha = 0.0
        UIView.animate(withDuration: 1, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {self.buttonBlink.alpha = 1.0}, completion: nil)
        // If you don't use the 2 lines above then the timer will continue from whatever time it was stopped at
        let timeNow = String(format: "%02d:%02d", timeMin, timeSec)
        showTimerLabel.text = timeNow
        
        stopTimer() // stop it at it's current time before starting it again
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] _ in
            self?.timerTick()
        }
    }
    @objc fileprivate func timerTick(){
        timeSec += 1
        
        if timeSec == 60{
            timeSec = 0
            timeMin += 1
        }
        
        let timeNow = String(format: "%02d:%02d", timeMin, timeSec)
        
        showTimerLabel.text = timeNow
    }
    @objc fileprivate func resetTimerToZero(){
        timeSec = 0
        timeMin = 0
        stopTimer()
    }
    
    // if you need to reset the timer to 0 and yourLabel.txt back to 00:00
    @objc fileprivate func resetTimerAndLabel(){
        
        resetTimerToZero()
        showTimerLabel.text = String(format: "%02d:%02d", timeMin, timeSec)
    }
    
    // stops the timer at it's current time
    @objc fileprivate func stopTimer(){
        timer?.invalidate()
    }
}
