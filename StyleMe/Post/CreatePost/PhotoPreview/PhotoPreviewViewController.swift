//
//  PreviewVC.swift
//  cence
//
//  Created by EL Group on 02/09/19.
//  Copyright © 2019 Vijendar. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Photos
import PhotosUI

enum FilterType : String {
    case Chrome = "CIPhotoEffectChrome"
    case Fade = "CIPhotoEffectFade"
    case Instant = "CIPhotoEffectInstant"
    case Noir = "CIPhotoEffectNoir"
    case Process = "CIPhotoEffectProcess"
    case Tonal = "CIPhotoEffectTonal"
    case Transfer =  "CIPhotoEffectTransfer"
    case Sepia = "CISepiaTone"
    case Mono = "CIPhotoEffectMono"
    case Invert = "CIColorInvert"
    case Monochrome = "CIColorMonochrome"
    case ColorCube = "CIColorCube"
}

protocol CameraAssetsDelegate: class {
    func getCapturedAssetsFromCamera(capturedAssets: PHAsset)
}

class PhotoPreviewViewController: UIViewController {
    
    @IBOutlet weak var originalImage: UIImageView!
    @IBOutlet weak var imageToFilter: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var filterCollectionView: UICollectionView!
    var delegate: CameraAssetsDelegate!
    var img: UIImage!
    var videoURL: URL!
    var isFrontCamera: Bool!
    var localUrl: String!
    var selectedIndex:IndexPath!
    
    // Array for the filters
    var arrFilter:[String] = ["CIPhotoEffectChrome","CIPhotoEffectFade","CIPhotoEffectInstant","CIPhotoEffectNoir","CIPhotoEffectProcess","CIPhotoEffectTonal","CIPhotoEffectTransfer","CISepiaTone","CIPhotoEffectMono","CIColorInvert","CIColorMonochrome","CIColorCube"]
    
    // Array for the associated names of the filters
    var arrFilterName:[String] = ["Normal","Chrome", "Fade", "Instant", "Noir", "Process", "Tonal","Transfer", "Sepia","Mono","Invert","Monochrome","Color Cube"]
    
    var player: AVPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        if self.img != nil  {
            setImagePreview(image: self.img)
        }
        if videoURL != nil {
            setVideoPreview(videoURL: self.videoURL)
        }
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: .main) { _ in
            self.player?.seek(to: CMTime.zero)
            self.player?.play()
        }
        selectedIndex = IndexPath.init(item: 0, section: 0)
        self.selectionOfFilter(index: selectedIndex.row)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customNavigationBar()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        let cell = getCell(index: selectedIndex.row)
        //
        //        self.filterButtonTapped(cell!.filterImageButton)
    }
    
    //MARK: Back button action
    @IBAction func backAction(_ sender: UIButton) {
        if let play = player {
            print("stopped")
            play.pause()
            player = nil
        }
        self.navigationController?.popViewController(animated: false)
    }
    
    
    //MARK: Method for getting Asset Url
    func getAssets(localURL: String) {
        let assets = PHAsset.fetchAssets(withLocalIdentifiers: [self.localUrl], options: nil)
        if assets.count > 0 {
            delegate.getCapturedAssetsFromCamera(capturedAssets: assets.object(at: 0))
        }
    }
    
    func setVideoPreview(videoURL: URL) {
        //        player = AVPlayer(url: URL(fileURLWithPath: videoURL.path))
        //        let playerLayer = AVPlayerLayer(player: player)
        //        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        //        previewView.layer.addSublayer(playerLayer)
        //        playerLayer.frame = self.view.layer.bounds
        //        player.play()
        //        self.bringSubViewToFront()
    }
    //MARK: Setting Image to preview
    func setImagePreview(image: UIImage) {
        //originalImage.image = UIImage(cgImage: image.cgImage!, scale: 1.0, orientation: .rightMirrored)
        
        if isFrontCamera {
            let img = UIImage(cgImage: image.cgImage!, scale: 1.0, orientation: .leftMirrored)
            originalImage.image = img
        }
        else {
            
            originalImage.image = image
        }
        
    }
    
}



extension PhotoPreviewViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    //MARK: CollectionView Delegate & Data Source
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFilterName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:FilterImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterImageCollectionViewCell", for: indexPath) as! FilterImageCollectionViewCell
        if selectedIndex.row == indexPath.row{
            cell.layer.borderColor = UIColor.init(red: 44.0/255.0, green: 117.0/255.0, blue: 246.0/255.0, alpha: 1.0).cgColor
            cell.layer.borderWidth = 2.0
            cell.filterNameLabel.backgroundColor = UIColor.init(red: 44.0/255.0, green: 117.0/255.0, blue: 246.0/255.0, alpha: 1.0)
            cell.filterNameLabel.textColor = UIColor.white
        }
        else{
            cell.layer.borderWidth = 0.0
            cell.filterNameLabel.backgroundColor = UIColor.white
            cell.filterNameLabel.textColor = UIColor.black
        }
        
        if indexPath.row == 0{
            var imagePreview:UIImage = UIImage.init()
            imagePreview = originalImage.image!
            cell.filterImageButton.tag = indexPath.row
            
            cell.filterImageButton.addTarget(self, action: #selector(PhotoPreviewViewController.filterButtonTapped(_:)), for: .touchUpInside)
            cell.filterNameLabel.text = arrFilterName[indexPath.row]
            if isFrontCamera{
                imagePreview = UIImage(cgImage: (originalImage.image?.cgImage)!, scale: 1.0, orientation: .leftMirrored)
            }
            
            cell.imageView.image = imagePreview
        }
        else{
            cell.filterImageButton.tag = indexPath.row
            cell.filterImageButton.addTarget(self, action: #selector(PhotoPreviewViewController.filterButtonTapped(_:)), for: .touchUpInside)
            
            // Create filters for each button
            let ciContext = CIContext(options: nil)
            let coreImage = CIImage(image: originalImage.image!)
            let filter = CIFilter(name: "\(arrFilter[indexPath.row - 1])" )
            filter!.setDefaults()
            filter!.setValue(coreImage, forKey: kCIInputImageKey)
            let filteredImageData = filter!.value(forKey: kCIOutputImageKey) as! CIImage
            let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
            var imagePreview:UIImage!
            
            if isFrontCamera{
                imagePreview = UIImage(cgImage: filteredImageRef!, scale: 1.0, orientation: .leftMirrored)
            }
            else{
                imagePreview = UIImage(cgImage: filteredImageRef!, scale: 1.0, orientation: originalImage.image!.imageOrientation)
            }
            
            // Assign filtered image to the button
            cell.imageView.image = imagePreview
            cell.filterNameLabel.text = arrFilterName[indexPath.row]
            
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width/3.5
        
        return CGSize(width: width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 2.0
    }
}
extension PhotoPreviewViewController{
    //MARK: Get Cell
    func getCell(index: Int) -> FilterImageCollectionViewCell? {
        guard let cell = self.filterCollectionView.cellForItem(at: IndexPath(row : index, section : 0)) as? FilterImageCollectionViewCell else {return nil}
        return cell
    }
    
    //MARK: Method for Selection of Selected Filter
    func selectionOfFilter(index:Int){
        
        if index == selectedIndex.row{
            return
        }
        else{
            let lastSelectedCell = getCell(index: selectedIndex.row)
            lastSelectedCell?.layer.borderWidth = 0.0
            lastSelectedCell?.filterNameLabel.backgroundColor = UIColor.white
            lastSelectedCell?.filterNameLabel.textColor = UIColor.black
            let selectedCell = getCell(index: index)
            selectedCell?.layer.borderColor = UIColor.init(red: 44.0/255.0, green: 117.0/255.0, blue: 246.0/255.0, alpha: 1.0).cgColor
            selectedCell?.layer.borderWidth = 2.0
            selectedCell?.filterNameLabel.backgroundColor = UIColor.init(red: 44.0/255.0, green: 117.0/255.0, blue: 246.0/255.0, alpha: 1.0)
            selectedCell?.filterNameLabel.textColor = UIColor.white
            selectedIndex.row = index
        }
    }
    
    //MARK: function for filter button Action
    
    @objc func filterButtonTapped(_ sender: UIButton) {
        let Cell = getCell(index: sender.tag)
        let button = sender as UIButton
        originalImage.isHidden = true
        imageToFilter.image = Cell?.imageView.image
        self.selectionOfFilter(index: button.tag)
    }
    
    
    //MARK: Custom Navigation Bar
    
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        backButton.setImage(UIImage.init(named: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(PhotoPreviewViewController.backButtonPressed(_:)), for: .touchUpInside)
        let leftItem = UIBarButtonItem()
        leftItem.customView = backButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "Filter"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem, fixedSpace,leftItem2]
        
        let nextButton = UIButton.init()
        nextButton.setTitle("NEXT", for: .normal)
        nextButton.setTitleColor(.red, for: .normal)
        nextButton.titleLabel?.font = UIFont.PoppinsSemiBold(ofSize: 15)
        nextButton.addTarget(self, action: #selector(PhotoPreviewViewController.nextButtonPressed(_:)), for: .touchUpInside)
        let rightItem = UIBarButtonItem()
        rightItem.customView = nextButton
        
        self.navigationItem.rightBarButtonItem = rightItem
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    //MARK: Back Button Action
    
    @objc func backButtonPressed(_ sender: Any){
        self.navigationController?.popViewController(animated: false)
    }
    
    //MARK: Next Button Action
    @objc func nextButtonPressed(_ sender:Any){
        if self.imageToFilter.image != nil {
            //Save image to gallery
            try? PHPhotoLibrary.shared().performChangesAndWait {
                let req: PHAssetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: self.imageToFilter.image!)
                self.localUrl = req.placeholderForCreatedAsset!.localIdentifier
                
            }
            let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.PostTabControllerStoryBoard, bundle: nil)
            let controller = story.instantiateViewController(withIdentifier: "NewPostTVC") as! NewPostTVC
            controller.imagePreview = self.imageToFilter.image
            controller.height = self.imageToFilter.frame.size.height
            controller.isVideo = false
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            try? PHPhotoLibrary.shared().performChangesAndWait {
                let req: PHAssetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: self.img)
                self.localUrl = req.placeholderForCreatedAsset!.localIdentifier
            }
            let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.PostTabControllerStoryBoard, bundle: nil)
            let controller = story.instantiateViewController(withIdentifier: "NewPostTVC") as! NewPostTVC
            controller.imagePreview = self.originalImage.image
            controller.height = self.originalImage.frame.size.height
            controller.isVideo = false
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
}
