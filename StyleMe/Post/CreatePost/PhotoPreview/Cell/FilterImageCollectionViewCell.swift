//
//  FilterImageCollectionViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 12/09/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class FilterImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var filterNameLabel: UILabel!
    @IBOutlet weak var filterImageButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
}
