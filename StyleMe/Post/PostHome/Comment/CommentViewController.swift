    //
    //  CommentViewController.swift
    //  StyleMe
    //
    //  Created by EL INFO on 18/02/20.
    //  Copyright © 2020 Admin. All rights reserved.
    //
    
    import UIKit
    import SDWebImage
    import AVKit
    
    class CommentViewController: UIViewController {
        
        @IBOutlet var sectionHeaderView: UIView!
        @IBOutlet weak var imageViewPost: UIImageView!
        @IBOutlet weak var imageViewUser: UIImageView!
        @IBOutlet weak var buttonFollow: UIButton!
        @IBOutlet weak var textViewDescription: UITextView!
        @IBOutlet weak var labelUserName: UILabel!
        @IBOutlet weak var labelDuration: UILabel!
        @IBOutlet weak var buttonLike: UIButton!
        @IBOutlet weak var labelLikeCount: UILabel!
        @IBOutlet weak var buttonComment: UIButton!
        @IBOutlet weak var labelCommentCount: UILabel!
        @IBOutlet weak var buttonShare: UIButton!
        @IBOutlet weak var labelShareCount: UILabel!
        @IBOutlet weak var baseView: UIView!
        @IBOutlet weak var footerView:UIView!
        @IBOutlet weak var footerContainer:UIView!
        @IBOutlet weak var imagreViewFooter:UIView!
        @IBOutlet weak var buttonPost:UIButton!
        @IBOutlet weak var textFieldComment:UITextField!
        @IBOutlet weak var tableViewComment:UITableView!
        @IBOutlet weak var canvasView:UIView!
        @IBOutlet weak var buttonPlay:UIButton!
        var commentCount:Int!
        var startTime = 0.0
        var player:AVPlayer!
        var playerViewController:AVPlayerViewController!
        var dictPost:[String:Any]!
        var image:UIImage!
        var arrComment:[Any] = [Any]()
        
        //MARK: View Controller Methods
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Do any additional setup after loading the view.
            //            let controller:PostTabBarController = self.tabBarController as! PostTabBarController
            //            controller.hideCustomTabBar()
            if let controller = self.tabBarController as? SalonTabBarController{
                controller.hideCustomTabBar()
            }
            else{
                let controller:PostTabBarController = self.tabBarController as! PostTabBarController
                controller.hideCustomTabBar()
            }
            self.customNavigationBar()
            self.getComments()
            registerForKeyboardWillShowNotification(self.tableViewComment)
            registerForKeyboardWillHideNotification(self.tableViewComment)
            
            /* use the above functions with
             block, in case you want the trigger just after the keyboard
             hide or show which will return you the keyboard size also.
             */
        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            buttonFollow.roundCorners(corners:[.allCorners], radius: 5.0)
            
            footerView.layer.cornerRadius = 10
            footerView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
            self.tableViewComment.layer.cornerRadius = 10.0
            self.tableViewComment.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            
            imageViewPost.layer.cornerRadius = 10
            imageViewPost.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            baseView.layer.cornerRadius = 10
            baseView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            
            tableViewComment.dropShadow(radius: 4.0, opacity: 0.4)
            
            imageViewUser.layer.cornerRadius = imageViewUser.frame.size.width/2
            imageViewUser.clipsToBounds = true
            let userId:Int = dictPost["user_id"] as Any as! Int
            
            let currentUserId:Int = UserDefaults.standard.integer(forKey: Constants.Login.userId)
            if currentUserId == userId{
                buttonFollow.isHidden = true
            }
            else{
                buttonFollow.isHidden = false
            }
            
            let isFollow:Bool = dictPost["is_follow"] as! Bool
            if isFollow{
                self.buttonFollow.setTitle("Following", for: .normal)
            }
            else{
                self.buttonFollow.setTitle("Follow", for: .normal)
            }
            textViewDescription.text = dictPost["description"] as? String
            textViewDescription.resizeForHeight()
            textViewDescription.resolveHashTags()
            labelUserName.text = dictPost["name"] as? String
            let likeCount:Int = dictPost["like_count"] as! Int
            labelLikeCount.text = "\(likeCount)"
            commentCount = (dictPost["comments_count"] as? Int)!
            labelCommentCount.text = "\(commentCount ?? 0)"
            let shareCount:Int = (dictPost["share_counts"] as? Int)!
            labelShareCount.text = "\(shareCount)"
            let isLiked:Bool = dictPost["is_liked"] as! Bool
            if isLiked{
                buttonLike.setImage(UIImage.init(named: "Like"), for: .normal)
            }
            else{
                buttonLike.setImage(UIImage.init(named: "dislike"), for: .normal)
            }
            let userImageString:String = dictPost["user_image"] as! String
            let userImageUrl:URL = URL.init(string: userImageString)!
            self.imageViewUser.sd_setImage(with: userImageUrl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
                if (cacheType == SDImageCacheType.none && image != nil) {
                    self.imageViewUser.alpha = 0;
                    UIView.animate(withDuration: 0.0, animations: { () -> Void in
                        self.imageViewUser.alpha = 1
                    })
                } else {
                    self.imageViewUser.alpha = 1;
                }
            })
            
            let arrImage:[Any] = dictPost["post_image"] as! [Any]
            let isVideo:Bool = dictPost["is_video"] as! Bool
            if arrImage.count > 0{
                let postImageUrlDictionary:[String:Any] = arrImage[0] as! [String:Any]
                let postImageUrlString:String = postImageUrlDictionary["url"] as! String
                let imageUrlString:String = Constants.apiConstants.baseUrlImage + postImageUrlString
                let imageUrl:URL = URL.init(string: imageUrlString)!
                
                if isVideo{
                    buttonPlay.isHidden = false
                    buttonPlay.addTarget(self, action: #selector(CommentViewController.playerView), for: .touchUpInside)
                    let thumbImagedict:[String:Any] = postImageUrlDictionary["thumb"] as! [String:Any]
                    var thumbImageString:String = thumbImagedict["url"] as! String
                    thumbImageString = Constants.apiConstants.baseUrlImage + thumbImageString
                    let thumbImageUrl = URL.init(string: thumbImageString)
                    imageViewPost.sd_setImage(with: thumbImageUrl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
                        if (cacheType == SDImageCacheType.none && image != nil) {
                            self.imageViewPost.alpha = 0;
                            UIView.animate(withDuration: 0.0, animations: { () -> Void in
                                self.imageViewPost.alpha = 1
                            })
                        } else {
                            self.imageViewPost.alpha = 1;
                        }
                    })
                }
                else{
                    buttonPlay.isHidden = true
                    self.imageViewPost.sd_setImage(with: imageUrl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
                        if (cacheType == SDImageCacheType.none && image != nil) {
                            self.imageViewPost.alpha = 0;
                            UIView.animate(withDuration: 0.0, animations: { () -> Void in
                                self.imageViewPost.alpha = 1
                            })
                        } else {
                            self.imageViewPost.alpha = 1;
                        }
                    })
                }
            }
            
            
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(delayedAction), userInfo: nil, repeats: false)
        }
        
        @objc func delayedAction(){
            sectionHeaderView.layoutIfNeeded()
        }
        
    }
    
    //MARK: Extension for Methods
    extension CommentViewController{
        //MARK: Custom Navigation Bar
        func customNavigationBar(){
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.navigationBar.barTintColor = .white
            let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
            fixedSpace.width = 20.0
            
            let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            backButton.setImage(UIImage.init(named: "back"), for: .normal)
            backButton.addTarget(self, action: #selector(NewPostViewController.backButtonPressed(_:)), for: .touchUpInside)
            let leftItem = UIBarButtonItem()
            leftItem.customView = backButton
            
            let labelTitle = UILabel()
            labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
            labelTitle.text = "Comment"
            let leftItem2 = UIBarButtonItem()
            leftItem2.customView = labelTitle
            self.navigationItem.leftBarButtonItems = [leftItem, fixedSpace,leftItem2]
            self.navigationController?.navigationBar.barTintColor = .white
        }
        //MARK: Back Button Action
        @objc func backButtonPressed(_ sender: Any){
            if let controller = self.tabBarController as? SalonTabBarController{
                controller.showCustomTabBar()
            }
            else{
                let controller:PostTabBarController = self.tabBarController as! PostTabBarController
                controller.showCustomTabBar()
            }
            self.navigationController?.popViewController(animated: true)
        }
        //MARK: Api for getting comments
        func getComments(){
            let isReachable = Reachability.isConnectedToNetwork()
            if isReachable{
                let postId:Int = dictPost["id"] as! Int
                var headerInfo:[String:String] = [String:String]()
                headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
                headerInfo["device-type"] = "ios"
                headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
                headerInfo["locale"] = "en"
                let strUrl:String = Constants.URLConstants.newPost + "/\(postId)" + Constants.URLConstants.postComment
                
                APIManager.getRequestForURLString(true, strUrl, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                    DispatchQueue.main.async {
                        if Success{
                            let resultDict = result as! [String:Any]
                            self.arrComment = resultDict["all_comments"] as! [Any]
                            self.tableViewComment.reloadData()
                        }
                        else{
                            let resultDict = result as! [String:Any]
                            
                            AppCustomClass.showToastAlert(Title:"", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                        }
                    }
                }
            }
            else{
                AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
            }
        }
        //MARK: Method for posting comment
        @IBAction func postComment(){
            let isReachable = Reachability.isConnectedToNetwork()
            if self.textFieldComment.text!.count > 0{
                self.textFieldComment.resignFirstResponder()
                if isReachable{
                    let postId:Int = dictPost["id"] as! Int
                    var headerInfo:[String:String] = [String:String]()
                    headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
                    headerInfo["device-type"] = "ios"
                    headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
                    headerInfo["locale"] = "en"
                    
                    var postDict:[String:Any] = [String:Any]()
                    postDict["description"] = textFieldComment.text
                    let strUrl:String = Constants.URLConstants.newPost + "/\(postId)" + Constants.URLConstants.postComment
                    APIManager.postRequestForURLString(true, strUrl, headerInfo, postDict) { (Done, error, Success, result, message, isActiveSession) in
                        DispatchQueue.main.async {
                            if Success{
                                self.getComments()
                                self.commentCount = self.commentCount + 1
                                self.labelCommentCount.text = "\(self.commentCount ?? 0)"
                                self.textFieldComment.text = ""
                                
                            }
                            else{
                                AppCustomClass.showToastAlert(Title:"", Message: message!, Controller: self, Time: 1)
                            }
                        }
                    }
                }
                else{
                    AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
                }
            }
            else{
                AppCustomClass.showToastAlert(Title: "", Message: "Please enter comment", Controller: self, Time: 1)
            }
        }
        
        //MARK: Method for like and dislike of comment
        @objc func commentLikeAndDislike( _ sender:Any){
            let tag:Int = (sender as AnyObject).tag
            let dictComment:[String:Any] = self.arrComment[tag] as! [String:Any]
            let isLiked:Bool = dictComment["is_liked"] as! Bool
            if isLiked{
                self.dislikeComment(index: tag)
            }
            else{
                self.likeComment(index: tag)
                
            }
        }
        
        //MARK: Api for like of comment
        @objc func likeComment(index:Int){
            let isReachable = Reachability.isConnectedToNetwork()
            if isReachable{
                let postId:Int = dictPost["id"] as! Int
                var headerInfo:[String:String] = [String:String]()
                headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
                headerInfo["device-type"] = "ios"
                headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
                headerInfo["locale"] = "en"
                
                var commentDict:[String:Any] = arrComment[index] as! [String : Any]
                let commentId:Int = commentDict["id"] as! Int
                
                ///posts/1/comment_posts/1/like_comments
                let strUrl:String = Constants.URLConstants.newPost + "/\(postId)" + "/comment_posts/\(commentId)/like_comments"
                APIManager.postRequestForURLString(true, strUrl, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                    DispatchQueue.main.async {
                        if Success{
                            
                            let commentCell = self.getCommentCell(index: index)
                            commentCell?.buttonLikeComment.setImage(UIImage.init(named: "Like"), for: .normal)
                            commentDict["is_liked"] = true
                            self.arrComment.remove(at: index)
                            self.arrComment.insert(commentDict, at: index)
                            
                        }
                        else{
                            let resultDict = result as! [String:Any]
                            
                            AppCustomClass.showToastAlert(Title:"", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                        }
                    }
                }
            }
            else{
                AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
            }
        }
        //MARK: Api for dislike comment
        @objc func dislikeComment(index:Int){
            let isReachable = Reachability.isConnectedToNetwork()
            if isReachable{
                let postId:Int = dictPost["id"] as! Int
                var headerInfo:[String:String] = [String:String]()
                headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
                headerInfo["device-type"] = "ios"
                headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
                headerInfo["locale"] = "en"
                var commentDict:[String:Any] = arrComment[index] as! [String : Any]
                let commentId:Int = commentDict["id"] as! Int
                
                ///posts/1/comment_posts/1/like_comments
                let strUrl:String = Constants.URLConstants.newPost + "/\(postId)" + "/comment_posts/\(commentId)/like_comments/1"
                
                APIManager.deleteRequestForURLString(true, strUrl, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                    DispatchQueue.main.async {
                        if Success{
                            
                            //  let resultDict = result as! [String:Any]
                            let commentCell = self.getCommentCell(index: index)
                            
                            commentCell?.buttonLikeComment.setImage(UIImage.init(named: "dislike"), for: .normal)
                            commentDict["is_liked"] = false
                            self.arrComment.remove(at: index)
                            self.arrComment.insert(commentDict, at: index)
                            
                            
                        }
                        else{
                            let resultDict = result as! [String:Any]
                            
                            AppCustomClass.showToastAlert(Title:"", Message: resultDict["message"] as! String, Controller: self, Time: 1)                    }
                    }
                }
            }
            else{
                AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
            }
        }
        
        //MARK: Button Like Post
        @IBAction func buttonLike( _sender:Any){
            let isReachable = Reachability.isConnectedToNetwork()
            if isReachable{
                
                let postId = self.dictPost["id"] as! Int
                
                var headerInfo:[String:String] = [String:String]()
                headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
                headerInfo["device-type"] = "ios"
                headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
                headerInfo["locale"] = "en"
                let strUrl:String = "posts/" + "\(postId)/like_posts"
                APIManager.postRequestForURLString(true, strUrl, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                    DispatchQueue.main.async {
                        if Success{
                            let resultDict = result as! [String:Any]
                            let isLiked:Bool = resultDict["is_liked"] as! Bool
                            if isLiked{
                                self.buttonLike.setImage(UIImage.init(named: "Like"), for: .normal)
                                
                            }
                            else{
                                self.buttonLike.setImage(UIImage.init(named: "dislike"), for: .normal)
                            }
                            let likeCount:Int = resultDict["like_count"] as! Int
                            self.labelLikeCount.text = String(likeCount)
                            self.dictPost["like_count"] = resultDict["like_count"]
                            self.dictPost["is_liked"] = isLiked
                            
                        }
                        else{
                            AppCustomClass.showToastAlert(Title:"", Message: message!, Controller: self, Time: 1)
                        }
                    }
                }
            }
            else{
                AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
            }
        }
        //MARK: Get Comment Cell
        func getCommentCell(index: Int) -> CommentTableViewCell? {
            guard let cell = self.tableViewComment.cellForRow(at: IndexPath(row : index, section : 0)) as? CommentTableViewCell else {return nil}
            return cell
        }
        func registerForKeyboardWillShowNotification(_ scrollView: UIScrollView, usingBlock block: ((CGSize?) -> Void)? = nil) {
            _ = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil, using: { notification -> Void in
                let userInfo = notification.userInfo!
                let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
                let contentInsets = UIEdgeInsets(top: scrollView.contentInset.top, left: scrollView.contentInset.left, bottom: keyboardSize.height, right: scrollView.contentInset.right)
                
                scrollView.setContentInsetAndScrollIndicatorInsets(contentInsets)
                block?(keyboardSize)
            })
        }
        
        //MARK: Follow button Method
        @IBAction func buttonFollowPressed( _sender : Any){
            let isFollow:Bool = dictPost["is_follow"] as! Bool
            if !isFollow{
                self.followUser()
            }
            else{
                self.unFollowUser()
            }
        }
        
        //MARK: Api request for follow user
        func followUser(){
            let isReachable = Reachability.isConnectedToNetwork()
            if isReachable{
                var headerInfo:[String:String] = [String:String]()
                headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
                headerInfo["device-type"] = "ios"
                headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
                headerInfo["locale"] = "en"
                
                let userId = dictPost["user_id"]
                var request:[String:Any] = [String:Any]()
                request["user_id"] = userId
                APIManager.postRequestForURLString(true, Constants.URLConstants.followUser, headerInfo,request ) { (Done, error, Success, result, message, isActiveSession) in
                    DispatchQueue.main.async {
                        if Success{
                            // AppCustomClass.showToastAlert(Title: "", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                            self.buttonFollow.setTitle("Following", for: .normal)
                            self.dictPost["is_follow"] = true
                            
                        }
                        else{
                            let resultDict = result as! [String:Any]
                            AppCustomClass.showToastAlert(Title:"", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                        }
                    }
                }
            }
            else{
                AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
                
            }
            
        }
        
        //MARK: Api request for unfollow user
        func unFollowUser(){
            let isReachable = Reachability.isConnectedToNetwork()
            if isReachable{
                var headerInfo:[String:String] = [String:String]()
                headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
                headerInfo["device-type"] = "ios"
                headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
                headerInfo["locale"] = "en"
                let userId = dictPost["user_id"]
                var request:[String:Any] = [String:Any]()
                request["user_id"] = userId
                APIManager.deleteRequestForURLString(true, Constants.URLConstants.unfollowUser, headerInfo, request) { (Done, error, Success, result, message, isActiveSession) in
                    DispatchQueue.main.async {
                        
                        if Success{
                            //    let resultDict = result as! [String:Any]
                            //  AppCustomClass.showToastAlert(Title: "", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                            self.buttonFollow.setTitle("Follow", for: .normal)
                            self.dictPost["is_follow"] = false
                        }
                        else{
                            AppCustomClass.showToastAlert(Title:"", Message: message!, Controller: self, Time: 1)
                        }
                    }
                }
            }
            else{
                AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
                
            }
        }
        
        func registerForKeyboardWillHideNotification(_ scrollView: UIScrollView, usingBlock block: ((CGSize?) -> Void)? = nil) {
            _ = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil, using: { notification -> Void in
                let userInfo = notification.userInfo!
                let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
                let contentInsets = UIEdgeInsets(top: scrollView.contentInset.top, left: scrollView.contentInset.left, bottom: 0, right: scrollView.contentInset.right)
                
                scrollView.setContentInsetAndScrollIndicatorInsets(contentInsets)
                block?(keyboardSize)
            })
        }
        
        
        @objc func playerView(){
            let arrImage:[Any] = dictPost["post_image"] as! [Any]
            let videoUrl:URL!
            if arrImage.count > 0{
                let postImageUrlDictionary:[String:Any] = arrImage[0] as! [String:Any]
                let postImageUrlString:String = postImageUrlDictionary["url"] as! String
                let imageUrlString:String = Constants.apiConstants.baseUrlImage + postImageUrlString
                videoUrl = URL.init(string: imageUrlString)!
                
                self.player = AVPlayer(url: videoUrl)
                self.player.rate = 1.0 //auto play
                let playerFrame = CGRect(x: 0, y: 0, width: self.canvasView.frame.width, height: self.canvasView.frame.height)
                self.playerViewController = AVPlayerViewController()
                self.playerViewController.player = self.player
                self.player.play()
                self.playerViewController.videoGravity = .resizeAspect
                self.playerViewController.view.frame = playerFrame
                self.playerViewController.showsPlaybackControls = false
                self.addChild(self.playerViewController)
                self.canvasView.addSubview(self.playerViewController.view)
                self.imageViewPost.isHidden = true
                self.buttonPlay.isHidden = true
                //            self.endTime = CMTimeGetSeconds((self.player.currentItem?.duration)!)
                
                self.playerViewController.didMove(toParent: self)
                self.player?.actionAtItemEnd = .none
                
                NotificationCenter.default.addObserver(self,
                                                       selector: #selector(self.playerItemDidReachEnd(notification:)),
                                                       name: .AVPlayerItemDidPlayToEndTime,
                                                       object: self.player?.currentItem)
            }
            let timeInterval: CMTime = CMTimeMakeWithSeconds(0.01, preferredTimescale: 100)
            //            self.timeObserver = self.player.addPeriodicTimeObserver(forInterval: timeInterval, queue: DispatchQueue.main) { (elapsedTime: CMTime) -> Void in self.observeTime(elapsedTime: elapsedTime) } as AnyObject?
        }
        
        @objc func playerItemDidReachEnd(notification: Notification) {
            if let playerItem = notification.object as? AVPlayerItem {
                let timescale = self.player.currentItem!.asset.duration.timescale
                let time = CMTimeMakeWithSeconds(self.startTime, preferredTimescale: timescale)
                
                playerItem.seek(to: time) { (success) in
                    self.player.pause()
                }
                self.imageViewPost.isHidden = false
                // playerItem.seek(to: CMTime.zero, completionHandler: nil)
                self.buttonPlay.isHidden = false
            }
        }
    }
    //MARK: Extension of Tableview Delegate and Datasource
    extension CommentViewController:UITableViewDelegate,UITableViewDataSource{
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrComment.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
            var comment:[String:Any] = [String:Any]()
            comment = arrComment[indexPath.row] as! [String:Any]
            let imageUrlString:String = comment["user_image"] as! String
            let description:String = comment["description"] as! String
            let userName:String = comment["user_name"] as! String
            let isLiked:Bool = comment["is_liked"] as! Bool
            if isLiked{
                cell.buttonLikeComment.setImage(UIImage.init(named: "Like"), for: .normal)
            }
            else{
                cell.buttonLikeComment.setImage(UIImage.init(named: "dislike"), for: .normal)
            }
            cell.buttonLikeComment.tag = indexPath.row
            cell.buttonLikeComment.addTarget(self, action: #selector(commentLikeAndDislike(_:)), for: .touchUpInside)
            cell.labelUserName.text = userName
            cell.labelComment.text = description
            cell.imageViewUser.layer.cornerRadius = cell.imageViewUser.frame.size.width/2
            cell.imageViewUser.clipsToBounds = true
            cell.imageViewUser.imageFromServerURL(imageUrlString, placeHolder: UIImage.init())
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            
            return sectionHeaderView
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            var frame = self.textViewDescription.frame
            frame.size.height = self.textViewDescription.contentSize.height
            self.textViewDescription.frame = frame
            return 250.0 + 42.0 + 50 + frame.height
        }
        
        func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            
            footerContainer.layer.cornerRadius = 25.0
            footerContainer.clipsToBounds = true
            self.imagreViewFooter.layer.cornerRadius = 15.0
            self.imagreViewFooter.clipsToBounds = true
            return footerView
        }
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return 70
        }
    }
    
    //MARK: Extension of TextField Delegate
    extension CommentViewController:UITextFieldDelegate{
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
    }
    
    extension UIScrollView {
        
        func setContentInsetAndScrollIndicatorInsets(_ edgeInsets: UIEdgeInsets) {
            self.contentInset = edgeInsets
            self.scrollIndicatorInsets = edgeInsets
        }
    }
