//
//  CommentTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 24/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelCommentTime: UILabel!
    @IBOutlet weak var labelComment: UILabel!
    @IBOutlet weak var buttonLikeComment: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
         //containerView.dropShadow(scale: true)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
