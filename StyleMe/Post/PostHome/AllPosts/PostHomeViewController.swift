//
//  PostHomeViewController.swift
//  StyleMe
//
//  Created by EL INFO on 16/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit

class PostHomeViewController: UIViewController {
    
    @IBOutlet weak var tableViewPost: UITableView!
    @IBOutlet weak var categoryCollectionView:UICollectionView!
    @IBOutlet var viewContainerSearch: UIView!
    @IBOutlet weak var textFieldSearch: UITextField!
    var activityIndicator: ActivityIndicator! = ActivityIndicator()
    var arrPost:[Any] = [Any]()
    var arrCategoryList:[Any] = [Any]()
    var containerView:UIView!
    var selectedCategoryIndex:IndexPath = IndexPath.init(row: 0, section: 0)
    var isCategorySelected = false
    var aboutToBecomeInvisibleCell = -1
    var avPlayerLayer: AVPlayerLayer!
    var firstLoad = true
    var visibleIP : IndexPath?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(getPost(_:)), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.init(red: 235.0/255.0, green: 65.0/255.0, blue: 72.0/255.0, alpha: 1.0)
        
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.registerTableViewCells()
        self.tableViewPost.addSubview(self.refreshControl)
        
        self.tableViewPost.rowHeight = UITableView.automaticDimension
        self.tableViewPost.estimatedRowHeight = 50
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.apiForPost(AnyObject.self)
        self.tableViewPost.beginUpdates()
        self.tableViewPost.setContentOffset( CGPoint(x: 0.0, y: 0.0), animated: false)
        self.tableViewPost.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 35.0, right: 0.0)
        self.tableViewPost.endUpdates()
        self.customNavigationBar()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        containerView.constraints.forEach { $0.isActive = false }
        containerView.removeFromSuperview()
        if isCategorySelected{
            let formerCell = self.getCategoryCell(index: selectedCategoryIndex.row)
            formerCell?.categoryImageView.layer.borderWidth = 0.0
            formerCell?.categoryImageView.layer.borderColor = UIColor.white.cgColor
            selectedCategoryIndex = IndexPath.init(row: 0, section: 0)
            isCategorySelected = false
        }
    }
    
}

//MARK: Extension for Collectionview cell
extension PostHomeViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrCategoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:CategoryCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
        let category:[String:Any] = self.arrCategoryList[indexPath.row] as! [String:Any]
        cell.categoryNameLabel.text = category["name"] as? String
        let imageUrlDict:[String:Any] = (category["image"] as? [String:Any])!
        if selectedCategoryIndex.row == indexPath.row{
            cell.categoryImageView.layer.borderWidth = 2.0
            cell.categoryImageView.layer.borderColor = UIColor.init(red: 235.0/255.0, green: 65.0/255.0, blue: 72.0/255.0, alpha: 1.0).cgColor
        }
        else{
            cell.categoryImageView.layer.borderWidth = 0.0
        }
        if !AppCustomClass.isNsnullOrNil(object: imageUrlDict["url"]  as AnyObject?){
            let imageUrlString:String = Constants.apiConstants.baseUrlImage + (imageUrlDict["url"] as? String)!
            let imageUrl:URL = URL.init(string: imageUrlString)!
            cell.categoryImageView.sd_setImage(with: imageUrl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
                if (cacheType == SDImageCacheType.none && image != nil) {
                    cell.categoryImageView.alpha = 0;
                    UIView.animate(withDuration: 0.0, animations: { () -> Void in
                        cell.categoryImageView.alpha = 1
                    })
                } else {
                    cell.categoryImageView.alpha = 1;
                }
            })
            cell.categoryImageView.imageFromServerURL(imageUrlString, placeHolder: UIImage.init())
            cell.categoryNameLabel.textColor = .white
        }
        else{
            cell.categoryNameLabel.textColor = .white
        }
        cell.viewOverlay.layer.cornerRadius = 45
        cell.viewOverlay.clipsToBounds = true
        cell.categoryImageView.layer.cornerRadius = 45
        cell.categoryImageView.clipsToBounds = true
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // let width = collectionView.frame.size.height - 4
        
        return CGSize(width: 110, height: 110)//collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{
            isCategorySelected = false
            self.apiForPost(AnyObject.self)
        }
        else if selectedCategoryIndex.row != indexPath.row{
            isCategorySelected = true
            let formerCell = self.getCategoryCell(index: selectedCategoryIndex.row)
            formerCell?.categoryImageView.layer.borderWidth = 0.0
            let cell = self.getCategoryCell(index: indexPath.row)
            cell?.categoryImageView.layer.borderWidth = 2.0
            cell?.categoryImageView.layer.borderColor = UIColor.init(red: 235.0/255.0, green: 65.0/255.0, blue: 72.0/255.0, alpha: 1.0).cgColor
            let category:[String:Any] = arrCategoryList[indexPath.row] as! [String:Any]
            let id:Int = category["id"] as! Int
            self.getCategoryPost(id: id)
            selectedCategoryIndex = IndexPath.init(row: indexPath.row, section: 0)
        }
        
    }
    
}

//MARK: Extension for TableView Delegate and DataSource
extension PostHomeViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath) as! PostTableViewCell
        let post = arrPost[indexPath.row] as! [String:Any]
        let description:String = post["description"] as! String
        //  let location:String = post["post_location"] as! String
        let userName:String = post["name"] as! String
        let userImageUrlString:String = post["user_image"] as! String
        let postImage:[Any] = post["post_image"] as! [Any]
        let isLiked:Bool = post["is_liked"] as! Bool
        let userId:Int = post["user_id"] as Any as! Int
        let currentUserId:Int = UserDefaults.standard.integer(forKey: Constants.Login.userId)
        
        if currentUserId == userId{
            cell.buttonFollow.isHidden = true
        }
        else{
            cell.buttonFollow.isHidden = false
        }
        if isLiked{
            cell.buttonLike.setImage(UIImage.init(named: "Like"), for: .normal)
        }
        else{
            cell.buttonLike.setImage(UIImage.init(named: "dislike"), for: .normal)
        }
        let isFollow:Bool = post["is_follow"] as! Bool
        if isFollow{
            cell.buttonFollow.setTitle("Following", for: .normal)
        }
        else{
            cell.buttonFollow.setTitle("Follow", for: .normal)
        }
        if !AppCustomClass.isNsnullOrNil(object: post["like_count"] as AnyObject){
            let likeCount:Int = (post["like_count"] as! NSNumber).intValue
            cell.labelLike.text = "\(likeCount)"
        }
        if !AppCustomClass.isNsnullOrNil(object: post["comments_count"] as AnyObject){
            let commentCount:Int = (post["comments_count"] as! NSNumber).intValue
            cell.LabelCommentCount.text = "\(commentCount)"
        }
        if !AppCustomClass.isNsnullOrNil(object: post["share_counts"] as AnyObject){
            let shareCount:Int = (post["share_counts"] as! NSNumber).intValue
            cell.labelShareCount.text = "\(shareCount)"
        }
        cell.textViewDescription.text = description
        cell.textViewDescription.textColor = .lightGray
        cell.textViewDescription.resolveHashTags()
        cell.labelNameUser.text = userName
        cell.buttonLike.tag = indexPath.row
        cell.buttonComment.tag = indexPath.row
        cell.buttonFollow.tag = indexPath.row
        cell.buttonFollow.addTarget(self, action: #selector(buttonFollowPressed(_sender:)), for: .touchUpInside)
        cell.buttonLike.addTarget(self, action: #selector(buttonLike(_sender:)), for: .touchUpInside)
        cell.buttonComment.addTarget(self, action: #selector(buttonCommentPressed( _sender:)), for: .touchUpInside)
        cell.imageViewUser.imageFromServerURL(userImageUrlString, placeHolder: UIImage.init())
        
        let isVideo:Bool = post["is_video"] as! Bool
        if postImage.count > 0{
            
            let postImageUrlDictionary:[String:Any] = postImage[0] as! [String:Any]
            let postImageUrlString:String = postImageUrlDictionary["url"] as! String
            let imageUrlString:String = Constants.apiConstants.baseUrlImage + postImageUrlString
            let imageUrl:URL = URL.init(string: imageUrlString)!
            if isVideo{
                cell.videoPlayerItem = AVPlayerItem.init(url: imageUrl)
                cell.imageViewPost.isHidden = true
                let thumbImagedict:[String:Any] = postImageUrlDictionary["thumb"] as! [String:Any]
                var thumbImageString:String = thumbImagedict["url"] as! String
                thumbImageString = Constants.apiConstants.baseUrlImage + thumbImageString
                let thumbImageUrl = URL.init(string: thumbImageString)
                cell.imageViewPost.sd_setImage(with: thumbImageUrl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
                    if (cacheType == SDImageCacheType.none && image != nil) {
                        cell.imageViewPost.alpha = 0;
                        UIView.animate(withDuration: 0.0, animations: { () -> Void in
                            cell.imageViewPost.alpha = 1
                        })
                    } else {
                        cell.imageViewPost.alpha = 1;
                    }
                })
                
            }
            else{
                cell.imageViewPost.isHidden = false
                cell.imageViewPost.sd_setImage(with: imageUrl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
                    if (cacheType == SDImageCacheType.none && image != nil) {
                        cell.imageViewPost.alpha = 0;
                        UIView.animate(withDuration: 0.0, animations: { () -> Void in
                            cell.imageViewPost.alpha = 1
                        })
                    } else {
                        cell.imageViewPost.alpha = 1;
                    }
                })
            }
        }
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let indexPaths = self.tableViewPost.indexPathsForVisibleRows
        var cells = [Any]()
        for ip in indexPaths!{
            if let videoCell = self.tableViewPost.cellForRow(at: ip) as? PostTableViewCell{
                cells.append(videoCell)
            }
        }
        let cellCount = cells.count
        if cellCount == 0 {return}
        if cellCount == 1{
            if visibleIP != indexPaths?[0]{
                visibleIP = indexPaths?[0]
            }
            if let videoCell = cells.last! as? PostTableViewCell{
                self.playVideoOnTheCell(cell: videoCell, indexPath: (indexPaths?.last)!)
            }
        }
        if cellCount >= 2 {
            for i in 0..<cellCount{
                let cellRect = self.tableViewPost.rectForRow(at: (indexPaths?[i])!)
                let intersect = cellRect.intersection(self.tableViewPost.bounds)
                //                curerntHeight is the height of the cell that
                //                is visible
                let currentHeight = intersect.height
                print("\n \(currentHeight)")
                let cellHeight = (cells[i] as AnyObject).bounds.size.height
                //                0.95 here denotes how much you want the cell to display
                //                for it to mark itself as visible,
                //                .95 denotes 95 percent,
                //                you can change the values accordingly
                if currentHeight > (cellHeight * 0.95){
                    if visibleIP != indexPaths?[i]{
                        visibleIP = indexPaths?[i]
                        print ("visible = \(indexPaths?[i])")
                        if let videoCell = cells[i] as? PostTableViewCell{
                            self.playVideoOnTheCell(cell: videoCell, indexPath: (indexPaths?[i])!)
                        }
                    }
                }
                else{
                    if aboutToBecomeInvisibleCell != indexPaths?[i].row{
                        aboutToBecomeInvisibleCell = (indexPaths?[i].row)!
                        if let videoCell = cells[i] as? PostTableViewCell{
                            self.stopPlayBack(cell: videoCell, indexPath: (indexPaths?[i])!)
                        }
                        
                    }
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("end = \(indexPath)")
        if let videoCell = cell as? PostTableViewCell {
            videoCell.stopPlayback()
        }
    }
}

//MARK: PostHomeViewController Methods
extension PostHomeViewController{
    
    //MARK: Register TableView Custom cells
    func registerTableViewCells(){
        let postCell = UINib.init(nibName: "PostTableViewCell", bundle: nil)
        self.tableViewPost.register(postCell, forCellReuseIdentifier: "PostTableViewCell")
    }
    
    //MARK: Method for Getting post
    @objc func getPost( _ sender:Any){
        if !isCategorySelected{
            self.apiForPost(sender)
        }
        else{
            let category:[String:Any] = self.arrCategoryList[selectedCategoryIndex.row] as! [String:Any]
            let id:Int = category["id"] as! Int
            self.getCategoryPost(id: id)
        }
    }
    //MARK: API request for getting all Posts
    @objc func apiForPost( _ sender:Any){
        print(sender)
        var isRefresh:Bool!
        if let obj = sender as? UIRefreshControl {
            print(obj)
            isRefresh = true
        }
        else{
            isRefresh = false
            print(sender)
        }
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            if !isRefresh{
                self.activityIndicator.showActivityIndicator(uiView: self.view)
            }
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            APIManager.getRequestForURLString(true, "posts", headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    if !isRefresh{
                        self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    }
                    if Success{
                        let resultDict = result as! [String:Any]
                        self.arrPost = (resultDict["all_posts"] as? [Any])!
                        self.arrCategoryList = resultDict["category_list"] as! [Any]
                        var dict:[String:Any] = [String:Any]()
                        var urlDict:[String:Any] = [String:Any]()
                        urlDict["url"] = ""
                        dict["id"] = 12345
                        dict["name"] = "All Post"
                        dict["image"] = urlDict
                        self.arrCategoryList.insert(dict, at: 0)
                        self.categoryCollectionView.reloadData()
                        
                        self.tableViewPost.reloadData()
                        self.visibleIP = IndexPath.init(row: 0, section: 0)
                        self.refreshControl.endRefreshing()
                    }
                    else{
                        AppCustomClass.showToastAlert(Title:"", Message: "Something went wrong", Controller: self, Time: 1)
                        self.refreshControl.endRefreshing()
                        
                    }
                }
            }
        }
        else{
            
            Timer.scheduledTimer(withTimeInterval: 1.1, repeats: false) { (timer) in
                self.tableViewPost.reloadData()
                self.refreshControl.endRefreshing()
            }
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    
    //MARK: Comment Button Action
    @objc func buttonCommentPressed( _sender:Any){
        let button:UIButton = _sender as! UIButton
        
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.PostTabControllerStoryBoard, bundle: nil)
        if #available(iOS 13.0, *) {
            let controller = story.instantiateViewController(identifier: "CommentViewController") as! CommentViewController
            controller.dictPost = arrPost[button.tag] as? [String:Any]
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = story.instantiateViewController(withIdentifier:"CommentViewController") as! CommentViewController
            controller.dictPost = arrPost[button.tag] as? [String:Any]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    //MARK: Like Button action
    @objc func buttonLike( _sender:Any){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            let tag:Int = (_sender as AnyObject).tag
            var dict:[String:Any] = arrPost[tag] as! [String:Any]
            let postId = dict["id"] as! Int
            
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            let strUrl:String = "posts/" + "\(postId)/like_posts"
            APIManager.postRequestForURLString(true, strUrl, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    if Success{
                        let resultDict = result as! [String:Any]
                        let postCell = self.getCell(index: tag)
                        let isLiked:Bool = resultDict["is_liked"] as! Bool
                        if isLiked{
                            postCell?.buttonLike.setImage(UIImage.init(named: "Like"), for: .normal)
                            
                        }
                        else{
                            postCell?.buttonLike.setImage(UIImage.init(named: "dislike"), for: .normal)
                        }
                        let likeCount:Int = resultDict["like_count"] as! Int
                        postCell?.labelLike.text = String(likeCount)
                        dict["like_count"] = resultDict["like_count"]
                        dict["is_liked"] = isLiked
                        self.arrPost.remove(at: tag)
                        self.arrPost.insert(dict, at: tag)
                        
                    }
                    else{
                        AppCustomClass.showToastAlert(Title:"", Message: message!, Controller: self, Time: 1)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    
    //MARK: Getting TableView Cells
    func getCell(index: Int) -> PostTableViewCell? {
        guard let cell = self.tableViewPost.cellForRow(at: IndexPath(row : index, section : 0)) as? PostTableViewCell else {return nil}
        return cell
    }
    
    //MARK: Getting CollectionView Cells
    func getCategoryCell(index: Int) -> CategoryCollectionViewCell? {
        guard let cell = self.categoryCollectionView.cellForItem(at: IndexPath(row : index, section : 0)) as? CategoryCollectionViewCell else {return nil}
        return cell
    }
    
    //MARK: Method for Custom Navigation Bar
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
        
        containerView = UIView.init(frame: CGRect.init(x: 20, y: UIApplication.shared.statusBarFrame.height
            + 5, width: UIScreen.main.bounds.size.width - 40, height: (self.navigationController?.navigationBar.frame.height)! - 5))
        self.navigationController?.view.addSubview(containerView)
        containerView.addSubview(viewContainerSearch)
        containerView.backgroundColor = UIColor.init(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        containerView.layer.cornerRadius = ((self.navigationController?.navigationBar.frame.height)! - 5)/2
        containerView.clipsToBounds = true
        
    }
    
    //MARK: Api Request for getting category based post
    func getCategoryPost(id:Int){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            let strUrl:String = "/filter_post?category_id=\(id)"
            APIManager.getRequestForURLString(true, strUrl, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    
                    if Success{
                        let resultDict = result as! [String:Any]
                        self.arrPost = (resultDict["all_posts"] as? [Any])!
                        self.tableViewPost.reloadData()
                        self.refreshControl.endRefreshing()
                    }
                    else{
                        AppCustomClass.showToastAlert(Title:"", Message: message!, Controller: self, Time: 1)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
            
        }
    }
    //MARK: Follow button Method
    @objc func buttonFollowPressed( _sender : Any){
        let tag = (_sender as AnyObject).tag
        let dict:[String:Any] = arrPost[tag!] as! [String:Any]
        let isFollow:Bool = dict["is_follow"] as! Bool
        if !isFollow{
            self.followUser(index: tag!)
        }
        else{
            self.unFollowUser(index: tag!)
        }
    }
    
    //MARK: Api request for follow user
    func followUser(index:Int){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            var dict:[String:Any] = arrPost[index] as! [String:Any]
            let userId = dict["user_id"]
            var request:[String:Any] = [String:Any]()
            request["user_id"] = userId
            APIManager.postRequestForURLString(true, Constants.URLConstants.followUser, headerInfo,request ) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    if Success{
                        let postCell = self.getCell(index: index)
                        // AppCustomClass.showToastAlert(Title: "", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                        postCell?.buttonFollow.setTitle("Following", for: .normal)
                        dict["is_follow"] = true
                        self.arrPost.remove(at: index)
                        self.arrPost.insert(dict, at: index)
                        
                    }
                    else{
                        let resultDict = result as! [String:Any]
                        AppCustomClass.showToastAlert(Title:"", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
            
        }
        
    }
    
    //MARK: Api request for unfollow user
    func unFollowUser(index:Int){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            var dict:[String:Any] = arrPost[index] as! [String:Any]
            let userId = dict["user_id"]
            var request:[String:Any] = [String:Any]()
            request["user_id"] = userId
            APIManager.deleteRequestForURLString(true, Constants.URLConstants.unfollowUser, headerInfo, request) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    if Success{
                        //    let resultDict = result as! [String:Any]
                        let postCell = self.getCell(index: index)
                        //  AppCustomClass.showToastAlert(Title: "", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                        postCell?.buttonFollow.setTitle("Follow", for: .normal)
                        dict["is_follow"] = false
                        self.arrPost.remove(at: index)
                        self.arrPost.insert(dict, at: index)
                    }
                    else{
                        AppCustomClass.showToastAlert(Title:"", Message: message!, Controller: self, Time: 1)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
            
        }
    }
    func playVideoOnTheCell(cell : PostTableViewCell, indexPath : IndexPath){
        cell.startPlayback()
    }
    
    func stopPlayBack(cell : PostTableViewCell, indexPath : IndexPath){
        cell.stopPlayback()
    }
    
    
}

//MARK: Extension for Textfield Delegate
extension PostHomeViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.PostTabControllerStoryBoard, bundle: nil)
        if #available(iOS 13.0, *) {
            let controller = story.instantiateViewController(identifier: "SearchPostTableViewController") as! SearchPostTableViewController
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = story.instantiateViewController(withIdentifier:"SearchPostTableViewController") as! SearchPostTableViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
