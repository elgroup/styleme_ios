//
//  PostTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 14/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVKit

class PostTableViewCell: UITableViewCell {
    @IBOutlet weak var videoPlayerSuperView:UIView!
    @IBOutlet weak var imageViewPost: UIImageView!
    @IBOutlet weak var buttonFollow: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var buttonLike: UIButton!
    @IBOutlet weak var labelLike: UILabel!
    @IBOutlet weak var buttonComment: UIButton!
    @IBOutlet weak var LabelCommentCount: UILabel!
    @IBOutlet weak var labelShareCount: UILabel!
    @IBOutlet weak var buttonShare: UIButton!
    @IBOutlet weak var textViewDescription: UITextView!
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var labelNameUser: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var shadowView:UIView!
    var dictPost:[String:Any]!
    var avPlayer: AVPlayer?
    var avPlayerLayer: AVPlayerLayer?
    var paused: Bool = false
    var playerItem: AVPlayerItem!
    // Key-value observing context
    private var playerItemContext = 0
    
    let requiredAssetKeys = [
        "playable",
        "hasProtectedContent"
    ]
    //This will be called everytime a new value is set on the videoplayer item
    var videoPlayerItem: AVPlayerItem? = nil {
        didSet {
            /*
             If needed, configure player item here before associating it with a player.
             (example: adding outputs, setting text style rules, selecting media options)
             */
            avPlayer?.replaceCurrentItem(with: self.videoPlayerItem)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        buttonFollow.roundCorners(corners:[.allCorners], radius: 5.0)
        containerView.layer.cornerRadius = 10.0
        containerView.clipsToBounds = true
        imageViewUser.layer.cornerRadius = imageViewUser.frame.size.width/2
        imageViewUser.clipsToBounds = true
        shadowView.dropShadow(scale: true, radius: 4.0,opacity: 0.4)
        //        let isVideo:Bool = dictPost["is_video"] as! Bool
        //        if isVideo{
        self.setupMoviePlayer()
        //}
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func setupMoviePlayer(){
        self.avPlayer = AVPlayer.init(playerItem: self.videoPlayerItem)
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        avPlayer?.volume = 10
        avPlayer?.actionAtItemEnd = .none
        self.backgroundColor = .clear
        avPlayerLayer?.frame = self.videoPlayerSuperView.bounds
        self.videoPlayerSuperView.layer.insertSublayer(avPlayerLayer!, at: 0)
        //  return ((player.currentItem?.asset) as? AVURLAsset)?.url
        
        // let asset = avPlayer?.currentItem?.asset
        self.avPlayerLayer?.backgroundColor = (UIColor.black).cgColor
        //playerItem = AVPlayerItem(asset: asset!,automaticallyLoadedAssetKeys: requiredAssetKeys)
        // Register as an observer of the player item's status property
        // self.videoPlayerItem!.addObserver(self,forKeyPath: #keyPath(AVPlayerItem.status),options: [.old, .new],context: &playerItemContext)
        // This notification is fired when the video ends, you can handle it in the method.
       // self.avPlayer!.addObserver(self, forKeyPath: "rate", options: NSKeyValueObservingOptions.new, context: nil)

        NotificationCenter.default.addObserver(self,selector: #selector(self.playerItemDidReachEnd(notification:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayer?.currentItem)
    }
    
    func stopPlayback(){
        self.avPlayer?.pause()
    }
    
    func startPlayback(){
        self.avPlayer?.play()
    }
    
    // A notification is fired and seeker is sent to the beginning to loop the video again
    @objc func playerItemDidReachEnd(notification: Notification) {
        let p: AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: CMTime.zero, completionHandler: nil)
    }
    
    func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutableRawPointer) {
        if keyPath == "rate" {
            if let rate = change as? Float {
                if avPlayer?.currentItem!.status == AVPlayerItem.Status.readyToPlay{
                         if rate != 0 && avPlayer!.error == nil {

                           print("normal playback")
//                           timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(ChannelDetailViewController.somAction), userInfo: nil, repeats: true)

                         }
                         else{
                           //timer?.invalidate()
                           print("movie player stopped")
                         }

                }else if avPlayer!.currentItem?.status == AVPlayerItem.Status.unknown{
//                         timer?.invalidate()
                         print("not ready to play")
                       }
                     }
            }
        }
    }
    
//    override func observeValue(forKeyPath keyPath: String?,of object: Any?,change: [NSKeyValueChangeKey : Any]?,context: UnsafeMutableRawPointer?) {
//
//        // Only handle observations for the playerItemContext
//        guard context == &playerItemContext else {
//            super.observeValue(forKeyPath: keyPath,
//                               of: object,
//                               change: change,
//                               context: context)
//            return
//        }
//
//        if keyPath == #keyPath(AVPlayerItem.status) {
//            let status: AVPlayerItem.Status
//            if let statusNumber = change?[.newKey] as? NSNumber {
//                status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
//            } else {
//                status = .unknown
//            }
//
//            // Switch over status value
//            switch status {
//            case .readyToPlay:
//                imageViewPost.isHidden = true
//                print("=============READY TO PLAY================")
//                break
//            // Player item is ready to play.
//            case .failed:
//                break
//            // Player item failed. See error.
//            case .unknown:
//                break
//            // Player item is not yet ready.
//            @unknown default:
//                break
//            }
//        }
//    }
//}


