//
//  CategoryCollectionViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 27/09/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var viewOverlay: UIView!
}
