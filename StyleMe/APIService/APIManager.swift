//
//  APIServices.swift
//  StyleMe
//
//  Created by Admin on 01/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
class APIManager: NSObject {
    
    static let sharedInstance = APIManager()
    
    private override init() {}
    //// for post request ////
    static func postRequestForURLString(_ useBaseString: Bool, _ apiString:String, _ headerInfo: Dictionary<String, String>, _ parameters:Dictionary<String, Any>?, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) {
        
        var params = [String:Any]()
        if parameters != nil {
            
            params = Dictionary()
            parameters?.forEach { (arg) in
                
                let (k, v) = arg
                params[k] = v
            }
        }
        
        APIManager.requestForURLString(useBaseString, apiString, "POST", headerInfo, params, complitionHandler: complitionHandler);
    }
    
    //// for post request ////
    static func putRequestForURLString(_ useBaseString: Bool, _ apiString:String, _ headerInfo: Dictionary<String, String>, _ parameters:Dictionary<String, Any>?, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) {
        
        var params = [String:Any]()
        if parameters != nil {
            
            params = Dictionary()
            parameters?.forEach { (arg) in
                
                let (k, v) = arg
                params[k] = v
            }
        }
        
        APIManager.requestForURLString(useBaseString, apiString, "PUT", headerInfo, params, complitionHandler: complitionHandler);
    }
    
    //// for delete request ////
    static func deleteRequestForURLString(_ useBaseString: Bool, _ apiString:String, _ headerInfo: Dictionary<String, String>, _ parameters:Dictionary<String, Any>?, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) {
        
        var params = [String:Any]()
        if parameters != nil {
            
            params = Dictionary()
            parameters?.forEach { (arg) in
                
                let (k, v) = arg
                params[k] = v
            }
        }
        
        APIManager.requestForURLString(useBaseString, apiString, "DELETE", headerInfo, params, complitionHandler: complitionHandler);
    }
    
    //// for get request ////
    static func getRequestForURLString(_ useBaseString: Bool, _ apiString:String,_ headerInfo: Dictionary<String, String>, _ parameters:Dictionary<String, Any>?, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) {
        
        var params = [String:Any]()
        if parameters != nil {
            
            params = Dictionary()
            parameters?.forEach { (arg) in
                
                let (k, v) = arg
                params[k] = v
            }
        }
        
        APIManager.requestForURLString(useBaseString, apiString, "GET", headerInfo, params, complitionHandler: complitionHandler);
    }
    
    //// for all request ////
    static func requestForURLString(_ useBaseString: Bool, _ apiString:String, _ httpMehtod:String, _ headerInfo: Dictionary<String, String>, _ parameters:Dictionary<String, Any>, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) {
        
        var requestData: Data? = nil
        var paramListString: String = ""
        
        if httpMehtod.uppercased().compare("GET") == ComparisonResult.orderedSame {
            for key in parameters.keys {
                var value:Any! = ""
                value = parameters[key]
                paramListString = paramListString + "\(key)=" + (value as AnyObject).description + "&"
            }
            paramListString = paramListString.trimmingCharacters(in: CharacterSet.init(charactersIn: "&"))
        }
        else {
            
            do {
                
                let data:Data = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                requestData = data
            }
            catch {
                
                print("data is not found")
            }
        }
        
        var urlString: String = ""
        
        if httpMehtod.uppercased().compare("POST") == ComparisonResult.orderedSame {
            
            if useBaseString {
                
                urlString = Constants.apiConstants.baseURL + apiString
            }
            else {
                
                urlString = apiString
            }
        }
        else {
            
            if useBaseString {
                
                if paramListString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count > 0 {
                    
                    urlString = Constants.apiConstants.baseURL + apiString + "?" + paramListString
                }
                else {
                    
                    urlString = Constants.apiConstants.baseURL + apiString
                }
            }
            else {
                
                if paramListString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count > 0 {
                    
                    urlString = apiString + "?" + paramListString
                }
                else {
                    
                    urlString = apiString
                }
            }
        }
        
        let url:URL = URL.init(string: urlString)!
        let request:NSMutableURLRequest = NSMutableURLRequest.init(url: url, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: 30)
        request.httpMethod = httpMehtod
        request.setValue("application/json",              forHTTPHeaderField: Constants.apiConstants.HTTPHeaderFieldContentType)
        request.setValue(Constants.apiConstants.deviceType, forHTTPHeaderField: Constants.apiConstants.HTTPHeaderFieldDeviceType)
        request.setValue(NSTimeZone.local.abbreviation(), forHTTPHeaderField: Constants.apiConstants.HTTPHeaderFieldTimeZone)
        
        for key:String in headerInfo.keys {
            
            request.setValue(headerInfo[key], forHTTPHeaderField: key)
        }
        
        if requestData != nil {
            
            request.httpBody = requestData
        }
        
        //        request.setValue(UserDefaults.standard.value(forKey: key_device_token) as! String, forHTTPHeaderField: key_device_token)
        
        print("Method : \(request.httpMethod)")
        print("URL : \(url)")
        print("Header : \(headerInfo.description)")
        print("Parameters : \(parameters.description)")
        //        return
        let session:URLSession = URLSession.shared
        let task:URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if data == nil {
                
                print("Data is nil from api : ", urlString)
                complitionHandler(false, error, false, nil, "", true)
            }
            else if response == nil {
                
                print("Response is nil from api : ", urlString)
                complitionHandler(false, error, false, nil, "", true)
            }
            else {
                
                do {
                    
                    let strTest:String = String.init(data: data!, encoding: .utf8)!
                    print("response : " + strTest)
                    
                    if strTest.count == 0 {
                        
                        print("Data is blank or nill from \(url)")
                    }
                    
                    let result:Any = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if result is Dictionary<String, Any> {
                        
                        //                        {"errors":[]}
                        
                        if ((result as! Dictionary<String, Any>)["errors"] != nil) &&  ((result as! Dictionary<String, Any>)["errors"] is Array<Dictionary<String, String>>) && ((result as! Dictionary<String, Any>)["errors"] as! Array<Dictionary<String, String>>).count > 0 && (((result as! Dictionary<String, Any>)["errors"] as! Array<Dictionary<String, String>>)[0]["detail"]?.lowercased().elementsEqual("access denied"))! {
                            
                            complitionHandler(true, error, false, result, "", false)
                            return
                        }
                        
                        var successResultFound: Bool = false
                        
                        //// for success result ////
                        if ((result as! Dictionary<String, Any>)[Constants.apiConstants.apiResultSuccessKey] != nil) {
                            
                            let number:NSNumber = (((result as! Dictionary<String, Any>)[Constants.apiConstants.apiResultSuccessKey]) as! NSNumber)
                            successResultFound = number.boolValue
                        }
                        
                        if ((result as! Dictionary<String, Any>)[Constants.apiConstants.apiResultStatusKey] != nil) {
                            
                            let number:NSNumber = (((result as! Dictionary<String, Any>)[Constants.apiConstants.apiResultStatusKey]) as! NSNumber)
                            successResultFound = (successResultFound || number.boolValue)
                        }
                        //// ***************** ////
                        
                        complitionHandler(true, error, successResultFound, result, "", true)
                    }
                    else {
                        
                        let msg: String = String.init(data: data!, encoding: String.Encoding.utf8)!
                        complitionHandler(true, error, false, nil, msg, true)
                    }
                }
                catch {
                    
                    complitionHandler(false, error, false, nil, "Data formate error", true)
                }
            }
        }
        task.resume()
    }
    
    static func postRequestWithMultipartForImages(postData:[String:Any],completionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void{
        let strAuth =  UserDefaults.standard.string(forKey: Constants.Login.authToken)
        let parameters:[String: AnyObject] = [
            "post[category_id]": postData["category_id"] as AnyObject,
            "post[post_location]": postData["post_location"] as AnyObject,
            "post[description]": postData["description"] as AnyObject,
            "post[is_video]": postData["is_video"] as AnyObject,
        ]
        print(parameters)
        let str = String(format:"%@%@", Constants.apiConstants.baseURL, "posts" )
        let url = NSURL(string: str)
        var request   = URLRequest(url:url! as URL)
        request.httpMethod = "POST"
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.addValue(strAuth!, forHTTPHeaderField: "auth-token")
        request.addValue("ios", forHTTPHeaderField: "device-type")
        request.addValue(UIDevice.current.identifierForVendor!.uuidString, forHTTPHeaderField: "device-id")
        let isVideo:Bool = postData["is_video"] as! Bool
        var mimetype:String!
        var  strFileName:String!
        if isVideo{
            mimetype = "video/mp4"
            strFileName = String(format:"Video%@.mp4","1234567")
        }
        else{
            mimetype = "image/png"
            strFileName = String(format:"Image%@.png","1234567")
        }
        
        request.httpBody = createBody(parameters: parameters ,
                                      boundary: boundary,
                                      data: postData["post_attachments_attributes"] as! [Data],
                                      mimeType: mimetype,
                                      filename: strFileName)
        
        let session = URLSession.shared
        let task:URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if data == nil {
                
                completionHandler(false, error, false, nil, "", true)
            }
            else if response == nil {
                
                completionHandler(false, error, false, nil, "", true)
            }
            else {
                
                do {
                    
                    let strTest:String = String.init(data: data!, encoding: .utf8)!
                    //print(strTest)
                    
                    if strTest.count == 0 {
                        
                        //print("Data is blank or nill from \(url)")
                    }
                    
                    let result:Any = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if result is Dictionary<String, Any> {
                        
                        if ((result as! Dictionary<String, Any>)["errors"] != nil) &&  ((result as! Dictionary<String, Any>)["errors"] is String) && ((result as! Dictionary<String, Any>)["errors"] as! String).elementsEqual("Access denied"){
                            
                            completionHandler(true, error, false, result, "", false)
                            return
                        }
                        
                        var successResultFound: Bool = false
                        var userSessionActive: Bool  = true
                        
                        if ((result as! Dictionary<String, Any>)["success"] != nil) {
                            
                            let number:NSNumber = (((result as! Dictionary<String, Any>)["success"]) as! NSNumber)
                            let error: Any? = (result as! Dictionary<String, Any>)["success"]
                            successResultFound = number.boolValue
                            
                            if error != nil && error is String && (error as! String).elementsEqual("Access denied") {
                                
                                //print("----- making user inactive")
                                userSessionActive = false
                            }
                        }
                        
                        completionHandler(true, error, successResultFound, result, "", userSessionActive)
                    }
                    else {
                        
                        let msg: String = String.init(data: data!, encoding: String.Encoding.utf8)!
                        completionHandler(true, error, false, nil, msg, true)
                    }
                }
                catch {
                    completionHandler(false, error, false, nil, error as? String, true)
                }
            }
        }
        task.resume()
    }
    
    
    
    static  func createBody(parameters: [String: AnyObject],boundary: String, data: [Data],mimeType: String, filename: String) -> Data{
        let body = NSMutableData()
        
        for (key, value) in parameters {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        body.appendString("--\(boundary)\r\n")
        for imageData in data {
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\("post[post_attachments_attributes][][avatar]")\"; filename=\"\(filename)\"\r\n")
            body.appendString("Content-Type: \(mimeType)\r\n\r\n")
            body.append(imageData)
            body.appendString("\r\n")
        }
        body.appendString("--\(boundary)--\r\n")
        return body as Data
    }
    
    
    
    static func putRequestWithMultipart(postData:[String:Any],completionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void{
        let strAuth =  UserDefaults.standard.string(forKey: Constants.Login.authToken)
        //        let parameters:[String: AnyObject] = [
        //            "post[category_id]": postData["category_id"] as AnyObject,
        //            "post[post_location]": postData["post_location"] as AnyObject,
        //            "post[description]": postData["description"] as AnyObject,
        //        ]
        let parameters:[String:AnyObject] = postData as [String:AnyObject]
        print(parameters)
        let str = String(format:"%@%@", Constants.apiConstants.baseURL, Constants.URLConstants.getProfile )
        let url = NSURL(string: str)
        var request   = URLRequest(url:url! as URL)
        request.httpMethod = "PUT"
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.addValue(strAuth!, forHTTPHeaderField: "auth-token")
        request.addValue("ios", forHTTPHeaderField: "device-type")
        request.addValue(UIDevice.current.identifierForVendor!.uuidString, forHTTPHeaderField: "device-id")
        
        let mimetype = "image/png"
        let userName:String = UserDefaults.standard.string(forKey: Constants.Login.userName)!
        let  strFileName = String(format:"Photo%@.png",userName)
        request.httpBody = createPutBody(parameters: parameters ,
                                         boundary: boundary,
                                         data: [postData["profile_image"] as! Data],
                                         mimeType: mimetype,
                                         filename: strFileName)
        
        let session = URLSession.shared
        let task:URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if data == nil {
                
                completionHandler(false, error, false, nil, "", true)
            }
            else if response == nil {
                
                completionHandler(false, error, false, nil, "", true)
            }
            else {
                
                do {
                    
                    let strTest:String = String.init(data: data!, encoding: .utf8)!
                    //print(strTest)
                    
                    if strTest.count == 0 {
                        
                        //print("Data is blank or nill from \(url)")
                    }
                    
                    let result:Any = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if result is Dictionary<String, Any> {
                        
                        if ((result as! Dictionary<String, Any>)["errors"] != nil) &&  ((result as! Dictionary<String, Any>)["errors"] is String) && ((result as! Dictionary<String, Any>)["errors"] as! String).elementsEqual("Access denied"){
                            
                            completionHandler(true, error, false, result, "", false)
                            return
                        }
                        
                        var successResultFound: Bool = false
                        var userSessionActive: Bool  = true
                        
                        if ((result as! Dictionary<String, Any>)[Constants.apiConstants.apiResultStatusKey] != nil) {
                            
                            let number:NSNumber = (((result as! Dictionary<String, Any>)[Constants.apiConstants.apiResultStatusKey]) as! NSNumber)
                            let error: Any? = (result as! Dictionary<String, Any>)["success"]
                            successResultFound = number.boolValue
                            
                            if error != nil && error is String && (error as! String).elementsEqual("Access denied") {
                                
                                //print("----- making user inactive")
                                userSessionActive = false
                            }
                        }
                        
                        completionHandler(true, error, successResultFound, result, "", userSessionActive)
                    }
                    else {
                        
                        let msg: String = String.init(data: data!, encoding: String.Encoding.utf8)!
                        completionHandler(true, error, false, nil, msg, true)
                    }
                }
                catch {
                    completionHandler(false, error, false, nil, error as? String, true)
                }
            }
        }
        task.resume()
    }
    
    static  func createPutBody(parameters: [String: AnyObject],boundary: String, data: [Data],mimeType: String, filename: String) -> Data{
        let body = NSMutableData()
        
        for (key, value) in parameters {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        body.appendString("--\(boundary)\r\n")
        for imageData in data {
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\("profile_image")\"; filename=\"\(filename)\"\r\n")
            body.appendString("Content-Type: \(mimeType)\r\n\r\n")
            body.append(imageData)
            body.appendString("\r\n")
        }
        body.appendString("--\(boundary)--\r\n")
        return body as Data
    }
    
    
    //    static func PostDataMultipart(parameters postData:[String:Any], urlString:String, headerInfo:[String:String], complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {
    //
    //        let urlStr: String = Constants.apiConstants.baseURL + urlString
    //
    //        //Request
    //        let request = NSMutableURLRequest()
    //        request.url = URL(string: urlStr)
    //        request.httpMethod = "POST"
    //
    //        let headerInfo: Dictionary<String, String> = headerInfo
    //
    //
    //        for key:String in headerInfo.keys {
    //
    //            request.setValue(headerInfo[key], forHTTPHeaderField: key)
    //        }
    //
    //        let boundary = "Boundary-\(UUID().uuidString)"
    //        let contentType = "multipart/form-data; boundary=\(boundary)"
    //        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
    //
    //        var body = Data()
    //        let chatData : [String: Any] = postData
    //
    //        for key in chatData.keys {
    //            let value : Any? = chatData[key]
    //            let type = chatData["type"] as! String
    //            if (key ==  "post_attachments_attributes") {
    //                let arrayImage:[Any] = chatData["post_attachments_attributes"] as! [Any]
    //                for _ in arrayImage{
    //                    if let data = "--\(boundary)\r\n".data(using: .utf8) {
    //                        body.append(data)
    //                    }
    //
    //                    if (type == "video") {
    //
    //                        if let data = "Content-Disposition: form-data; name=\"\(key)\"; filename=\"video.mp4\"\r\n\r\n".data(using: .utf8) {
    //                            body.append(data)
    //                        }
    //
    //                        //                    if let data = "Content-Type: video/mov\r\n\r\n".data(using: .utf8) {
    //                        //                        body.append(data)
    //                        //                    }
    //
    //                    } else  if (type == "image"){
    //
    //                        if let data = "Content-Disposition: form-data; name=\"\(key)\"; filename=\"image.png\"\r\n\r\n".data(using: .utf8) {
    //                            body.append(data)
    //                        }
    //
    //                    }else if (type == "doc"){
    //
    //                        if let data = "Content-Type: application/octa-stream\r\n\r\n".data(using: .utf8) {
    //                            body.append(data)
    //                        }
    //                    }
    //
    //                    if let value = chatData[key] as? Data {
    //                        body.append(value)
    //                    }
    //                    if let data = "\r\n".data(using: .utf8) {
    //                        body.append(data)
    //                    }
    //                }
    //            }else {
    //
    //                if let data = "--\(boundary)\r\n".data(using: .utf8) {
    //                    body.append(data)
    //                }
    //                if let data = "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8) {
    //                    body.append(data)
    //                }
    //
    //                if let data = "\(value!)\r\n".data(using: .utf8) {
    //                    body.append(data)
    //                }
    //            }
    //
    //        }
    //        // close form
    //
    //        if let data = "--\(boundary)--\r\n".data(using: .utf8) {
    //            body.append(data)
    //        }
    //
    //        // set request body
    //        request.httpBody = body
    //
    //
    //        let sessionConfiguration = URLSessionConfiguration.default
    //        let session = URLSession(configuration: sessionConfiguration, delegate: self as? URLSessionDelegate, delegateQueue: nil)
    //
    //        let task:URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
    //
    //            if data == nil {
    //
    //                complitionHandler(false, error, false, nil, "", true)
    //            }
    //            else if response == nil {
    //
    //                complitionHandler(false, error, false, nil, "", true)
    //            }
    //            else {
    //
    //                do {
    //
    //                    let strTest:String = String.init(data: data!, encoding: .utf8)!
    //                    //print(strTest)
    //
    //                    if strTest.count == 0 {
    //
    //                        //print("Data is blank or nill from \(url)")
    //                    }
    //
    //                    let result:Any = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
    //
    //                    if result is Dictionary<String, Any> {
    //
    //                        if ((result as! Dictionary<String, Any>)["errors"] != nil) &&  ((result as! Dictionary<String, Any>)["errors"] is String) && ((result as! Dictionary<String, Any>)["errors"] as! String).elementsEqual("Access denied"){
    //
    //                            complitionHandler(true, error, false, result, "", false)
    //                            return
    //                        }
    //
    //                        var successResultFound: Bool = false
    //                        var userSessionActive: Bool  = true
    //
    //                        if ((result as! Dictionary<String, Any>)["success"] != nil) {
    //
    //                            let number:NSNumber = (((result as! Dictionary<String, Any>)["success"]) as! NSNumber)
    //                            let error: Any? = (result as! Dictionary<String, Any>)["success"]
    //                            successResultFound = number.boolValue
    //
    //                            if error != nil && error is String && (error as! String).elementsEqual("Access denied") {
    //                                //print("----- making user inactive")
    //                                userSessionActive = false
    //                            }
    //                        }
    //
    //                        complitionHandler(true, error, successResultFound, result, "", userSessionActive)
    //                    }
    //                    else {
    //
    //                        let msg: String = String.init(data: data!, encoding: String.Encoding.utf8)!
    //                        complitionHandler(true, error, false, nil, msg, true)
    //                    }
    //                }
    //                catch {
    //                    complitionHandler(false, error, false, nil, error as? String, true)
    //                }
    //            }
    //        }
    //        task.resume()
    //    }
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
    
    
}
