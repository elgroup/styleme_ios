//
//  CountryListTableViewController.swift
//  CountryListExample
//
//  Created by Juan Pablo on 9/8/17.
//  Copyright © 2017 Juan Pablo Fernandez. All rights reserved.
//

import UIKit

public protocol CountryListDelegate: class {
    func selectedCountry(dict: NSDictionary)
    func didCancel()
}

class CountryList: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var tableView: UITableView!
    var searchController: UISearchBar!
    var resultsController = UITableViewController()
    var filteredCountries = NSMutableArray() //[Country]()
    var arrCountryList = NSMutableArray()
    var isSuceess:Bool!
    var isSearch:Bool!
    open weak var delegate: CountryListDelegate?
    
    public var countryList: [Country] {
        let countries = Countries()
        let countryList = countries.countries
        return countryList
    }
    
    var indexList: [String] {
        var indexList: [String] = []
        for country in countryList {
            if let firstLetter = country.name?.first?.description.lowercased() {
                if !indexList.contains(firstLetter) { indexList.append(firstLetter) }
            }
        }
        return indexList
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Country List"
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
        
        // UIApplication.shared.statusBarStyle = .lightContent
        
        //
        //        tableView = UITableView(frame: view.frame)
        //        tableView.register(CountryCell.self, forCellReuseIdentifier: "Cell")
        //        tableView.tableFooterView = UIView()
        //        tableView.dataSource = self
        //        tableView.delegate = self
        //        tableView.backgroundColor = UIColor.clear
        //        tableView.separatorColor = .clear
        //        self.view.addSubview(tableView)
        //        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(handleCancel))
        //        setUpSearchBar()
        //        let countries = countryList as NSArray
        //        for countryobj in countries {
        //            let name = ( countryobj as! Country).name
        //            let countryObj =  NSMutableDictionary()
        //            countryObj.setValue(name, forKey:"name")
        //            countryObj.setValue((countryobj as! Country).countryCode, forKey: "countryCode")
        //            countryObj.setValue((countryobj as! Country).phoneExtension, forKey: "phoneExtension")
        //            arrCountryList.add(countryObj)
        //        }
    }
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        let colorStatus = Colors()
        //        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        //        let statusBarLayer = colorStatus.gl
        //        statusBarLayer?.frame = UIApplication.shared.statusBarView!.frame
        //        UIApplication.shared.statusBarView?.layer.insertSublayer(statusBarLayer!, at: 0)
        isSearch = false
        self.title = "Country List"
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black, NSAttributedString.Key.font:UIFont.PoppinsSemiBold(ofSize: 18)]
        let statusBarHeight: CGFloat = self.statusBarHeight()
        tableView = UITableView.init(frame: CGRect(x: 0.0, y: self.navigationController!.navigationBar.frame.size.height + 50 + statusBarHeight, width: self.view.frame.size.width, height: self.view.frame.size.height - 50 - statusBarHeight - self.navigationController!.navigationBar.frame.size.height))
        tableView.register(CountryCell.self, forCellReuseIdentifier: "Cell")
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = UIColor.white
        tableView.separatorColor = .lightGray
        self.view.addSubview(tableView)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(handleCancel))
        setUpSearchBar()
        let countries = countryList as NSArray
        arrCountryList.removeAllObjects()
        for countryobj in countries {
            let name = ( countryobj as! Country).name
            let countryObj =  NSMutableDictionary()
            countryObj.setValue(name, forKey:"name")
            countryObj.setValue((countryobj as! Country).countryCode, forKey: "countryCode")
            countryObj.setValue((countryobj as! Country).phoneExtension, forKey: "phoneExtension")
            arrCountryList.add(countryObj)
        }
    }
    //MARK: Status Bar Style
    override public var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    override public func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        // tableView = UITableView.init(frame: CGRect(x: 0.0, y: self.navigationController!.navigationBar.frame.size.height + 50, width: self.view.frame.size.width, height: self.view.frame.size.height - 50))
        //  tableView.frame = CGRect(x: 0.0, y: (self.navigationController?.navigationBar.frame.size.height)! + 22 + 50, width: self.view.frame.size.width, height: self.view.frame.size.height - 50)
        //  self.searchController = UISearchBar.init(frame: CGRect(x: 0, y: self.navigationController!.navigationBar.frame.size.height, width: self.view.frame.size.width, height: 50.0))
        //  self.searchController.frame = CGRect(x: 0, y: self.navigationController!.navigationBar.frame.size.height + 22, width: self.view.frame.size.width, height: 50.0)
    }
    func statusBarHeight() -> CGFloat {
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        return Swift.min(statusBarSize.width, statusBarSize.height)
    }
    
    func setUpSearchBar(){
        self.searchController = UISearchBar.init()
        let statusBarHeight: CGFloat = self.statusBarHeight()
        self.searchController.frame = CGRect(x: 0, y: self.navigationController!.navigationBar.frame.size.height + statusBarHeight, width: self.view.frame.size.width, height: 50.0)
        self.view.addSubview(searchController)
        self.searchController.delegate = self
        self.searchController?.placeholder = "Search"
        self.searchController.showsCancelButton = true
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if (searchBar.text?.count)! > 0{
            isSearch = true
            filteredCountries.removeAllObjects()
            
            if searchText.count != 0{
                let predicate:NSPredicate = NSPredicate.init(format:"name beginswith[c] %@",searchText)
                filteredCountries = (arrCountryList.filtered(using: predicate) as NSArray).mutableCopy() as! NSMutableArray
                tableView.reloadData()
            }
        }else{
            isSearch = false
            tableView.reloadData()
        }
    }
    //    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
    //        searchBar.resignFirstResponder()
    //    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // let cell = tableView.cellForRow(at: indexPath) as! CountryCell
        //let country = cell.country!
        
        //self.searchController?.isActive = false
        if  isSearch && searchController!.text != "" {
            let dict = filteredCountries[indexPath.row] as! NSDictionary
            self.delegate?.selectedCountry(dict: dict)
            tableView.deselectRow(at: indexPath, animated: true)
            self.tableView.reloadData()
            self.dismiss(animated: true, completion: nil)
            return
        }
        let predicate:NSPredicate = NSPredicate.init(format:"name beginswith[c] %@",indexList[indexPath.section])
        let arr = arrCountryList.filtered(using: predicate) as NSArray
        let selectedDict = arr.object(at: indexPath.row) as! NSDictionary
        self.delegate?.selectedCountry(dict: selectedDict)
        
        tableView.deselectRow(at: indexPath, animated: true)
        self.tableView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch{
            return filteredCountries.count
        }
        else{
            let predicate:NSPredicate = NSPredicate.init(format:"name beginswith[c] %@",indexList[section])
            let arr = arrCountryList.filtered(using: predicate)
            return arr.count
        }
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isSearch {
            return ""
        }
        else{
            return indexList[section].uppercased()
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        if isSearch{
            return 1
        }
        else{
            return indexList.count
        }
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! CountryCell
        if  isSearch && searchController!.text != "" {
            let dict = filteredCountries[indexPath.row] as! NSDictionary
            cell.nameLabel?.text = dict["name"] as? String
            let strCountryCode = (dict["countryCode"] as! String).lowercased()
            cell.flagImageView?.image = UIImage.init(named:strCountryCode)
            return cell
        }
        let predicate:NSPredicate = NSPredicate.init(format:"name beginswith[c] %@",indexList[indexPath.section])
        let arr = arrCountryList.filtered(using: predicate)
        let dict = arr[indexPath.row] as! NSDictionary
        let countryCode = dict["phoneExtension"] as! String
        let countryName:String = dict["name"] as! String
        let strLabelText:String = "+(\(countryCode)) " + countryName
        cell.nameLabel?.text = strLabelText
        let strCountryCode = (dict["countryCode"] as! String).lowercased()
        cell.flagImageView?.image = UIImage.init(named:strCountryCode)
        return cell
    }
    
    @objc func handleCancel() {
        //        self.dismiss(animated: true, completion: nil)
        self.delegate?.didCancel()
        
    }
}

