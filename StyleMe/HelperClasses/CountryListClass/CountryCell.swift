//
//  CountryCell.swift
//  CountryListExample
//
//  Created by Juan Pablo on 9/8/17.
//  Copyright © 2017 Juan Pablo Fernandez. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {
    
    var nameLabel: UILabel?
    var extensionLabel: UILabel?
    var flagImageView: UIImageView?
    
    var country: Country? {
        didSet {
            if let name = country!.name {
                flagImageView?.image = UIImage.init(named: ((country?.countryCode)?.lowercased())!)
                print(name)
                // nameLabel?.text = "\(name)"
                //extensionLabel?.text = "+\(country!.phoneExtension)"
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        self.flagImageView?.image = UIImage.init(named: "")
        self.nameLabel?.text = ""
        self.extensionLabel?.text = ""
    }
    
    func setup() {
        
        flagImageView = UIImageView()
        flagImageView?.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(flagImageView!)
        
        //        flagImageView?.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        //        flagImageView?.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        flagImageView?.frame = CGRect(x:20 , y: self.frame.height/2 - 7, width: 25 , height: 15)
        flagImageView?.contentMode = .scaleAspectFit
        flagImageView?.layer.cornerRadius = 2.0
        flagImageView?.clipsToBounds = true
        //flagImageView?.backgroundColor = UIColor.black
        
        nameLabel = UILabel()
        nameLabel?.textColor = UIColor.black
        nameLabel?.font = UIFont.systemFont(ofSize: 20)
        nameLabel?.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(nameLabel!)
        nameLabel?.frame = CGRect(x: 60 , y: 5, width: UIScreen.main.bounds.size.width - 100 , height: self.frame.height - 5)
        nameLabel?.font = UIFont.PoppinsLight(ofSize: 13)//UIFont.init(name: "Avenir-Medium", size: 16)
        //        nameLabel?.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        //        nameLabel?.leftAnchor.constraint(equalTo: self., constant: 15).isActive = true
        
        extensionLabel = UILabel()
        extensionLabel?.textColor = UIColor.gray
        extensionLabel?.font = UIFont.systemFont(ofSize: 18)
        extensionLabel?.translatesAutoresizingMaskIntoConstraints = false
        extensionLabel?.frame = CGRect(x: UIScreen.main.bounds.size.width-60, y: 5, width: 60, height: self.frame.height - 5)
        self.addSubview(extensionLabel!)
        //        extensionLabel?.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        //        extensionLabel?.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
    }
}
