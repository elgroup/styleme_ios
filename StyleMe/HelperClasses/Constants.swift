//
//  Constants.swift
//  StyleMe
//
//  Created by Admin on 01/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    enum addressType:Int {
        case Home = 0
        case Work = 1
        case Other = 2
    }
    
    enum apiConstants {
        static let baseURL = "http://styleme.gharwale.com/api/v1/"
        static let baseUrlImage = "http://styleme.gharwale.com"
        public static var deviceType: String = "iOS"
        static var apiResultSuccessKey: String = "success"
        static var apiResultStatusKey: String  = "status"
        static var HTTPHeaderFieldContentType:String = "Content-Type"
        static var HTTPHeaderFieldDeviceType:String = "Device-Type"
        static var HTTPHeaderFieldTimeZone = "Time-Zone"
        static let alertSuccess = "Success"
        static let locale = "LOCALE"
    }
    enum URLConstants {
        static let login = "users/sign_in"
        static let signup = "users"
        static let forgotPassword = "users/passwords"
        static let otpVerification = "users/send_otp"
        static let checkUser = "uid_check"
        static let socialSignUp = "users/social_network_login"
        static let newPost = "posts"
        static let getCategories = "categories"
        static let postComment = "/comment_posts"
        static let followUser = "/follow"
        static let unfollowUser = "/unfollow"
        static let getProfile = "profile"
        static let userPosts = "user_posts"
        static let signOutUser = "users/sign_out"
        static let profileUpdate = "profile/password"
        static let followersList = "followers"
        static let followingList = "following"
        static let addAddress = "add_address"
        static let deleteAddress = "remove_address"
        static let updateAddress = "update_address"
        static let getSalons = "salons"
        static let getSubCategory = "sub_categories"
        static let joinStylist = "join_as_stylist"
    }
    enum AppConstants{
        static let alert:String = "Alert"
        static let alertMessageCheckInternet = "Please connect to internet connection"
    }
    enum StaticTextHeaders {
        static let headerTextLogin = "Welcome to StyleUp app, your one time solution to your Salon and beauty needs."
        static let headerTextForgotPassword = "Forgot Password"
        static let headerTextOtpVerification = "Verification"
        static let headerTextCongratulations = "Congratulations"
        static let textForgotPass = "Forgotten your password? Worry no more! Fill up the phone number and click on Send."
        static let textVerification = "The OTP is generated and is required to be filled in the given boxes and then press the Sign In tab."
        static let headerResetPassword = "Reset Password"
        static let textResetPassword = "Reset your password then only you can Sign In"
    }
    
    enum Login {
        static let name = "name"
        static let authToken = "auth_token"
        static let mobileNumber = "mobile_number"
        static let mobileCode = "mobile_code"
        static let countryId = "country_id"
        static let userId = "id"
        static let userName = "user_name"
        static let isLogin = "isLogin"
    }
    
    enum SignupConstants{
        static let successSignup = "User successfully created. Please Sign In to continue."
        static let inputEmailAlertText = "Please enter Email"
        static let inputMobileAlertText = "Please enter Mobile Number"
        static let validateMobileAlertText =  "Please verify Mobile Number"
        static let inputNameAlertText = "Please enter Name"
        static let inputPasswordAlertText = "Please enter Password"
        static let inputConfirmPasswordAlertText = "Please enter Confirm Password"
        static let validatePasswordandConfirmPassword = "Confirm Password must be same as Password"
        static let buttonVerifyDefaultTitle = "Verify"
        static let buttonVerifyTitleVerified = "Verified"
        static let inputCountryAlertText = "Please select Country"
    }
    enum LoginConstants {
        static let inputUsernameAlert = "Please enter Email Id or Mobile Number"
        static let inputPasswordAlert = "Please enter Password" 
    }
    enum OtpVerificatonConstants{
        static let messageValidCode = "Please enter valid Code"
         static let messageWrongOTP = "The OTP entered is incorrect."
        static let messageSuccesVerification = "Successfully verified your Mobile Number"
    }
    enum ResetPasswordConstant{
        static let alertNoPassword = "Please enter Password"
        static let alertPasswordLength = "Password cannot be greater than 16 characters and less than 8 characters."
        static let alertNoConfirmPassword = "Please enter Confirm Password"
        static let alertMatchPasswordAndConfirmPassword = "Password and Confirm Password must be same"
    }
    enum UIViewControllerIdentifier{
        static let LoginTableViewController = "LoginTableViewController"
        static let SignUpTableViewController = "SignUpTableViewController"
        static let OTPVerificationTableViewController = "OTPVerificationTableViewController"
        static let ForgotPasswordTableViewController = "ForgotPasswordTableViewController"
        static let CongratulationsViewController = "CongratulationsViewController"
        static let ResetPasswordTableViewController = "ResetPasswordViewController"
        static let SalonTabControllerStoryBoard = "Salon"
        static let StylistStoryBoard = "Stylist"
        static let PostTabControllerStoryBoard = "Post"
        static let MainStoryBoard = "Main"
        static let SalonTabbarController = "SalonTabBarController"
        static let SalonHomeViewController = "SalonHomeViewController"
        static let SearchViewController = "SearchViewController"
        static let NotificationViewController = "NotificationViewController"
        static let ProfileViewController = "ProfileViewController"
        static let AddPostViewController = "AddPostViewController"
        static let PostHomeViewController = "PostHomeViewController"
        static let AddPostTabBarController = "AddPostTabBarController"
        static let GalleryViewController = "GalleryViewController"
        static let VideoViewController = "VideoViewController"
        static let CameraViewController = "CameraViewController"
        static let OfferViewController = "OfferViewController"
    }
    
}
