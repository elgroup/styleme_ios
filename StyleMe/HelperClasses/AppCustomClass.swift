//
//  AppCustomClass.swift
//  StyleMe
//
//  Created by EL INFO on 07/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AppCustomClass: NSObject {
    
    //MARK: Hide Indicator With Show Message.............................................
    static func showToastAlert(Title name:String , Message message:String, Controller vc:UIViewController, Time seconds: Int)
    {
        //uti.hideActivityIndicator(uiView: myView)
        let alertController = UIAlertController(title: name, message: message, preferredStyle: .alert)
        vc.present(alertController, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(seconds)) {
            print("1m μs seconds later")
            alertController.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: Custom Alert with button Action on Single Button
    static func showCustomAlert(alertTitle name:String, Message message:String,buttonTitle title:String, Controller vc:UIViewController,completionForOk:@escaping (_ success:Bool) -> Void){
        let alertController = UIAlertController(title: name, message: message, preferredStyle: .alert)
        let alert:UIAlertAction = UIAlertAction.init(title: title, style: .default) { (UIAlertAction) in
            completionForOk(true)
        }
        let alertCancel:UIAlertAction = UIAlertAction.init(title: "Cancel", style: .destructive) { (UIAlertAction) in
            completionForOk(false)
        }
        alertController.addAction(alert)
        alertController.addAction(alertCancel)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: Check For Null or nil
    static func isNsnullOrNil(object : AnyObject?) -> Bool
    {
        if (object is NSNull) || (object == nil)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    //MARK: Format Number
    func formatPoints(from: Int) -> String {

        let number = Double(from)
        let thousand = number / 1000
        let million = number / 1000000
        let billion = number / 1000000000

        if billion >= 1.0 {
            return "\(round(billion*10)/10)B"
        } else if million >= 1.0 {
            return "\(round(million*10)/10)M"
        } else if thousand >= 1.0 {
            return ("\(round(thousand*10/10))K")
        } else {
            return "\(Int(number))"
        }
    }
}
