//
//  MultiPartTextField.h
//  Wheelys_Client
//
//  Created by ShailshNailwal on 6/19/17.
//  Copyright © 2017 Shailsh. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MultiPartTextField : UIView

@property(nonatomic) short numberOfTextPartRequired;
@property(nonatomic) short numberOfCharactersPerPartRequired;
@property(nonatomic, strong) NSString *currentValue;

@end
