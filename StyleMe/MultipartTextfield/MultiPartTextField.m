//
//  MultiPartTextField.m
//  Wheelys_Client
//
//  Created by ShailshNailwal on 6/19/17.
//  Copyright © 2017 Shailsh. All rights reserved.
//

#import "MultiPartTextField.h"


@interface MultiPartTextField ()<UITextFieldDelegate> {
    
    UITextField *_tfCardNumberPart1;
    UITextField *_tfCardNumberPart2;
    UITextField *_tfCardNumberPart3;
    UITextField *_tfCardNumberPart4;
    NSMutableArray *_arrCardParts;
}
    
    @end

@implementation MultiPartTextField
    
- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    
    if (self) {
        
        _arrCardParts = [NSMutableArray new];
        _numberOfTextPartRequired = 4;
        _numberOfCharactersPerPartRequired = 1;
        
        [self fixUI];
    }
    return self;
}
    
    
- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        _arrCardParts = [NSMutableArray new];
        
        _numberOfTextPartRequired = 4;
        _numberOfCharactersPerPartRequired = 1;
        
        [self fixUI];
    }
    return self;
}
    
- (void) fixUI {
    
    CGRect frame;
    CGFloat margin = 30;
    int width = (self.frame.size.width - (_numberOfTextPartRequired - 1) * margin) / _numberOfTextPartRequired;
    
    if (_arrCardParts.count != _numberOfTextPartRequired) {
        [_arrCardParts removeAllObjects];
        for (int i = 0; i < _numberOfTextPartRequired; i ++) {
            UITextField *tf = [UITextField new];
            tf.delegate = self;
            [_arrCardParts addObject:tf];
        }
    }
    
    UITextField *tf;
    for (int i = 0; i < _numberOfTextPartRequired; i++) {
        tf = _arrCardParts[i];
        frame = CGRectMake((i * (width + margin)), 0, width, self.frame.size.height);
        tf.frame = frame;
        tf.delegate = self;
        tf.textAlignment = NSTextAlignmentCenter;
        tf.font = [UIFont fontWithName:@"Poppins-Regular" size:32];
        tf.tag = i;
        tf.backgroundColor = [UIColor lightGrayColor];
        //        tf.placeholder = [@(i) stringValue];
        [tf setKeyboardType:UIKeyboardTypeNumberPad];
        //        tf.tintColor = [UIColor redColor];
        
        //// for project specific ////
        UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, tf.frame.size.height-2, width, 1)];
        bottomLine.backgroundColor = [UIColor lightGrayColor];
        [tf addSubview:bottomLine];
        // tf.backgroundColor = [UIColor redColor];
        //////////////////////////////
        
        //        if ( _numberOfTextPartRequired == 4) {
        //
        //        }
        [self addSubview:tf];
    }
}
    
    //- (void) setNumberOfTextPartRequired:(short)numberOfTextPartRequired {
    //
    //    _numberOfTextPartRequired = numberOfTextPartRequired;
    //
    //    for (UITextField *tf in _arrCardParts) {
    //
    //        [tf removeFromSuperview];
    //    }
    //
    //    [_arrCardParts removeAllObjects];
    //    [self fixUI];
    //}
    
    //- (void)setNumberOfCharactersPerPartRequired:(short)numberOfCharacterPerPartRequired {
    //
    //    _numberOfCharactersPerPartRequired = numberOfCharacterPerPartRequired;
    //
    //    for (UITextField *tf in _arrCardParts) {
    //
    //        [tf removeFromSuperview];
    //    }
    //
    //    [_arrCardParts removeAllObjects];
    //    [self fixUI];
    //}
    
    - (NSString *)currentValue {
    
        NSString *finalValue = @"";
    
        for (UITextField *tf in _arrCardParts) {
    
            finalValue = [NSString stringWithFormat:@"%@%@", finalValue, [tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }
    
        return finalValue;
    }
    
#pragma mark- Text field
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([_arrCardParts containsObject:textField]) {
        
        unichar ch = (string.length > 0) ? [string characterAtIndex:0] : 0;
        
        if ([string isEqualToString:@" "] || ((ch < 48 || ch > 57) && ch != 0)) {
            
            return NO;
        }
        
        NSInteger index = [_arrCardParts indexOfObject:textField];
        
        if ([string isEqualToString:@""] && index > 0 && textField.text.length == 1) {
            
            textField.text = @"";
            UITextField *previousTextField = [_arrCardParts objectAtIndex:(index-1)];
            [previousTextField becomeFirstResponder];
            
            return NO;
        }
        else if (![string isEqualToString:@""] && index < _arrCardParts.count-1 && textField.text.length == _numberOfCharactersPerPartRequired) {
            
            UITextField *nextTextField = [_arrCardParts objectAtIndex:(index+1)];
            
            for (int i = (int)(index + 1); i < _arrCardParts.count; i++) {
                
                UITextField *tf = [_arrCardParts objectAtIndex:i];
                tf.text = @"";
            }
            
            nextTextField.text = string;//@"";
            [nextTextField becomeFirstResponder];
            return NO;
        }
        else if (![string isEqualToString:@""] && index < _arrCardParts.count && textField.text.length == _numberOfCharactersPerPartRequired) {
            UITextField *nextTextField = [_arrCardParts objectAtIndex:(index+1)];
            
            for (int i = (int)(index + 1); i < _arrCardParts.count; i++) {
                
                UITextField *tf = [_arrCardParts objectAtIndex:i];
                tf.text = @"";
            }
            
            nextTextField.text = string;//@"";
            [nextTextField becomeFirstResponder];
            return NO;
        }
    }
    return true;
}
    
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if ([_arrCardParts containsObject:textField]) {
        
        if ([_arrCardParts indexOfObject:textField] > 0) {
            
            UITextField *previousTF = [_arrCardParts objectAtIndex:([_arrCardParts indexOfObject:textField]-1)];
            
            if (previousTF.text.length < _numberOfCharactersPerPartRequired) {
                
                [previousTF becomeFirstResponder];
            }
            
        }
    }
}
    
- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    [self fixUI];
}
    
    @end
