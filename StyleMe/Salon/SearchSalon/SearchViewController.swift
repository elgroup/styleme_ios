//
//  SearchViewController.swift
//  StyleMe
//
//  Created by EL INFO on 13/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    @IBOutlet weak var viewContainerSearch: UIView!
    @IBOutlet weak var textFieldSearch:UITextField!
    @IBOutlet weak var tableViewSearch:UITableView!
    var containerView:UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.customNavigationBar()
        self.textFieldSearch.becomeFirstResponder()
        self.tableViewSearch.register(UINib(nibName: "TopSalonTableViewCell", bundle: nil), forCellReuseIdentifier: "TopSalonTableViewCell")

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    @IBAction func tapped(_ sender: Any) {
        self.textFieldSearch.resignFirstResponder()
    }
    
    
   
}

extension SearchViewController{
    //MARK: Method for Custom Navigation Bar
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
        
        containerView = UIView.init(frame: CGRect.init(x: 20, y: UIApplication.shared.statusBarFrame.height
            + 5, width: UIScreen.main.bounds.size.width - 40, height: (self.navigationController?.navigationBar.frame.height)! - 5))
        self.navigationController?.view.addSubview(containerView)
        containerView.addSubview(viewContainerSearch)
        containerView.backgroundColor = UIColor.init(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        containerView.layer.cornerRadius = ((self.navigationController?.navigationBar.frame.height)! - 5)/2
        containerView.clipsToBounds = true
        
    }
}

extension SearchViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableViewSearch.dequeueReusableCell(withIdentifier: "TopSalonTableViewCell", for: indexPath as IndexPath) as! TopSalonTableViewCell
        var dictSalons:[String:Any] = [String:Any]()
        
//        dictSalons = self.topSalonsArray[indexPath.row] as! [String:Any]
//        cell.setData(topSalons: dictSalons)
        return cell
    }
    
    
}

extension SearchViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104

    }
}
