//
//  SalonHomeViewController.swift
//  StyleMe
//
//  Created by EL INFO on 13/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import CoreLocation

class SalonHomeViewController: UIViewController {
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var headerViewSalonsAndSpa:UIView!
    @IBOutlet weak var containerSalonsAndSpa:UIView!
    @IBOutlet weak var headerViewTopSalons:UIView!
    @IBOutlet weak var headerViewTopStylists:UIView!
    @IBOutlet weak var containerNavigationBar:UIView!
    @IBOutlet weak var labelUserName:UILabel!
    @IBOutlet weak var buttonLocation:UIButton!
    @IBOutlet weak var containerViewSearch:UIView!
    @IBOutlet weak var textFieldSearch:UITextField!
    @IBOutlet weak var containerFilter:FilterView!
    var activityIndicator:ActivityIndicator = ActivityIndicator()
    var topSalonsArray:[Any] = [Any]()
    var promotionsArray:[Any] = [Any]()
    var categoryArray:[Any] = [Any]()
    var topStylistArray:[Any] = [Any]()
    var containerView:UIView!
    var isFilterViewClosed:Bool = false
    var locationManager = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarUIView?.backgroundColor = UIColor.white
        }
        else{
            UIApplication.shared.statusBarUIView?.backgroundColor = UIColor.white
        }
      //  self.getResponse()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getLocation()
        // self.customNavigationBar()
        self.labelUserName.text = UserDefaults.standard.string(forKey: Constants.Login.userName)?.capitalized
        self.homeTableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 45.0, right: 0.0)
        self.homeTableView.register(UINib(nibName: "TopSalonTableViewCell", bundle: nil), forCellReuseIdentifier: "TopSalonTableViewCell")
        self.homeTableView.register(UINib(nibName: "OffersTableViewCell", bundle: nil), forCellReuseIdentifier: "OffersTableViewCell")
        self.homeTableView.register(UINib(nibName: "StylistListTableViewCell", bundle: nil), forCellReuseIdentifier: "StylistListTableViewCell")
        self.homeTableView.register(UINib(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoryTableViewCell")
        self.homeTableView.register(UINib(nibName: "SubCategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "SubCategoryTableViewCell")
        self.getDataSalon()
        self.customNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        containerView.constraints.forEach { $0.isActive = false }
        containerView.removeFromSuperview()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.containerViewSearch.layer.cornerRadius = 17
        self.containerViewSearch.clipsToBounds = true
    }
    
    
}
