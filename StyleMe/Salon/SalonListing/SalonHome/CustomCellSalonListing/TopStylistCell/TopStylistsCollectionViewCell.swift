//
//  TopStylistsCollectionViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 06/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import Cosmos

class TopStylistsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageViewStylist:UIImageView!
    @IBOutlet weak var imageViewCategory:UIImageView!
    @IBOutlet weak var labelStylistName:UILabel!
    @IBOutlet weak var ratingView:CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageViewStylist.layer.cornerRadius = 3.0
        imageViewStylist.clipsToBounds = true
        imageViewCategory.layer.cornerRadius = imageViewCategory.frame.size.width/2
        imageViewCategory.clipsToBounds = true
        ratingView.rating = 4.7
        ratingView.settings.fillMode = .half
    }

}
