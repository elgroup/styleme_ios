//
//  StylistListTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 06/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class StylistListTableViewCell: UITableViewCell {
    @IBOutlet private weak var collectionView: UICollectionView!
    var stylistArray:[Any] = [Any]()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib.init(nibName: "TopStylistsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TopStylistsCollectionViewCell")
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 150, height: 190)
        layout.scrollDirection = .horizontal
        
        self.collectionView.collectionViewLayout = layout
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func loadTopStylistCell(arr:[Any]){
        self.stylistArray = arr
        self.collectionView.reloadData()
    }
    
}
extension StylistListTableViewCell:UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.stylistArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopStylistsCollectionViewCell", for: indexPath as IndexPath) as! TopStylistsCollectionViewCell
        let dict:[String:Any] = self.stylistArray[indexPath.row] as! [String:Any]
        let stylistName:String = dict["name"] as! String
        let rating:Float = dict["ratings"] as! Float
        var strUrl:String = dict["image"] as! String
        cell.labelStylistName.text = stylistName 
        strUrl = Constants.apiConstants.baseUrlImage + strUrl
        let imageURl:URL = URL.init(string: strUrl)!
        cell.imageViewStylist!.sd_setImage(with: imageURl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
            if (cacheType == SDImageCacheType.none && image != nil) {
                cell.imageViewCategory.alpha = 0;
                UIView.animate(withDuration: 0.0, animations: { () -> Void in
                    cell.imageViewCategory.alpha = 1
                })
            } else {
                cell.imageViewCategory.alpha = 1;
            }
        })
        cell.contentView.backgroundColor = .red
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
