    //
    //  SubCategoryTableViewCell.swift
    //  StyleMe
    //
    //  Created by EL INFO on 07/01/20.
    //  Copyright © 2020 Admin. All rights reserved.
    //
    
    import UIKit
    
    class SubCategoryTableViewCell: UITableViewCell {
        
        @IBOutlet weak var collectionViewSubCategory:UICollectionView!
        override func awakeFromNib() {
            super.awakeFromNib()
            self.collectionViewSubCategory.register(UINib.init(nibName: "SubCategoryServiceTypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SubCategoryServiceTypeCollectionViewCell")
            // Initialization code
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
            // Configure the view for the selected state
        }
        
    }
    
    extension SubCategoryTableViewCell:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 3
        }
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryServiceTypeCollectionViewCell", for: indexPath as IndexPath) as! SubCategoryServiceTypeCollectionViewCell
            if indexPath.row == 0{
                cell.labelSubcategoryName.text = "Anvesh Yadav"
            }
            else if indexPath.row == 1{
                cell.labelSubcategoryName.text = "Anvesh"
            }
            else{
                cell.labelSubcategoryName.text = "vgergegr gregerw gergerg"
            }
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
        }
//        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//            return
//        }
    }
