//
//  OffersTableViewCell.swift
//  StyleMe
//
//  Created by Mamta on 31/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class OffersTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    var promotionData:[Any] = [Any]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib.init(nibName: "SalonOffersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SalonOffersCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func reloadCell(promotionArray:[Any]){
        promotionData = promotionArray
        self.collectionView.reloadData()
    }
}

extension OffersTableViewCell:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promotionData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SalonOffersCollectionViewCell", for: indexPath as IndexPath) as! SalonOffersCollectionViewCell
        
//        cell.contentView.backgroundColor = .red
        let dict:[String:Any] = promotionData[indexPath.row] as! [String:Any]
        cell.eventTypeImage.backgroundColor = .red
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width - 80, height: 120)
       }
}
