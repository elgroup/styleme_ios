//
//  TopSalonTableViewCell.swift
//  StyleMe
//
//  Created by Mamta on 31/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage


class TopSalonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewTopSalons:UIImageView!
    @IBOutlet weak var labelSalonName:UILabel!
    @IBOutlet weak var labelRating:UILabel!
    @IBOutlet weak var labelReviewCount:UILabel!
    @IBOutlet weak var buttonLocation:UIButton!
    @IBOutlet weak var labelSalonAddress:UILabel!
    @IBOutlet weak var container:UIView!
    @IBOutlet weak var ratingView:CosmosView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelSalonName.sizeToFit()
        imageViewTopSalons.roundCorners([.allCorners], radius: 3.0)
        imageViewTopSalons?.backgroundColor = .red
        container.layer.cornerRadius = 3.0
        container.clipsToBounds = true
        container.dropShadow(scale: true, radius: 1.0, opacity: 0.4)
        ratingView.settings.fillMode = .half
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setData(topSalons:[String:Any]) {
        let salonName:String = topSalons["business_name"] as! String
        let avgRating:Float = topSalons["avg_ratings"] as! Float
        
        let reviewCount:Int = topSalons["ratings_count"] as! Int
        
        labelSalonName.text = salonName
        labelRating.text = "\(avgRating)"
        ratingView.rating = Double(avgRating)
        labelReviewCount.text = "\(reviewCount)" + " reviews"
        var imageUrlString: String =  topSalons["business_logo"] as! String
        imageUrlString = Constants.apiConstants.baseUrlImage + imageUrlString
        let imageURl:URL = URL.init(string: imageUrlString)!
        
        imageViewTopSalons!.sd_setImage(with: imageURl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
            if (cacheType == SDImageCacheType.none && image != nil) {
                self.imageViewTopSalons.alpha = 0;
                UIView.animate(withDuration: 0.0, animations: { () -> Void in
                    self.imageViewTopSalons.alpha = 1
                })
            } else {
                self.imageViewTopSalons.alpha = 1;
            }
        })
        imageViewTopSalons.backgroundColor = .white
    }
}
