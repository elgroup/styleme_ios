//
//  ServiceTypeCollectionViewCell.swift
//  StyleMe
//
//  Created by Mamta on 31/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ServiceTypeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewCategory:UIImageView!
    @IBOutlet weak var labelCategoryName:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageViewCategory.layer.cornerRadius = imageViewCategory.frame.size.width/2
        imageViewCategory.clipsToBounds = true
        
        // Initialization code
    }

}
