//
//  CategoryTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 07/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import SDWebImage
protocol SelectCategoryDelegate {
    func gotoSelectedCategory(dict:[String:Any])
}
class CategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionViewCategory:UICollectionView!
    var categoryArray:[Any] = [Any]()
    var delegate:SelectCategoryDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionViewCategory.register(UINib.init(nibName: "ServiceTypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ServiceTypeCollectionViewCell")
        //      let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //     layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        //        layout.itemSize = CGSize(width: 90, height: 110)
        //        layout.scrollDirection = .horizontal
        
        //   self.collectionViewCategory.collectionViewLayout = layout
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func loadCategoryCell(categoryArray:[Any]){
        self.categoryArray = categoryArray
        self.collectionViewCategory.reloadData()
    }
    
}
extension CategoryTableViewCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoryArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceTypeCollectionViewCell", for: indexPath as IndexPath) as! ServiceTypeCollectionViewCell
        let dict:[String:Any] = self.categoryArray[indexPath.row] as! [String:Any]
        let imageUrldict:[String:Any] = dict["image"] as! [String:Any]
        var imageUrlStr:String = imageUrldict["url"] as? String ?? ""
        imageUrlStr = Constants.apiConstants.baseUrlImage + imageUrlStr
        let imageURl:URL = URL.init(string: imageUrlStr)!
        cell.imageViewCategory!.sd_setImage(with: imageURl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
            if (cacheType == SDImageCacheType.none && image != nil) {
                cell.imageViewCategory.alpha = 0;
                UIView.animate(withDuration: 0.0, animations: { () -> Void in
                    cell.imageViewCategory.alpha = 1
                })
            } else {
                cell.imageViewCategory.alpha = 1;
            }
        })
        let name:String = dict["name"] as! String
        cell.labelCategoryName.text = name.capitalized
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedCategory:[String:Any] = categoryArray[indexPath.row] as! [String:Any]
        self.delegate.gotoSelectedCategory(dict: selectedCategory)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 160)
    }
}
