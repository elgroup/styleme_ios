//
//  SalonHome+TableViewDelegateAndDataSource.swift
//  StyleMe
//
//  Created by EL INFO on 26/02/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
//MARK: Extension for TableView Delegate
extension SalonHomeViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0,2:
            return 0
        case 1:
            if categoryArray.count > 0{
                return 50
            }
            else{
                return 0
            }
        case 3:
            if topSalonsArray.count > 0{
                return 30
            }
            else{
                return 0
            }
        case 4:
            if topStylistArray.count > 0{
                return 30
            }
            else{
                return 0
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView:UIView = UIView.init()
        switch section {
        case 1:
            self.containerSalonsAndSpa.roundCorners([.bottomRight,.topRight], radius: self.containerSalonsAndSpa.frame.size.height/2)
            return headerViewSalonsAndSpa
        case 3:
            return headerViewTopSalons
        case 4:
            return headerViewTopStylists
        default:
            return headerView
        }
    }
}

//MARK: DataSource of TableView
extension SalonHomeViewController:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0,1,2:
            return 1
        case 3:
            return topSalonsArray.count
        case 4:
            return 1
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 120
        case 1:
            return 150
        case 2:
            return 0
        case 3:
            return 104
        case 4:
            return 190
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = homeTableView.dequeueReusableCell(withIdentifier: "OffersTableViewCell", for: indexPath as IndexPath) as! OffersTableViewCell
            cell.reloadCell(promotionArray: promotionsArray)
            return cell
        }
        else if indexPath.section == 1 {
            let cell = homeTableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath as IndexPath) as!
            CategoryTableViewCell
            cell.delegate = self
            cell.loadCategoryCell(categoryArray: self.categoryArray)
            return cell
        } else if indexPath.section == 2 {
            let cell = homeTableView.dequeueReusableCell(withIdentifier: "SubCategoryTableViewCell", for: indexPath as IndexPath) as! SubCategoryTableViewCell
            return cell
        } else if indexPath.section == 3 {
            let cell = homeTableView.dequeueReusableCell(withIdentifier: "TopSalonTableViewCell", for: indexPath as IndexPath) as! TopSalonTableViewCell
            var dictSalons:[String:Any] = [String:Any]()
            dictSalons = self.topSalonsArray[indexPath.row] as! [String:Any]
            cell.setData(topSalons: dictSalons)
            //cell.labelSalonName.text = salonName.capitalized
            
            return cell
        }
        else  {
            let cell = homeTableView.dequeueReusableCell(withIdentifier: "StylistListTableViewCell", for: indexPath as IndexPath) as! StylistListTableViewCell
            cell.loadTopStylistCell(arr: self.topStylistArray)
            return cell
            
        }
        
    }
}
