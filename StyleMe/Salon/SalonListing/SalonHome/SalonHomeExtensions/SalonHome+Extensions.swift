//
//  SalonHome+Extensions.swift
//  StyleMe
//
//  Created by EL INFO on 25/02/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import CoreLocation

//MARK: EXTENSIONS
//MARK: Extension of SalonHomeViewController
extension SalonHomeViewController{
    //MARK: Method for Custom Navigation Bar
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
        
        containerView = UIView.init()
        containerView.frame =  CGRect.init(x: 0, y: UIApplication.shared.statusBarFrame.height, width: UIScreen.main.bounds.size.width, height: (self.navigationController?.navigationBar.frame.height)!)
        self.navigationController?.view.addSubview(containerView)
        
        //containerView.backgroundColor = .red
        containerNavigationBar.frame = containerView.bounds
        containerView.addSubview(containerNavigationBar)
        // containerView.backgroundColor = UIColor.init(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        containerView.clipsToBounds = true
        
    }
    @IBAction func filterButton(_ sender: AnyObject){
        self.containerFilter.delegate = self
        self.containerFilter.alpha = 1
        self.containerFilter.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height)
        var arr = [Any]()
        for n in self.categoryArray {
            var dict:[String:Any] = n as! [String:Any]
            dict["isSelected"] = false
            arr.append(dict)
        }
        self.isFilterViewClosed = true
        self.containerFilter.category = arr
        self.containerFilter.selectedIndex = IndexPath.init(row: -1, section: 0)
        self.view.addSubview(self.containerFilter)
        self.containerFilter.lcBottomFilter.constant = -self.containerFilter.frame.height/2
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { (timer) in
            
            self.containerFilter.blurrView.alpha = 0
            self.animShow(view: self.containerFilter.baseViewForFilter)
            
        }
    }
    
    func animShow(view:UIView){
        self.containerFilter.lcBottomFilter.constant = -self.containerFilter.baseViewForFilter.frame.size.height
        
        UIView.animate(withDuration: 0.4, animations: {
            self.containerFilter.lcBottomFilter.constant = 0
            self.containerFilter.blurrView.alpha = 1
            view.superview!.layoutIfNeeded()
        }) { (success) in
            if success{
                self.containerFilter.selectInitialRow()
            }
        }
    }
    
    func getResponse(){
        APIManager.getRequestForURLString(false, "staging.clients.searchinfluence.com/api/clients/index", [:], [:]) { (Success, error, nothing, result, mesage, isActiveSession) in
            print(result)
        }
    }
    
    //MARK: Method for Location Button
    @IBAction func locationButtonPressed(_sender: Any){
        //let button:UIButton = _sender as! UIButton
        //self.containerView.removeFromSuperview()
        
        if isFilterViewClosed{
            containerFilter.buttonCrossPressed()
        }
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
        if #available(iOS 13.0, *) {
            let controller = story.instantiateViewController(identifier: "GetCurrentAddressViewController") as! GetCurrentAddressViewController
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = story.instantiateViewController(withIdentifier:"GetCurrentAddressViewController") as! GetCurrentAddressViewController
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    //MARK: Mehtod for getting Address from latitude and longitude
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        let lon: Double = pdblLongitude//Double("\(pdblLongitude)")!
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks
                
                
                if pm != nil && pm!.count > 0 {
                    let pm = placemarks![0]
                    self.buttonLocation.setTitle(pm.locality, for: .normal)
                }
        })
    }
    
    //MARK: Method for getting current location
    func getLocation() {
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            return
            
        case .denied, .restricted:
            let alert = UIAlertController(title: "Location Services disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            
            present(alert, animated: true, completion: nil)
            return
        case .authorizedAlways, .authorizedWhenInUse:
            break
            
        @unknown default:
            print("Error in fetching current location")
        }
        
        // 4
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.activityType = CLActivityType.otherNavigation
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    //MARK: Salon List
    func getDataSalon(){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            self.activityIndicator.showActivityIndicator(uiView: self.view)
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            
            var param:[String:Any] = [String:Any]()
            param["lat"] = locationManager.location?.coordinate.latitude
            param["long"] = locationManager.location?.coordinate.longitude
                
            APIManager.postRequestForURLString(true, Constants.URLConstants.getSalons, headerInfo, param) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if Success{
                        let resultDict = result as! [String:Any]
                        print(resultDict)
                        self.categoryArray = resultDict["category_list"] as! [Any]
                        self.promotionsArray = resultDict["advertisment"] as! [Any]
                        self.topSalonsArray = resultDict["salons_list"] as! [Any]
                        self.topStylistArray = resultDict["top_stylist"] as! [Any]
                        self.homeTableView.reloadData()
                    }
                    else{
                        let msg:String!
                        if result != nil {
                            let resultDict = result as! [String:Any]
                            msg = resultDict["message"] as? String
                        }
                        else{
                            msg = "Something went wrong"
                        }
                        
                        AppCustomClass.showToastAlert(Title:"", Message:msg, Controller: self, Time: 1)
                    }
                }
            }
//            APIManager.getRequestForURLString(true, Constants.URLConstants.getSalons, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
//                DispatchQueue.main.async {
//                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
//                    if Success{
//                        let resultDict = result as! [String:Any]
//                        print(resultDict)
//                        self.categoryArray = resultDict["category_list"] as! [Any]
//                        self.promotionsArray = resultDict["advertisment"] as! [Any]
//                        self.topSalonsArray = resultDict["salons_list"] as! [Any]
//                        self.topStylistArray = resultDict["top_stylist"] as! [Any]
//                        self.homeTableView.reloadData()
//                    }
//                    else{
//                        let msg:String!
//                        if result != nil {
//                            let resultDict = result as! [String:Any]
//                            msg = resultDict["message"] as? String
//                        }
//                        else{
//                            msg = "Something went wrong"
//                        }
//
//                        AppCustomClass.showToastAlert(Title:"", Message:msg, Controller: self, Time: 1)
//                    }
//                }
//            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
}


//MARK: Extension of CurrentLocationDelegate
extension SalonHomeViewController:CurrentLocationDelegate{
    func getAddress(dictAddress: [String : Any]) {
        let locality:String = dictAddress["locality"] as! String
        self.buttonLocation.setTitle(locality, for: .normal)
    }
}

//MARK: Extension of Place Picker Delegate
extension SalonHomeViewController:PlacePickerDelegate{
    func updateLocationData(address: String?, lat: Double?, long: Double?) {
        self.buttonLocation.setTitle(address, for: .normal)
    }
}
//MARK: Extension Of CLLocation manager Delegate
extension SalonHomeViewController:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation = locations.last {
            print("Current location: \(currentLocation)")
            self.getAddressFromLatLon(pdblLatitude: currentLocation.coordinate.latitude, withLongitude: currentLocation.coordinate.longitude)
            locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}

extension SalonHomeViewController:SelectCategoryDelegate{
    func gotoSelectedCategory(dict: [String : Any]) {
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
        let controller = story.instantiateViewController(withIdentifier:"SelectedCategorySalonListViewController") as! SelectedCategorySalonListViewController
        controller.dictCategoryDetails = dict
        controller.strLocationName = buttonLocation.titleLabel?.text
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

extension SalonHomeViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let controller:SalonTabBarController = self.tabBarController as! SalonTabBarController
        controller.buttonHomePressed(controller.buttonSearch as Any)
        if isFilterViewClosed{
            containerFilter.buttonCrossPressed()
        }
    }
}

extension SalonHomeViewController:DismissFilterViewDelegate{
    func crossButtonAction(isOpen: Bool) {
        self.animHide()
        self.isFilterViewClosed = isOpen
    }
    
    func animHide(){
        UIView.animate(withDuration: 0.4, delay: 0, options: [.curveLinear],
                       animations: {
                        self.containerFilter.lcBottomFilter.constant = -400
                        self.containerFilter.superview!.layoutIfNeeded()
                        self.containerFilter.blurrView.alpha = 0
        },  completion: {(_ completed: Bool) -> Void in
            self.containerFilter.removeFromSuperview()
        })
    }
    
    
}
