//
//  SubFilterTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 26/02/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class SubFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonCheckBox:UIButton!
    @IBOutlet weak var labelServices:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
