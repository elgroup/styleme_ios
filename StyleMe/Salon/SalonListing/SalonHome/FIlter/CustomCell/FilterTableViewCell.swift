//
//  FilterTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 25/02/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var labelCategory:UILabel!
    @IBOutlet weak var viewContainer:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
