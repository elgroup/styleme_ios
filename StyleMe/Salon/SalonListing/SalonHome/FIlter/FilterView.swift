    //
    //  FilterView.swift
    //  StyleMe
    //
    //  Created by EL INFO on 25/02/20.
    //  Copyright © 2020 Admin. All rights reserved.
    //
    
    import UIKit
    protocol DismissFilterViewDelegate{
        func crossButtonAction(isOpen:Bool)
    }
    class FilterView: UIView,UITableViewDelegate,UITableViewDataSource {
        
        @IBOutlet weak var buttonCross:UIButton!
        @IBOutlet weak var buttonClear:UIButton!
        @IBOutlet weak var blurrView:UIView!
        @IBOutlet weak var baseViewForFilter:UIView!
        @IBOutlet weak var tableViewMainTopics:UITableView!
        @IBOutlet weak var tableViewSubTopics:UITableView!
        @IBOutlet weak var lcBottomFilter:NSLayoutConstraint!
        var topicsForFilter:[String] = ["Services","Price Range"]
        var category:[Any]!
        var selectedIndex:IndexPath!
        var dictFilter:[String:Any]!
        var delegate:DismissFilterViewDelegate!
        
        
        func selectInitialRow(){
            tableView(self.tableViewMainTopics, didSelectRowAt: IndexPath.init(row: 0, section: 0))
            
        }
        @IBAction func buttonCrossPressed(){
            dictFilter = [String:Any]()
            let count = category.count
            for n in 0..<count{
                var dict:[String:Any] = category[n] as! [String:Any]
                dict["isSelected"] = false
                category.remove(at: n)
                category.insert(dict, at: n)
            }
//            self.animHide(view: baseViewForFilter)
            self.delegate.crossButtonAction(isOpen: false)
        }
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            switch tableView.tag {
            case 100:
                let cell:FilterTableViewCell = tableViewMainTopics.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as! FilterTableViewCell
                cell.labelCategory.text = topicsForFilter[indexPath.row]
                return cell
                
            case 101:
                switch selectedIndex.row {
                case 0:
                    if indexPath.row == category.count{
                        let cell:ApplyFilterTableViewCell = tableViewSubTopics.dequeueReusableCell(withIdentifier: "ApplyFilterTableViewCell", for: indexPath) as! ApplyFilterTableViewCell
                        cell.buttonApplyFilters.addTarget(self, action: #selector(applyFilter), for: .touchUpInside)
                        return cell
                    }
                    else{
                        let cell:SubFilterTableViewCell = tableViewSubTopics.dequeueReusableCell(withIdentifier: "SubFilterTableViewCell", for: indexPath) as! SubFilterTableViewCell
                        let dict:[String:Any] = category[indexPath.row] as! [String:Any]
                        cell.labelServices.text = (dict["name"] as! String)
                        let isSelected:Bool = dict["isSelected"] as! Bool
                        if isSelected{
                            cell.buttonCheckBox.setImage(UIImage.init(named: "check-box-tick"), for: .normal)
                        }
                        else{
                            cell.buttonCheckBox.setImage(UIImage.init(named: "check-box"), for: .normal)
                            
                        }
                        return cell
                    }
                default:
                    return UITableViewCell.init()
                }
            default:
                return UITableViewCell.init()
            }
            
            
            
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            switch tableView.tag {
            case 100:
                return topicsForFilter.count
            case 101:
                switch selectedIndex.row {
                case 0:
                    return category.count + 1
                case 1:
                    return 0
                default:
                    return 0
                }
            default:
                return 0
            }
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 40
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            switch tableView.tag {
            case 100:
                if selectedIndex.row != indexPath.row{
                    let cell = self.getFilterCell(index: indexPath.row)
                    let lastSelectedCell = self.getFilterCell(index: selectedIndex.row)
                    lastSelectedCell?.viewContainer.backgroundColor = UIColor.init(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
                    cell?.viewContainer.backgroundColor = UIColor.white
                    selectedIndex = IndexPath.init(row: indexPath.row, section: 0)
                    self.tableViewSubTopics.reloadData()
                }
            case 101:
                switch selectedIndex.row {
                case 0:
                    self.selectCategory(indexPath: indexPath as NSIndexPath)
                    break
                case 1:
                    print("label")
                    break
                default:
                    print("vjbjvobvoj")
                }
            default:
                print("vjbjvobvoj")
            }
        }
        
        func selectCategory(indexPath:NSIndexPath){
            let cell = self.getSubFilterCell(index: indexPath.row)
            var dict:[String:Any] = category[indexPath.row] as! [String:Any]
            if dict["isSelected"] as! Bool == false{
                dict["isSelected"] = true
                category.remove(at: indexPath.row)
                category.insert(dict, at: indexPath.row)
                cell?.buttonCheckBox.setImage(UIImage.init(named: "check-box-tick"), for: .normal)
            }
            else{
                dict["isSelected"] = false
                category.remove(at: indexPath.row)
                category.insert(dict, at: indexPath.row)
                cell?.buttonCheckBox.setImage(UIImage.init(named: "check-box"), for: .normal)
            }
        }
        //MARK: Getting TableView Cells
        func getSubFilterCell(index: Int) -> SubFilterTableViewCell? {
            guard let cell = self.tableViewSubTopics.cellForRow(at: IndexPath(row : index, section : 0)) as? SubFilterTableViewCell else {return nil}
            return cell
        }
        func getFilterCell(index: Int) -> FilterTableViewCell? {
            guard let cell = self.tableViewMainTopics.cellForRow(at: IndexPath(row : index, section : 0)) as? FilterTableViewCell else {return nil}
            return cell
        }
        @objc  func applyFilter(){
            let predicate:NSPredicate =  NSPredicate(format: "isSelected = %d", true)
            let array:NSArray = (category as NSArray).filtered(using: predicate) as NSArray
            if array.count != 0{
                dictFilter = [String:Any]()
                dictFilter["service_type"] = array
            }
//            self.animHide(view: baseViewForFilter)
            self.delegate.crossButtonAction(isOpen: false)
        }
        
        @IBAction func clearAllFilters(){
            dictFilter = [String:Any]()
            let count = category.count
            for n in 0..<count{
                var dict:[String:Any] = category[n] as! [String:Any]
                dict["isSelected"] = false
                category.remove(at: n)
                category.insert(dict, at: n)
            }
            self.tableViewMainTopics.reloadData()
            self.selectInitialRow()
            self.tableViewSubTopics.reloadData()
        }
    }
