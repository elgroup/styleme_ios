//
//  SelectedCategorySalonListViewController.swift
//  StyleMe
//
//  Created by EL INFO on 17/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class SelectedCategorySalonListViewController: UIViewController {
    @IBOutlet weak var containerNavigationBar:UIView!
    @IBOutlet weak var buttonBack:UIButton!
    @IBOutlet weak var containerViewSearch:UIView!
    @IBOutlet weak var textFieldSearch:UITextField!
    @IBOutlet weak var imageViewCategory:UIImageView!
    @IBOutlet weak var labelCategoryName:UILabel!
    @IBOutlet weak var labelLocationName:UILabel!
    @IBOutlet weak var collectionViewSubCategory:UICollectionView!
    @IBOutlet weak var tableViewSalonList:UITableView!
    var containerView:UIView!
    var strLocationName:String!
    var dictCategoryDetails:[String:Any]!
    var arrSubCategory:[Any]!
    var activityIndicator:ActivityIndicator = ActivityIndicator()
    var arrSalon:[Any]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.customNavigationBar()
        let controller:SalonTabBarController = self.tabBarController as! SalonTabBarController
        controller.hideCustomTabBar()
        labelCategoryName.text = dictCategoryDetails["name"] as? String
        let imageUrldict:[String:Any] = dictCategoryDetails["image"] as! [String:Any]
        var imageUrlStr:String = imageUrldict["url"] as? String ?? ""
        imageUrlStr = Constants.apiConstants.baseUrlImage + imageUrlStr
        let imageURl:URL = URL.init(string: imageUrlStr)!
        
        imageViewCategory!.sd_setImage(with: imageURl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
            if (cacheType == SDImageCacheType.none && image != nil) {
                self.imageViewCategory.alpha = 0;
                UIView.animate(withDuration: 0.0, animations: { () -> Void in
                    self.imageViewCategory.alpha = 1
                })
            } else {
                self.imageViewCategory.alpha = 1;
            }
        })
        labelLocationName.text = "at \(strLocationName!)"
        self.collectionViewSubCategory.register(UINib.init(nibName: "ServiceTypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ServiceTypeCollectionViewCell")
        self.imageViewCategory.layer.cornerRadius = 25
        self.imageViewCategory.layer.borderWidth = 2.0
        self.imageViewCategory.layer.borderColor = UIColor.init(red: 236.0/255.0, green: 68.0/255.0, blue: 84.0/255.0, alpha: 1.0).cgColor
        self.apiForSubcategory()
        self.tableViewSalonList.register(UINib(nibName: "TopSalonTableViewCell", bundle: nil), forCellReuseIdentifier: "TopSalonTableViewCell")
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        containerView.constraints.forEach { $0.isActive = false }
        containerView.removeFromSuperview()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.containerViewSearch.layer.cornerRadius = 17
        self.containerViewSearch.clipsToBounds = true
    }
    
}

extension SelectedCategorySalonListViewController{
    //MARK: Method for Custom Navigation Bar
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
        
        containerView = UIView.init()
        containerView.frame =  CGRect.init(x: 0, y: UIApplication.shared.statusBarFrame.height, width: UIScreen.main.bounds.size.width, height: (self.navigationController?.navigationBar.frame.height)!)
        self.navigationController?.view.addSubview(containerView)
        
        //containerView.backgroundColor = .red
        containerNavigationBar.frame = containerView.bounds
        containerView.addSubview(containerNavigationBar)
        // containerView.backgroundColor = UIColor.init(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        containerView.clipsToBounds = true
        
    }
    @IBAction func backButtonPressed(){
        let controller:SalonTabBarController = self.tabBarController as! SalonTabBarController
        controller.showCustomTabBar()
        self.navigationController?.popViewController(animated: true)
    }
    
    func apiForSubcategory(){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            self.activityIndicator.showActivityIndicator(uiView: self.view)
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            var param:[String:Any] = [String:Any]()
            //let id:Int = arrCategoryId//[0] as! Int
            var arrID:[Any] = [Any]()
            let id = dictCategoryDetails["id"]
            arrID.append(id as Any)
            param["category_id"] = arrID
            
            APIManager.postRequestForURLString(true, Constants.URLConstants.getSubCategory, headerInfo, param) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if Success{
                        let resultDict = result as! [String:Any]
                        print(resultDict)
                        self.arrSubCategory = [Any]()
                        
                        self.arrSubCategory = [Any]()
                        self.arrSubCategory = resultDict["sub_categories"] as? [Any]
                        self.collectionViewSubCategory.reloadData()
                    }
                    else{
                        let resultDict = result as! [String:Any]
                        let message = resultDict["message"] as! String
                        AppCustomClass.showToastAlert(Title:"", Message:message, Controller: self, Time: 1)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    func apiForSalons(){
        
    }
}

extension SelectedCategorySalonListViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.arrSubCategory == nil{
            return 0
        }
        else{
            return self.arrSubCategory.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceTypeCollectionViewCell", for: indexPath as IndexPath) as! ServiceTypeCollectionViewCell
        let dict:[String:Any] = self.arrSubCategory[indexPath.row] as! [String:Any]
        let imageUrldict:[String:Any] = dict["image"] as! [String:Any]
        var imageUrlStr:String = imageUrldict["url"] as? String ?? ""
        imageUrlStr = Constants.apiConstants.baseUrlImage + imageUrlStr
        let imageURl:URL = URL.init(string: imageUrlStr)!
        cell.imageViewCategory!.sd_setImage(with: imageURl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
            if (cacheType == SDImageCacheType.none && image != nil) {
                cell.imageViewCategory.alpha = 0;
                UIView.animate(withDuration: 0.0, animations: { () -> Void in
                    cell.imageViewCategory.alpha = 1
                })
            } else {
                cell.imageViewCategory.alpha = 1;
            }
        })
        let name:String = dict["name"] as! String
        cell.labelCategoryName.text = name.capitalized
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //        let selectedCategory:[String:Any] = categoryArray[indexPath.row] as! [String:Any]
        //        self.delegate.gotoSelectedCategory(dict: selectedCategory)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionViewSubCategory.frame.size.width/4, height: 160)
    }
}

extension SelectedCategorySalonListViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrSalon == nil{
            return 2
        }
        else{
            return self.arrSalon.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopSalonTableViewCell", for: indexPath as IndexPath) as! TopSalonTableViewCell
        var dictSalons:[String:Any] = [String:Any]()
        //                  dictSalons = self.topSalonsArray[indexPath.row] as! [String:Any]
        //                  cell.setData(topSalons: dictSalons)
        return cell
    }
}

extension SelectedCategorySalonListViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let controller:SalonTabBarController = self.tabBarController as! SalonTabBarController
        controller.buttonHomePressed(controller.buttonSearch as Any )
        controller.showCustomTabBar()
        self.navigationController?.popViewController(animated: false)
    }
}
