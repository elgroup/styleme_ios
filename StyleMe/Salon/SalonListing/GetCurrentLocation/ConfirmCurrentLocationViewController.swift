//
//  AddAddressViewController.swift
//  StyleMe
//
//  Created by EL INFO on 06/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVFoundation
import GoogleMaps
import CoreLocation
import GooglePlaces

//MARK: Protocol
protocol PlacePickerDelegate: class {
    func updateLocationData(address: String?, lat: Double?, long: Double?)
}
class ConfirmCurrentLocationViewController: UIViewController, GMSAutocompleteResultsViewControllerDelegate {
    
    
    let locationManager = CLLocationManager()
    var location : CLLocation?
    var currentPlace : GMSPlace!
    var locationSet : Bool = true
    var placesClient: GMSPlacesClient!
    
    var updatedPlace: GMSAddress!
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    var fromHomeVC: Bool = false
    var isMarkerHidden: Bool! = false
    var isFirst: Bool! = true
    var delegate: PlacePickerDelegate?
    var locality:String!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var centerImgView: UIImageView!
    @IBOutlet var lblMsgView: UIView!
    @IBOutlet var btnBack: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Ask for Authorisation from the User.
        self.setUpLocation()
        self.locationManager.requestAlwaysAuthorization()
        lblMsgView.isHidden = true
        self.btnBack.isEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setUpMapView()
        self.customNavigationBar()
        // Get location
        if fromHomeVC {
            self.view.isHidden = true
        }
        else {
            self.view.isHidden = false
            //            self.actInd.frame = CGRect(x: 0.0, y: 0.0, width: 80.0, height: 80.0);
            //            self.actInd.center = self.mapView.center
            //            self.actInd.hidesWhenStopped = true
            //            self.actInd.style = UIActivityIndicatorView.Style.gray
            //            self.mapView.addSubview(self.actInd)
        }
        self.mapView.delegate = self
        placesClient = GMSPlacesClient.shared()
    }
}


extension ConfirmCurrentLocationViewController{
    func setUpMapView() -> Void {
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        searchController?.searchBar.sizeToFit()
        //navigationItem.titleView = searchController?.searchBar
        searchController?.searchBar.tintColor = .lightGray
        searchController?.searchBar.delegate = self
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
    }
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        menuButton.setImage(UIImage.init(named: "back"), for: .normal)
        menuButton.addTarget(self, action: #selector(ConfirmCurrentLocationViewController.crossButtonClicked(_:)), for: .touchUpInside)
        let leftItem1 = UIBarButtonItem()
        leftItem1.customView = menuButton
        
        
        self.navigationItem.leftBarButtonItem = leftItem1
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        searchController?.searchBar.sizeToFit()
        navigationItem.titleView = searchController?.searchBar
        searchController?.searchBar.tintColor = .lightGray
        searchController?.searchBar.delegate = self
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    @objc func crossButtonClicked(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    // Program mark: Setup current location
    func setUpLocation() -> Void {
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    
    
    // MARK : Custom function
    func loadMap() {
        let position = CLLocationCoordinate2DMake((self.location?.coordinate.latitude)!, (self.location?.coordinate.longitude)!)
        _ = GMSMarker(position: position)
        
        // Set camera position and map zoom level
        
        let camera = GMSCameraPosition.camera(withLatitude: (self.location?.coordinate.latitude)!, longitude: (self.location?.coordinate.longitude)!, zoom: 16.0)
        self.mapView!.animate(to: camera)
        
        self.mapView.settings.compassButton = true
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        self.searchController?.searchBar.text = ""
        print(mapView.camera.target.latitude)
        print(mapView.camera.target.longitude)
        
        let position = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(position) { response, error in
            if error != nil {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            } else {
                if let places = response?.results() {
                    if let place = places.first {
                        self.btnBack.isEnabled = true
                        self.updatedPlace = place
                        if let lines = place.lines {
                            print("GEOCODE: Formatted Address: \(lines[0])")
                            self.searchController?.searchBar.text = lines[0]
                        }
                        
                    } else {
                        print("GEOCODE: nil first in places")
                    }
                } else {
                    print("GEOCODE: nil in places")
                }
            }
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if isMarkerHidden {
            self.lblMsgView.isHidden = true
            self.centerImgView.isHidden = false
            self.mapView.bringSubviewToFront(self.centerImgView)
            self.isMarkerHidden = false
        }
    }
    
    func mapViewSnapshotReady(_ mapView: GMSMapView) {
        self.isMarkerHidden = true
        self.isFirst = false
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if !isFirst {
            //  self.actInd.startAnimating()
            
            _ = Timer.scheduledTimer(timeInterval: 1.0,
                                     target: self,
                                     selector: #selector(eventWith(timer:)),
                                     userInfo: [ "foo" : "bar" ],
                                     repeats: false)
        }
    }
    
    @objc func eventWith(timer: Timer!) {
        if updatedPlace != nil{
        let address = updatedPlace.lines
        self.searchController?.searchBar.text = address![0]
        // self.actInd.stopAnimating()
        self.btnBack.isEnabled = true
        self.btnBack.isHidden = false
        }
    }
    
    @IBAction func doneButtonClicked(_ sender:Any){
        self.delegate?.updateLocationData(address:self.locality , lat:self.currentPlace.coordinate.latitude , long:self.currentPlace.coordinate.longitude )
        let controller:SalonTabBarController = self.tabBarController as! SalonTabBarController
        controller.showCustomTabBar()
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
}
extension ConfirmCurrentLocationViewController:CLLocationManagerDelegate{
    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = (locations.last)!
        if location != nil && self.locationSet {
            // Do any additional setup after loading the view.
            self.loadMap()
            self.locationSet = false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
        case .restricted:
            print("Location Ristricted")
            break
        case .denied:
            print("Location Denied")
            break
            
        @unknown default:
            print("Error")
        }
    }
}

//   AddAddressViewController
extension ConfirmCurrentLocationViewController:GMSMapViewDelegate{
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        self.currentPlace = place
        self.searchController?.isActive = false
        
        if fromHomeVC {
            delegate?.updateLocationData(address: place.formattedAddress, lat: place.coordinate.latitude, long: place.coordinate.longitude)
            self.dismiss(animated: true, completion: nil)
        }
        else {
            let camera = GMSCameraPosition.camera(withLatitude: self.currentPlace.coordinate.latitude, longitude: self.currentPlace.coordinate.longitude, zoom: 16.0)
            self.mapView!.animate(to: camera)
            self.searchController?.searchBar.text = place.formattedAddress
        }
        
        print("Place name: \(String(describing: place.name))")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
}

extension ConfirmCurrentLocationViewController:UISearchBarDelegate{
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Do some search stuff
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if fromHomeVC {
            self.dismiss(animated: true, completion: nil)
        }
    }
}


