//
//  CurrentLocationTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 06/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class CurrentLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewCurrentLocation:UIImageView!
    @IBOutlet weak var labelAddressHeader:UILabel!
    @IBOutlet weak var labelAddressDesc:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
