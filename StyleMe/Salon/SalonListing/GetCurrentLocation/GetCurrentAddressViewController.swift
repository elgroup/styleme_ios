//
//  GetCurrentAddressViewController.swift
//  StyleMe
//
//  Created by EL INFO on 06/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
protocol CurrentLocationDelegate {
    func getAddress(dictAddress:[String:Any])
}
class GetCurrentAddressViewController: UIViewController {
    
    @IBOutlet weak var tableViewCurrentLocation:UITableView!
    var arrAddressList:[Any] = [Any]()
    var activityIndicator = ActivityIndicator.init()
    var delegate: CurrentLocationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let controller:SalonTabBarController = self.tabBarController as! SalonTabBarController
        controller.hideCustomTabBar()
        self.customNavigationBar()
        self.getAddressList()
        // Do any additional setup after loading the view.
    }
}

extension GetCurrentAddressViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
            var controller:ConfirmCurrentLocationViewController!
            if #available(iOS 13.0, *) {
                controller  = story.instantiateViewController(identifier: "ConfirmCurrentLocationViewController") as? ConfirmCurrentLocationViewController
            } else {
                controller = story.instantiateViewController(withIdentifier:"ConfirmCurrentLocationViewController") as? ConfirmCurrentLocationViewController
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            let dict:[String:Any] = arrAddressList[indexPath.row] as! [String:Any]
            self.delegate?.getAddress(dictAddress: dict)
            let controller:SalonTabBarController = self.tabBarController as! SalonTabBarController
            controller.showCustomTabBar()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view:UIView = UIView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 40))
        let label:UILabel = UILabel.init(frame: CGRect.init(x: 20.0, y: 5.0, width: UIScreen.main.bounds.size.width - 60, height: 30))
        label.text = "Saved Address"
        label.font = UIFont.PoppinsSemiBold(ofSize: 14)
        label.textColor = UIColor.darkGray
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        else{
            return 40
        }
    }
}


extension GetCurrentAddressViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        else{
            return self.arrAddressList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentLocationTableViewCell", for: indexPath) as! CurrentLocationTableViewCell
        if indexPath.section == 0{
            
        }
        else{
            let dict:[String:Any] = self.arrAddressList[indexPath.row] as! [String : Any]
            let addressType:Int = dict["address_type"] as! Int
            cell.labelAddressHeader.text = self.getAddresstype(adressType: addressType)
            let street:String = dict["street_block"] as! String
            let landmark:String = dict["landmark"] as? String ?? ""
            let pickupAddress:String = dict["pickup_address"] as! String
            let address:String =  street + " ,\(landmark)" + " ,\(pickupAddress)"
            cell.labelAddressDesc.text = address
            cell.imageViewCurrentLocation.image = self.getAddressTypeIcons(adressType: addressType)
        }
        return cell
    }
    
}



extension GetCurrentAddressViewController{
    //MARK: Custom Navigation Bar
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        backButton.setImage(UIImage.init(named: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(GetCurrentAddressViewController.backButtonPressed(_:)), for: .touchUpInside)
        let leftItem = UIBarButtonItem()
        leftItem.customView = backButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "Select Address"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem, fixedSpace,leftItem2]
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    //MARK: Back Button Action
    @objc func backButtonPressed(_ sender: Any){
        let controller:SalonTabBarController = self.tabBarController as! SalonTabBarController
        controller.showCustomTabBar()
        self.navigationController?.popViewController(animated: true)
    }
    func getAddressList(){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            
            self.activityIndicator.showActivityIndicator(uiView: self.view)
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            APIManager.getRequestForURLString(true, "addresses", headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if Success{
                        let resultDict = result as! [String:Any]
                        self.arrAddressList = (resultDict["user_address"] as? [Any])!
                        
                        self.tableViewCurrentLocation.reloadData()
                    }
                    else{
                        AppCustomClass.showToastAlert(Title:"", Message:"No Address found.", Controller: self, Time: 1)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    func getAddresstype(adressType:Int) -> String{
        switch adressType {
        case 0:
            return "Home"
        case 1:
            return "Work"
        case 2:
            return "Other"
        default:
            return "Other"
        }
    }
    
    func getAddressTypeIcons(adressType:Int) -> UIImage{
        switch adressType {
        case 0:
            return UIImage.init(named: "home_location")!
        case 1:
            return UIImage.init(named:"Office")!
        case 2:
            return UIImage.init(named:"other_location")!
        default:
            return UIImage.init(named:"other_location")!
        }
    }
}

