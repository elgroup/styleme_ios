//
//  BookingViewController.swift
//  StyleMe
//
//  Created by Ruchi EL on 17/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class BookingViewController: UIViewController {
    
    @IBOutlet weak var tbleView : UITableView!
    var upcomingArr = [AnyObject]()
    var historyArr = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbleView.reloadData()
        self.customNavigationBar()
        self.tbleView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 45.0, right: 0.0)

    }

    
}
extension BookingViewController{
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "MY BOOKINGS"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItem = leftItem2
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white

    }
}

extension BookingViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 0:
            return 2//upcomingArr.count
        case 1:
            return 4//historyArr.count
        default:
            return 4//historyArr.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch (indexPath.section) {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingTableViewCell")! as! UpcomingTableViewCell
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell")! as! HistoryTableViewCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell")! as! HistoryTableViewCell
            return cell
        }
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        switch (indexPath.section) {
//        case 1:
//            return 220
//            
//        default:
//            return 1
//        }
//    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
            headerView.backgroundColor = .white
            let label = UILabel()
            
            label.frame = CGRect.init(x: 15, y: 0, width:headerView.frame.width-10, height: headerView.frame.height)
            label.text = "Upcoming"
            label.backgroundColor = .white
            label.font = UIFont(name: "Poppins-SemiBold", size: 16)
            label.textColor = UIColor.black
            
            headerView.addSubview(label)
            return headerView
            
        }
        else{
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
            headerView.backgroundColor = .white
            let label = UILabel()
            label.frame = CGRect.init(x: 15, y: 0, width:headerView.frame.width-10, height: headerView.frame.height)
            label.text = "History"
            label.font = UIFont(name: "Poppins-SemiBold", size: 16)
            label.textColor = UIColor.black
            label.backgroundColor = .white

            headerView.addSubview(label)
            return headerView
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}
