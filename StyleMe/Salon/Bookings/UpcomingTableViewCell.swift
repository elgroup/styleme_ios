//
//  UpcomingTableViewCell.swift
//  StyleMe
//
//  Created by Ruchi EL on 17/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class UpcomingTableViewCell: UITableViewCell {
    @IBOutlet weak var BaseView : UIView!
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var typeLbl : UILabel!
    @IBOutlet weak var type2Lbl : UILabel!
    @IBOutlet weak var timeLbl : UILabel!
    @IBOutlet weak var time2Lbl : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var date2Lbl : UILabel!
    @IBOutlet weak var specialLbl : UILabel!
    @IBOutlet weak var special2Lbl : UILabel!
    @IBOutlet weak var firstOldPriceLbl1 : UILabel!
    @IBOutlet weak var firstNewPriceLbl : UILabel!
    @IBOutlet weak var secOldPriceLbl : UILabel!
    @IBOutlet weak var secNewPriceLbl : UILabel!
    @IBOutlet weak var totalPriceLbl : UILabel!
    @IBOutlet weak var cancelBtn : UIButton!
    @IBOutlet weak var editBtn : UIButton!
    @IBOutlet weak var directionBtn : UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        BaseView.layer.cornerRadius = 5.0
        BaseView.clipsToBounds = true
        BaseView.dropShadow(scale: true, radius: 2.0, opacity: 0.4)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        // Configure the view for the selected state
    }

}
