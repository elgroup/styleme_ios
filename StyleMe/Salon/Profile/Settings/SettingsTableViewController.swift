//
//  SettingsTableViewController.swift
//  StyleMe
//
//  Created by EL INFO on 13/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class SettingsTableViewController: UITableViewController {
    
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelEmailId: UILabel!
    @IBOutlet weak var labelPhoneNumber: UILabel!
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var buttonSwitchNotifications: UISwitch!
    @IBOutlet weak var buttonJoinNow: UIButton!
    @IBOutlet weak var labelStylist: UILabel!
    var dictUser:[String:Any]!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let controller = self.tabBarController as? SalonTabBarController{
            controller.hideCustomTabBar()
        }
        else{
            let controller:PostTabBarController = self.tabBarController as! PostTabBarController
            controller.hideCustomTabBar()
        }
        self.imageContainer.layer.cornerRadius = self.imageContainer.frame.size.width/2
        self.imageContainer.clipsToBounds = true
        
        self.imageViewUser.layer.cornerRadius = self.imageViewUser.frame.size.width/2
        self.imageViewUser.clipsToBounds = true
        
        self.buttonJoinNow.layer.cornerRadius = 5.0
        self.buttonJoinNow.clipsToBounds = true
        
        self.tableView.setContentOffset( CGPoint(x: 0.0, y: 0.0), animated: false)
        self.tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 45.0, right: 0.0)
        
        let countryCode:Int = Int(dictUser["mobile_code"] as! String)!
        let number:String = dictUser["mobile_number"] as! String
        let isStylist:Bool = dictUser["is_stylist"] as! Bool
        if isStylist{
            labelStylist.text = "Joined as a Pro"
            buttonJoinNow.setTitle("Edit Services", for: .normal)
        }
        else{
            labelStylist.text = "Join as a Pro"
            buttonJoinNow.setTitle("Join Now", for: .normal)
        }
        self.labelName.text = dictUser["user_name"] as? String
        self.labelEmailId.text = dictUser["email"] as? String
        self.labelPhoneNumber.text = "+\(countryCode) " + number
        
        let imageUrl:String = (self.dictUser["profile_image"] as? String)!
        let profileImageUrl:URL = URL.init(string: imageUrl)!
        self.imageViewUser.sd_setImage(with: profileImageUrl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
            if (cacheType == SDImageCacheType.none && image != nil) {
                self.imageViewUser.alpha = 0;
                UIView.animate(withDuration: 0.0, animations: { () -> Void in
                    self.imageViewUser.alpha = 1
                })
            } else {
                self.imageViewUser.alpha = 1;
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        if (self.tableView.contentSize.height < self.tableView.frame.size.height) {
            self.tableView.isScrollEnabled = false
        }
        else {
            self.tableView.isScrollEnabled = true
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1{
            let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
            if #available(iOS 13.0, *) {
                let controller = story.instantiateViewController(identifier: "ChangePasswordTableViewController") as! ChangePasswordTableViewController
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                let controller = story.instantiateViewController(withIdentifier:"ChangePasswordTableViewController") as! ChangePasswordTableViewController
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
            
        }
        if indexPath.row == 3{
            let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
            if #available(iOS 13.0, *) {
                let controller = story.instantiateViewController(identifier: "AddressListViewController") as! AddressListViewController
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                let controller = story.instantiateViewController(withIdentifier:"AddressListViewController") as! AddressListViewController
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
            
        }
        if indexPath.row == 6{
            AppCustomClass.showCustomAlert(alertTitle: "", Message: "Are you sure to Logout from StyleUp?", buttonTitle: "Continue", Controller: self) { (success) in
                if success{
                    self.logOutApi()
                }
            }
        }
    }
}
extension SettingsTableViewController{
    @IBAction func backButtonPressed(_ sender : Any){
        if let controller = self.tabBarController as? SalonTabBarController{
            controller.showCustomTabBar()
        }
        else{
            let controller:PostTabBarController = self.tabBarController as! PostTabBarController
            controller.showCustomTabBar()
        }
        self.navigationController?.popViewController(animated: true)
    }
    func logOutApi(){
        let isInternet = Reachability.isConnectedToNetwork()
        if isInternet{
            let appdelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.MainStoryBoard, bundle: nil)
            let tabBarController:BaseForRegistrationViewController = story.instantiateViewController(withIdentifier:"BaseForRegistrationViewController" ) as! BaseForRegistrationViewController
            appdelegate.window!.rootViewController = tabBarController
            appdelegate.window?.makeKeyAndVisible()
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            APIManager.deleteRequestForURLString(true, Constants.URLConstants.signOutUser, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                self.resetDefaults()
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    @IBAction func editProfile(_ sender: Any){
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
        if #available(iOS 13.0, *) {
            let controller = story.instantiateViewController(identifier: "EditProfileTableViewController") as! EditProfileTableViewController
            controller.dictProfile = self.dictUser
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = story.instantiateViewController(withIdentifier:"EditProfileTableViewController") as! EditProfileTableViewController
            controller.dictProfile = self.dictUser
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    @IBAction func joinNowButtonPressed(_ sender:Any){
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.StylistStoryBoard, bundle: nil)
        if #available(iOS 13.0, *) {
            let controller = story.instantiateViewController(identifier: "AddFreelancerTableViewController") as! AddFreelancerTableViewController
            controller.dictProfile = self.dictUser
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = story.instantiateViewController(withIdentifier:"AddFreelancerTableViewController") as! AddFreelancerTableViewController
            controller.dictProfile = self.dictUser
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
}
