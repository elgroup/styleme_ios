//
//  EditProfileTableViewController.swift
//  StyleMe
//
//  Created by EL INFO on 21/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage
class EditProfileTableViewController: UITableViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var imageViewProfile:UIImageView!
    @IBOutlet weak var textFieldName:ACFloatingTextfield!
    @IBOutlet weak var textfieldEmailId:ACFloatingTextfield!
    @IBOutlet weak var textFieldBio:ACFloatingTextfield!
    @IBOutlet weak var containerName:UIView!
    @IBOutlet weak var containerEmail:UIView!
    @IBOutlet weak var containerBio:UIView!
    @IBOutlet weak var buttonUpdate:UIView!
    @IBOutlet weak var containerButtonImagePicker:UIView!
    
    let activityIndicator:ActivityIndicator = ActivityIndicator()
    var dictProfile:[String:Any]!
    var dictParam:[String:Any] = [String:Any]()
    var isEdited:Bool = false
    var img:UIImageView!=nil
    var isImageChanged:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customNavigationBar()
        self.buttonUpdate.isUserInteractionEnabled = false
        self.textFieldName.text = self.dictProfile["user_name"] as? String
        self.textfieldEmailId.text = self.dictProfile["email"] as? String
        self.textFieldBio.text = self.dictProfile["biography"] as? String
        let imageUrl:String = (self.dictProfile["profile_image"] as? String)!
        let profileImageUrl:URL = URL.init(string: imageUrl)!
        self.imageViewProfile.sd_setImage(with: profileImageUrl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
            if (cacheType == SDImageCacheType.none && image != nil) {
                self.imageViewProfile.alpha = 0;
                UIView.animate(withDuration: 0.0, animations: { () -> Void in
                    self.imageViewProfile.alpha = 1
                })
            } else {
                self.imageViewProfile.alpha = 1;
            }
        })
        //  self.imageViewProfile.imageFromServerURL(imageUrl, placeHolder: UIImage.init(named: "user"))
        
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.containerBio.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        self.containerName.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        self.containerEmail.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        self.buttonUpdate.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        self.imageViewProfile.layer.cornerRadius = 85
        self.imageViewProfile.clipsToBounds = true
        self.containerButtonImagePicker.layer.cornerRadius = 30
        self.containerButtonImagePicker.clipsToBounds = true
        
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
}

extension EditProfileTableViewController{
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        menuButton.setImage(UIImage.init(named: "cross"), for: .normal)
        menuButton.addTarget(self, action: #selector(EditProfileTableViewController.crossButtonClicked(_:)), for: .touchUpInside)
        let leftItem1 = UIBarButtonItem()
        leftItem1.customView = menuButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "Edit Profile"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    @objc func crossButtonClicked(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func updateProfileButtonPressed(_ sender : Any){
        if textFieldName.text != self.dictProfile["user_name"] as? String{
            dictParam["user_name"] = textFieldName.text
        }
        if textfieldEmailId.text != self.dictProfile["email"] as? String{
            dictParam["email"] = textfieldEmailId.text
        }
        if textFieldBio.text != self.dictProfile["biography"] as? String{
            dictParam["biography"] = textFieldBio.text
        }
        if isImageChanged{
            dictParam["profile_image"] = (imageViewProfile.image?.jpegData(compressionQuality: 0.5))! //imageViewProfile.image //
        }
        if dictParam.count > 0{
            self.apiRequestForProfileUpdate()
        }
    }
    
    func apiRequestForProfileUpdate(){
        let isInternet = Reachability.isConnectedToNetwork()
        if isInternet{
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            activityIndicator.showActivityIndicator(uiView: self.view)
            if isImageChanged{
                APIManager.putRequestWithMultipart(postData: self.dictParam) { (Done, Error, Success, result, message , isActiveSession) in
                    DispatchQueue.main.async {
                        self.activityIndicator.hideActivityIndicator(uiView: self.view)
                        if Success{
                            let resultDict:[String:Any] = result as! [String:Any]
                            print(resultDict)
                            AppCustomClass.showToastAlert(Title: "", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                            Timer.scheduledTimer(withTimeInterval: 1.1, repeats: false) { (timer) in
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                        else{
                            self.activityIndicator.hideActivityIndicator(uiView: self.view)
                            AppCustomClass.showToastAlert(Title: "Alert", Message: "Something went wrong. Please try again.", Controller: self, Time: 1)
                        }
                    }
                }
            }
            else{
                APIManager.putRequestForURLString(true, Constants.URLConstants.getProfile, headerInfo, self.dictParam) { (Done, Error, Success, result, message , isActiveSession) in
                    DispatchQueue.main.async {
                        self.activityIndicator.hideActivityIndicator(uiView: self.view)
                        if Success{
                            let resultDict:[String:Any] = result as! [String:Any]
                            AppCustomClass.showToastAlert(Title: "", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                            Timer.scheduledTimer(withTimeInterval: 1.1, repeats: false) { (timer) in
                                self.navigationController?.popViewController(animated: true)
                            }
                            
                        }
                        else{
                            AppCustomClass.showToastAlert(Title: "Alert", Message: "Something went wrong. Please try again.", Controller: self, Time: 1)
                        }
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    
    @IBAction func buttonTapped(_ sender:Any)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imag = UIImagePickerController()
            imag.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imag.sourceType = UIImagePickerController.SourceType.photoLibrary;
            //imag.mediaTypes = [kUTTypeImage];
            imag.allowsEditing = false
            self.present(imag, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var selectedImage: UIImage?
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
            self.imageViewProfile.image = selectedImage!
            picker.dismiss(animated: true, completion: nil)
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
            self.imageViewProfile.image = selectedImage!
            picker.dismiss(animated: true, completion: nil)
        }
        self.isImageChanged = true
        buttonUpdate.isUserInteractionEnabled = true
    }
}

extension EditProfileTableViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textFieldName.text != self.dictProfile["user_name"] as? String{
            self.buttonUpdate.isUserInteractionEnabled = true
        }
        else if textfieldEmailId.text != self.dictProfile["email"] as? String{
            
            self.buttonUpdate.isUserInteractionEnabled = true
        }
        else if textFieldBio.text != self.dictProfile["biography"] as? String{
            self.buttonUpdate.isUserInteractionEnabled = true
        }
    }
}
