//
//  ChangePasswordTableViewController.swift
//  StyleMe
//
//  Created by EL INFO on 14/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ChangePasswordTableViewController: UITableViewController {
    
    @IBOutlet weak var containerViewOldPassword: UIView!
    @IBOutlet weak var textFieldOldPassword: ACFloatingTextfield!
    @IBOutlet weak var containerViewNewPassword: UIView!
    @IBOutlet weak var textFieldNewPassword: ACFloatingTextfield!
    @IBOutlet weak var containerViewConfirmPassword: UIView!
    @IBOutlet weak var textFieldConfirmPassword: ACFloatingTextfield!
    @IBOutlet weak var buttonSubmit: UIButton!
    let activityIndicator:ActivityIndicator = ActivityIndicator()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textFieldOldPassword.becomeFirstResponder()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        containerViewOldPassword.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        containerViewNewPassword.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        containerViewConfirmPassword.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        buttonSubmit.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
}
extension ChangePasswordTableViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension ChangePasswordTableViewController{
    //MARK: custom navigation
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationItem.hidesBackButton = true
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        menuButton.setImage(UIImage.init(named: "cross"), for: .normal)
        menuButton.addTarget(self, action: #selector(ChangePasswordTableViewController.crossButtonClicked(_:)), for: .touchUpInside)
        let leftItem1 = UIBarButtonItem()
        leftItem1.customView = menuButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "Change Password"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    //MARK: Cross button action
    @objc func crossButtonClicked(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Submit button action
    @IBAction func submitButtonPressed(_ sender : Any){
        if textFieldOldPassword.text!.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Old Password field can't be blank!!!", Controller: self, Time: 1)
            
        }
        else if textFieldNewPassword.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Password field can't be blank!!!", Controller: self, Time: 1)
        }
        else if textFieldNewPassword.text!.count > 16 || textFieldNewPassword.text!.count < 6{
            AppCustomClass.showToastAlert(Title: "", Message: "Allowed characters for Password is 6 to 16 characters!!!", Controller: self, Time: 2)
            
        }
        else if textFieldConfirmPassword.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Confirm Password field can't be blank!!!", Controller: self, Time: 1)
        }
        else if textFieldConfirmPassword.text!.count > 16 || textFieldConfirmPassword.text!.count < 6{
            AppCustomClass.showToastAlert(Title: "", Message: "Allowed characters for Password is 6 to 16 characters!!!", Controller: self, Time: 1)
        }
        else if textFieldConfirmPassword.text != textFieldNewPassword.text{
            AppCustomClass.showToastAlert(Title: "", Message: "Password and Confirm Password must be same!!!", Controller: self, Time: 1)
        }
        else if textFieldOldPassword.text != textFieldNewPassword.text{
            AppCustomClass.showToastAlert(Title: "", Message: "Password and Current Password can't be same!!!", Controller: self, Time: 1)
        }
            
        else{
            self.view.endEditing(true)
            self.apiForUpdatePassword()
        }
    }
    
    func apiForUpdatePassword(){
        let isInternet = Reachability.isConnectedToNetwork()
        if isInternet{
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            var params:[String:Any] = [String:Any]()
            params["current_password"] = textFieldOldPassword.text
            params["new_password"] = textFieldConfirmPassword.text
            
            activityIndicator.showActivityIndicator(uiView: self.view)
            APIManager.putRequestForURLString(true, Constants.URLConstants.profileUpdate, headerInfo, params) { (Done, error, Success, Result, message, isActiveSession) in
                DispatchQueue.main.async {
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if Success{
                        let resultDict:[String:Any] = Result as! [String:Any]
                        AppCustomClass.showToastAlert(Title: "", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                        Timer.scheduledTimer(withTimeInterval: 1.1, repeats: false) { (timer) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                    else{
                        AppCustomClass.showToastAlert(Title: "Alert", Message: "Something went wrong. Please try again.", Controller: self, Time: 1)
                    }
                }
                
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
            
        }
    }
}
