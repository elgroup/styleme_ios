//
//  AddressListTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 12/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AddressListTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewTypeofAddress: UIImageView!
    @IBOutlet weak var labelFullAddress: UILabel!
    @IBOutlet weak var labelAddressType: UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var buttonDelete: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
