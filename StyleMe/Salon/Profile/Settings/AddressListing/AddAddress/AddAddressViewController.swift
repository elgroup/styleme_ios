//
//  AddAddressViewController.swift
//  StyleMe
//
//  Created by EL INFO on 19/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces

class AddAddressViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var fixedMarker: UIImageView!
    @IBOutlet weak var containerViewCustomAddress: UIView!
    @IBOutlet weak var labelSelectedAddress: UILabel!
    @IBOutlet weak var textFieldHouseNumber: ACFloatingTextfield!
    @IBOutlet weak var textFieldLandMark: ACFloatingTextfield!
    @IBOutlet weak var buttonHome: UIButton!
    @IBOutlet weak var buttonWork: UIButton!
    @IBOutlet weak var buttonOther: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lcTopContainerView: NSLayoutConstraint!
    @IBOutlet weak var lcHeightContainer: NSLayoutConstraint!
    
    var selectedLocation:CLLocationCoordinate2D!
    var heightOfTopConstraint:CGFloat!
    var isEdited:Bool = false
    let locationManager = CLLocationManager()
    var location : CLLocation?
    var currentPlace : GMSPlace!
    var locationSet : Bool = true
    var placesClient: GMSPlacesClient!
    var updatedPlace: GMSAddress!
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    var fromHomeVC: Bool = false
    var isMarkerHidden: Bool! = false
    var isFirst: Bool! = true
    var delegate: PlacePickerDelegate?
    var activityIndicator:ActivityIndicator = ActivityIndicator()
    var selectedAddressType:Int!
    var isUpdate:Bool!
    var dictUpdateAddress:[String:Any]!
    var isHome:Bool!
    var isWork:Bool!
    var isOther:Bool!
    var locality:String!
    
    /// Get distance from top, based on status bar and navigation
    public var topDistance : CGFloat{
        get{
            if self.navigationController != nil && !self.navigationController!.navigationBar.isTranslucent{
                return 0
            }else{
                let barHeight=self.navigationController?.navigationBar.frame.height ?? 0
                let statusBarHeight = UIApplication.shared.isStatusBarHidden ? CGFloat(0) : UIApplication.shared.statusBarFrame.height
                return barHeight + statusBarHeight
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if isUpdate{
            
            self.loadMap()
            let addressType:Int = dictUpdateAddress["address_type"] as! Int
            if isHome && addressType == 0{
                buttonHome.isEnabled = true
            }
            else if isHome{
                buttonHome.isEnabled = false
            }
            else{
                buttonHome.isEnabled = true
            }
            
            if isWork && addressType == 1{
                buttonWork.isEnabled = true
            }
            else if isWork{
                buttonWork.isEnabled = false
            }
            else{
                buttonWork.isEnabled = true
            }
            btnBack.setTitle("Update Address", for: .normal)
            
        }
        else{
            if isHome{
                buttonHome.isEnabled = false
            }
            else{
                buttonHome.isEnabled = true
            }
            if isWork{
                buttonWork.isEnabled = false
            }
            self.setUpLocation()
            self.locationManager.requestAlwaysAuthorization()
            btnBack.setTitle("Add Address", for: .normal)

        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setUpMapView()
        self.customNavigationBar()
        // Get location
        self.mapView.delegate = self
        placesClient = GMSPlacesClient.shared()
        self.lcTopContainerView.priority = UILayoutPriority.defaultLow
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.btnBack.layer.cornerRadius = 5.0
        self.btnBack.clipsToBounds = true
        self.heightOfTopConstraint = self.view.frame.size.height - self.containerViewCustomAddress.frame.size.height
    }
}

extension AddAddressViewController{
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        menuButton.setImage(UIImage.init(named: "back"), for: .normal)
        menuButton.addTarget(self, action: #selector(ConfirmCurrentLocationViewController.crossButtonClicked(_:)), for: .touchUpInside)
        let leftItem1 = UIBarButtonItem()
        leftItem1.customView = menuButton
        
        
        self.navigationItem.leftBarButtonItem = leftItem1
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        searchController?.searchBar.sizeToFit()
        navigationItem.titleView = searchController?.searchBar
        searchController?.searchBar.tintColor = .lightGray
        searchController?.searchBar.delegate = self
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    @objc func crossButtonClicked(_ sender: Any){
        if isEdited{
            self.lcTopContainerView.constant = heightOfTopConstraint
            self.lcHeightContainer.constant = 330
            self.lcTopContainerView.priority = UILayoutPriority.defaultLow
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: {
                self.view.layoutIfNeeded()
            }) { (completed) in
                self.isEdited = false
            }
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func buttonMethods(_ sender:Any){
        let tag = (sender as AnyObject).tag
        if tag == 11{
            let image = UIImage(named: "home_location")?.withRenderingMode(.alwaysTemplate)
            self.buttonHome.setImage(image, for: .normal)
            self.buttonHome.tintColor = UIColor.red
            self.buttonHome.setTitleColor(UIColor.red, for: .normal)
            
            let image2 = UIImage(named: "Office")?.withRenderingMode(.alwaysTemplate)
            self.buttonWork.setImage(image2, for: .normal)
            self.buttonWork.tintColor = UIColor.black
            self.buttonWork.setTitleColor(UIColor.black, for: .normal)
            
            
            let image3 = UIImage(named: "other_location")?.withRenderingMode(.alwaysTemplate)
            self.buttonOther.setImage(image3, for: .normal)
            self.buttonOther.tintColor = UIColor.black
            self.buttonOther.setTitleColor(UIColor.black, for: .normal)
            
            selectedAddressType = 0
            
        }
        if tag == 12{
            let image = UIImage(named: "Office")?.withRenderingMode(.alwaysTemplate)
            self.buttonWork.setImage(image, for: .normal)
            self.buttonWork.tintColor = UIColor.red
            self.buttonWork.setTitleColor(UIColor.red, for: .normal)
            
            
            let image2 = UIImage(named: "home_location")?.withRenderingMode(.alwaysTemplate)
            self.buttonHome.setImage(image2, for: .normal)
            self.buttonHome.tintColor = UIColor.black
            self.buttonHome.setTitleColor(UIColor.black, for: .normal)
            
            
            let image3 = UIImage(named: "other_location")?.withRenderingMode(.alwaysTemplate)
            self.buttonOther.setImage(image3, for: .normal)
            self.buttonOther.tintColor = UIColor.black
            self.buttonOther.setTitleColor(UIColor.black, for: .normal)
            selectedAddressType = 1
            
            
        }
        if tag == 13{
            let image3 = UIImage(named: "home_location")?.withRenderingMode(.alwaysTemplate)
            self.buttonHome.setImage(image3, for: .normal)
            self.buttonHome.tintColor = UIColor.black
            self.buttonHome.setTitleColor(UIColor.black, for: .normal)
            
            
            let image2 = UIImage(named: "Office")?.withRenderingMode(.alwaysTemplate)
            self.buttonWork.setImage(image2, for: .normal)
            self.buttonWork.tintColor = UIColor.black
            self.buttonWork.setTitleColor(UIColor.black, for: .normal)
            
            
            let image = UIImage(named: "other_location")?.withRenderingMode(.alwaysTemplate)
            self.buttonOther.setImage(image, for: .normal)
            self.buttonOther.tintColor = UIColor.red
            self.buttonOther.setTitleColor(UIColor.red, for: .normal)
            selectedAddressType = 2
            
            
        }
        
        if tag == 14{
            
            
        }
    }
    
    @IBAction func buttonAddAddressPressed(_ sender:Any){
        if self.textFieldHouseNumber.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Enter Flat/House/Block No.", Controller: self, Time: 1)
            self.textFieldHouseNumber.becomeFirstResponder()
        }
        else{
            if isEdited{
                self.view.endEditing(true)
                self.lcTopContainerView.constant = heightOfTopConstraint
                self.lcHeightContainer.constant = 330
                self.lcTopContainerView.priority = UILayoutPriority.defaultLow
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: {
                    self.view.layoutIfNeeded()
                }) { (completed) in
                    self.isEdited = false
                }
            }
        }
        
        if isUpdate{
            if self.selectedLocation != nil{
            self.apiForUpdate()
            }
        }
        else{
            self.apiForAddAddress()
        }
    }
    
    func apiForAddAddress(){
        let isInternet = Reachability.isConnectedToNetwork()
        if isInternet{
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            activityIndicator.showActivityIndicator(uiView: self.view)
            var param:[String:Any] = [String:Any]()
            param["address_type"] = selectedAddressType
            param["landmark"] = self.textFieldLandMark.text
            param["street_block"] = self.textFieldHouseNumber.text
            param["latitude"] = self.selectedLocation.latitude
            param["longitude"] = self.selectedLocation.longitude
            param["pickup_address"] = self.labelSelectedAddress.text
            param["locality"] = self.locality
            var dictParam:[String:Any] = [String:Any]()
            dictParam["user_address"] = param
            
            APIManager.postRequestForURLString(true, Constants.URLConstants.addAddress, headerInfo, dictParam) { (Done, Error, Success, result, message , isActiveSession) in
                DispatchQueue.main.async {
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if Success{
                        let resultDict:[String:Any] = result as! [String:Any]
                        AppCustomClass.showToastAlert(Title: "", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                        Timer.scheduledTimer(withTimeInterval: 1.1, repeats: false) { (timer) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                    else{
                        AppCustomClass.showToastAlert(Title: "Alert", Message: "Something went wrong. Please try again.", Controller: self, Time: 1)
                    }
                }
            }
            
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    func apiForUpdate(){
        let isInternet = Reachability.isConnectedToNetwork()
        if isInternet{
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            activityIndicator.showActivityIndicator(uiView: self.view)
            var param:[String:Any] = [String:Any]()
            param["address_type"] = selectedAddressType
            param["landmark"] = self.textFieldLandMark.text
            param["street_block"] = self.textFieldHouseNumber.text
            param["latitude"] = self.selectedLocation.latitude
            param["longitude"] = self.selectedLocation.longitude
            param["pickup_address"] = self.labelSelectedAddress.text
            param["locality"] = self.locality
            param["id"] = dictUpdateAddress["id"]
            var dictParam:[String:Any] = [String:Any]()
            dictParam["user_address"] = param
            
            APIManager.putRequestForURLString(true, Constants.URLConstants.updateAddress, headerInfo, dictParam) { (Done, Error, Success, result, message , isActiveSession) in
                DispatchQueue.main.async {
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if Success{
                        let resultDict:[String:Any] = result as! [String:Any]
                        AppCustomClass.showToastAlert(Title: "", Message: resultDict["message"] as! String, Controller: self, Time: 1)
                        Timer.scheduledTimer(withTimeInterval: 1.1, repeats: false) { (timer) in
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else{
                        AppCustomClass.showToastAlert(Title: "Alert", Message: "Something went wrong. Please try again.", Controller: self, Time: 1)
                    }
                }
            }
            
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    
    // Program mark: Setup current location
    func setUpLocation() -> Void {
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    // MARK : Custom function
    func loadMap() {
        var position:CLLocationCoordinate2D = CLLocationCoordinate2D.init()
        //
        var camera:GMSCameraPosition = GMSCameraPosition.init()
        //
        if isUpdate{
            let latitude:NSNumber = dictUpdateAddress["latitude"] as! NSNumber
            let longitude:NSNumber = dictUpdateAddress!["longitude"] as! NSNumber
            
            let addresstype:Int = dictUpdateAddress["address_type"] as! Int
            let landmark:String = dictUpdateAddress["landmark"] as? String ?? ""
            let street:String = dictUpdateAddress["street_block"] as! String
            textFieldLandMark.text = landmark
            textFieldHouseNumber.text = street
            if addresstype == 0{
                let image = UIImage(named: "home_location")?.withRenderingMode(.alwaysTemplate)
                self.buttonHome.setImage(image, for: .normal)
                self.buttonHome.tintColor = UIColor.red
                self.buttonHome.setTitleColor(UIColor.red, for: .normal)
                
                let image2 = UIImage(named: "Office")?.withRenderingMode(.alwaysTemplate)
                self.buttonWork.setImage(image2, for: .normal)
                self.buttonWork.tintColor = UIColor.black
                self.buttonWork.setTitleColor(UIColor.black, for: .normal)
                
                
                let image3 = UIImage(named: "other_location")?.withRenderingMode(.alwaysTemplate)
                self.buttonOther.setImage(image3, for: .normal)
                self.buttonOther.tintColor = UIColor.black
                self.buttonOther.setTitleColor(UIColor.black, for: .normal)
                
                selectedAddressType = 0
            }
            else if addresstype == 1{
                let image = UIImage(named: "Office")?.withRenderingMode(.alwaysTemplate)
                self.buttonWork.setImage(image, for: .normal)
                self.buttonWork.tintColor = UIColor.red
                self.buttonWork.setTitleColor(UIColor.red, for: .normal)
                
                
                let image2 = UIImage(named: "home_location")?.withRenderingMode(.alwaysTemplate)
                self.buttonHome.setImage(image2, for: .normal)
                self.buttonHome.tintColor = UIColor.black
                self.buttonHome.setTitleColor(UIColor.black, for: .normal)
                
                
                let image3 = UIImage(named: "other_location")?.withRenderingMode(.alwaysTemplate)
                self.buttonOther.setImage(image3, for: .normal)
                self.buttonOther.tintColor = UIColor.black
                self.buttonOther.setTitleColor(UIColor.black, for: .normal)
                selectedAddressType = 1
            }
            else{
                let image3 = UIImage(named: "home_location")?.withRenderingMode(.alwaysTemplate)
                self.buttonHome.setImage(image3, for: .normal)
                self.buttonHome.tintColor = UIColor.black
                self.buttonHome.setTitleColor(UIColor.black, for: .normal)
                
                
                let image2 = UIImage(named: "Office")?.withRenderingMode(.alwaysTemplate)
                self.buttonWork.setImage(image2, for: .normal)
                self.buttonWork.tintColor = UIColor.black
                self.buttonWork.setTitleColor(UIColor.black, for: .normal)
                
                
                let image = UIImage(named: "other_location")?.withRenderingMode(.alwaysTemplate)
                self.buttonOther.setImage(image, for: .normal)
                self.buttonOther.tintColor = UIColor.red
                self.buttonOther.setTitleColor(UIColor.red, for: .normal)
                selectedAddressType = 2
            }
            position = CLLocationCoordinate2DMake((CLLocationDegrees(truncating: latitude)), CLLocationDegrees(truncating: longitude))
            _ = GMSMarker(position: position)
            camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(truncating: latitude), longitude: CLLocationDegrees(truncating: longitude), zoom: 16.0)
            
        }
        else{
            position = CLLocationCoordinate2DMake((self.location?.coordinate.latitude)!, (self.location?.coordinate.longitude)!)
            _ = GMSMarker(position: position)
            camera = GMSCameraPosition.camera(withLatitude: (self.location?.coordinate.latitude)!, longitude: (self.location?.coordinate.longitude)!, zoom: 16.0)
        }
        
        //        let position =
        
        // Set camera position and map zoom level
        
        
        self.mapView!.animate(to: camera)
        
        self.mapView.settings.compassButton = true
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
    }
    
    
    @objc func eventWith(timer: Timer!) {
        let address = updatedPlace.lines
        self.searchController?.searchBar.text = address![0]
        // self.actInd.stopAnimating()
        //        self.btnBack.isEnabled = true
        //        self.btnBack.isHidden = false
    }
}
//   AddAddressViewController
extension AddAddressViewController:GMSAutocompleteResultsViewControllerDelegate{
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        self.currentPlace = place
        self.searchController?.isActive = false
        
        if fromHomeVC {
            delegate?.updateLocationData(address: place.formattedAddress, lat: place.coordinate.latitude, long: place.coordinate.longitude)
            self.dismiss(animated: true, completion: nil)
        }
        else {
            let camera = GMSCameraPosition.camera(withLatitude: self.currentPlace.coordinate.latitude, longitude: self.currentPlace.coordinate.longitude, zoom: 16.0)
            self.mapView!.animate(to: camera)
            self.searchController?.searchBar.text = place.formattedAddress
        }
        
        print("Place name: \(String(describing: place.name))")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    

}
extension AddAddressViewController:UISearchBarDelegate{
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Do some search stuff
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if fromHomeVC {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension AddAddressViewController:CLLocationManagerDelegate{
    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = (locations.last)!
        if location != nil && self.locationSet {
            // Do any additional setup after loading the view.
            self.loadMap()
            self.locationSet = false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
        case .restricted:
            print("Location Ristricted")
            break
        case .denied:
            print("Location Denied")
            break
            
        @unknown default:
            print("Error")
        }
    }
}

extension AddAddressViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if !isEdited{
            self.lcTopContainerView.constant = 0
            self.lcHeightContainer.constant = self.view.frame.size.height - topDistance
            self.lcTopContainerView.priority = UILayoutPriority.defaultHigh
            self.view.endEditing(true)
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: {
                self.view.layoutIfNeeded()
            }) { (completed) in
                self.isEdited = true
                textField.becomeFirstResponder()
            }}
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if isEdited{
            self.lcTopContainerView.constant = heightOfTopConstraint
            self.lcHeightContainer.constant = 330
            self.lcTopContainerView.priority = UILayoutPriority.defaultLow
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: {
                self.view.layoutIfNeeded()
            }) { (completed) in
                self.isEdited = false
            }
        }
        return true
    }
}

extension AddAddressViewController:GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        //self.btnBack.isEnabled = true
        self.searchController?.searchBar.text = ""
        print(mapView.camera.target.latitude)
        print(mapView.camera.target.longitude)
        
        let position = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(position) { response, error in
            if error != nil {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            } else {
                if let places = response?.results() {
                    if let place = places.first {
                        self.locality = place.locality
                        self.updatedPlace = places.first
                        
                        if let lines = place.lines {
                            print("GEOCODE: Formatted Address: \(lines[0])")
                            self.searchController?.searchBar.text = lines[0]
                            self.labelSelectedAddress.text = lines[0]
                        }
                        if let coordinate:CLLocationCoordinate2D = place.coordinate{
                            self.selectedLocation = CLLocationCoordinate2D.init()
                            self.selectedLocation.latitude = Double(coordinate.latitude)
                            self.selectedLocation.longitude = Double(coordinate.longitude)
                        }
                    } else {
                        print("GEOCODE: nil first in places")
                    }
                } else {
                    print("GEOCODE: nil in places")
                }
            }
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if isMarkerHidden {
            self.fixedMarker.isHidden = false
            self.mapView.bringSubviewToFront(self.fixedMarker)
            self.isMarkerHidden = false
        }
    }
    
    func mapViewSnapshotReady(_ mapView: GMSMapView) {
        self.isMarkerHidden = true
        self.isFirst = false
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if !isFirst {
            //  self.actInd.startAnimating()
            
            _ = Timer.scheduledTimer(timeInterval: 1.0,
                                     target: self,
                                     selector: #selector(eventWith(timer:)),
                                     userInfo: [ "foo" : "bar" ],
                                     repeats: false)
        }
    }
    
}
