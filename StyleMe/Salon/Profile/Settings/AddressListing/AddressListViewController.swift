//
//  AddressListViewController.swift
//  StyleMe
//
//  Created by EL INFO on 12/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AddressListViewController: UIViewController {
    
    @IBOutlet weak var buttonAddadress: UIButton!
    @IBOutlet weak var tableViewAddressList: UITableView!
    var activityIndicator:ActivityIndicator = ActivityIndicator()
    var arrAddressList:[Any] = [Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customNavigationBar()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAddressList()
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.buttonAddadress.layer.cornerRadius = 5.0
        self.buttonAddadress.clipsToBounds = true
    }
}

extension AddressListViewController{
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        menuButton.setImage(UIImage.init(named: "cross"), for: .normal)
        menuButton.addTarget(self, action: #selector(AddressListViewController.crossButtonClicked(_:)), for: .touchUpInside)
        let leftItem1 = UIBarButtonItem()
        leftItem1.customView = menuButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        labelTitle.text = "Manage Address"
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    @objc func crossButtonClicked(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getAddressList(){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            
            self.activityIndicator.showActivityIndicator(uiView: self.view)
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            APIManager.getRequestForURLString(true, "addresses", headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if Success{
                        let resultDict = result as! [String:Any]
                        self.arrAddressList = (resultDict["user_address"] as? [Any])!
                        
                        self.tableViewAddressList.reloadData()
                    }
                    else{
                        AppCustomClass.showToastAlert(Title:"", Message:"No Address found.", Controller: self, Time: 1)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    @IBAction func buttonAddAddressPressed(_ sender : Any){
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
        var controller = AddAddressViewController.init()
        
        
        if #available(iOS 13.0, *) {
            controller = story.instantiateViewController(identifier: "AddAddressViewController") as! AddAddressViewController
        } else {
            controller = story.instantiateViewController(withIdentifier:"AddAddressViewController") as! AddAddressViewController
        }
        
        let dictUpdate:[String:Any] = arrAddressList[(sender as AnyObject).tag ] as! [String:Any]
        let predicatHome:NSPredicate = NSPredicate(format: "address_type == %d", 0)
        let arrayHome:NSArray = (arrAddressList as NSArray).filtered(using: predicatHome) as NSArray
        
        let predicatWork:NSPredicate = NSPredicate(format: "address_type == %d", 1)
        let arrayWork:NSArray = (arrAddressList as NSArray).filtered(using: predicatWork) as NSArray
        
        let predicatOther:NSPredicate = NSPredicate(format: "address_type == %d", 2)
        let arrayOther:NSArray = (arrAddressList as NSArray).filtered(using: predicatOther) as NSArray
        
        controller.isUpdate = true
        controller.dictUpdateAddress = dictUpdate
        if arrayHome.count == 0{
            controller.isHome = false
        }
        else{
            controller.isHome = true
        }
        if arrayWork.count == 0{
            controller.isWork = false
        }
        else{
            controller.isWork = true
        }
        if arrayOther.count == 0{
            controller.isOther = false
        }
        else{
            controller.isOther = true
        }
        controller.isUpdate = false
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func deleteAddressPressed(_ sender: Any){
        let tag:Int = (sender as AnyObject).tag
        let dict:[String:Any] = arrAddressList[tag] as! [String:Any]
        let id:Int = dict["id"] as! Int
        AppCustomClass.showCustomAlert(alertTitle: "", Message: "Are you sure to Delete the selected Address?", buttonTitle: "Delete", Controller: self) { (success) in
            self.deleteApiForAddress(id: id)
        }
    }
    func deleteApiForAddress(id:Int){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            self.activityIndicator.showActivityIndicator(uiView: self.view)
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            var param:[String:Any] = [String:Any]()
            param["address_id"] = id
            APIManager.deleteRequestForURLString(true, Constants.URLConstants.deleteAddress, headerInfo, param) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if Success{
                        self.getAddressList()
                        
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    
    @objc func updateButtonPressed(_ sender: Any){
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
        var controller:AddAddressViewController = AddAddressViewController()
        if #available(iOS 13.0, *) {
            controller = story.instantiateViewController(identifier: "AddAddressViewController") as! AddAddressViewController
        } else {
            controller = story.instantiateViewController(withIdentifier:"AddAddressViewController") as! AddAddressViewController
        }
        let dictUpdate:[String:Any] = arrAddressList[(sender as AnyObject).tag ] as! [String:Any]
        let predicatHome:NSPredicate = NSPredicate(format: "address_type == %d", 0)
        let arrayHome:NSArray = (arrAddressList as NSArray).filtered(using: predicatHome) as NSArray
        
        let predicatWork:NSPredicate = NSPredicate(format: "address_type == %d", 1)
        let arrayWork:NSArray = (arrAddressList as NSArray).filtered(using: predicatWork) as NSArray
        
        let predicatOther:NSPredicate = NSPredicate(format: "address_type == %d", 2)
        let arrayOther:NSArray = (arrAddressList as NSArray).filtered(using: predicatOther) as NSArray
        
        controller.isUpdate = true
        controller.dictUpdateAddress = dictUpdate
        if arrayHome.count == 0{
            controller.isHome = false
        }
        else{
            controller.isHome = true
        }
        if arrayWork.count == 0{
            controller.isWork = false
        }
        else{
            controller.isWork = true
        }
        if arrayOther.count == 0{
            controller.isOther = false
        }
        else{
            controller.isOther = true
        }
        controller.isUpdate = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
}


extension AddressListViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableViewAddressList.estimatedRowHeight
    }
    
}
extension AddressListViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAddressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressListTableViewCell", for: indexPath) as! AddressListTableViewCell
        let dictAddress:[String:Any] = arrAddressList[indexPath.row] as! [String:Any]
        let addressType:Int = dictAddress["address_type"] as! Int
        if addressType == 1{
            cell.labelAddressType.text = "Work"
            cell.imageViewTypeofAddress?.image = UIImage.init(named: "Office")
        }
        else if addressType == 0{
            cell.labelAddressType.text = "Home"
            cell.imageViewTypeofAddress?.image = UIImage.init(named: "home_location")
        }
        else{
            cell.labelAddressType.text = "Other"
            cell.imageViewTypeofAddress?.image = UIImage.init(named: "other_location")
        }
        let street:String = dictAddress["street_block"] as! String
        let landmark:String = dictAddress["landmark"] as? String ?? ""
        let pickupAddress:String = dictAddress["pickup_address"] as! String
        let address:String =  street + " ,\(landmark)" + " ,\(pickupAddress)"
        cell.labelFullAddress.text = address
        cell.buttonDelete.tag = indexPath.row
        cell.buttonEdit.tag = indexPath.row
        
        cell.buttonDelete.addTarget(self, action: #selector(AddressListViewController.deleteAddressPressed(_:)), for: .touchUpInside)
        cell.buttonEdit.addTarget(self, action: #selector(AddressListViewController.updateButtonPressed(_:)), for: .touchUpInside)
        
        return cell
    }
    
    
}
