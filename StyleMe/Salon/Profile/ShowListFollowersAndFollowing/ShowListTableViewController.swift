//
//  ShowListTableViewController.swift
//  StyleMe
//
//  Created by EL INFO on 15/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ShowListTableViewController: UITableViewController {
    
    var isFollower:Bool!
    var activityIndicator:ActivityIndicator = ActivityIndicator()
    var arrList:[Any] = [Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customNavigationBar()
        self.tableView.setContentOffset( CGPoint(x: 0.0, y: 0.0), animated: false)
        self.tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 45.0, right: 0.0)
        
        self.fetchList()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //tableView.dequeueReusableCell(withIdentifier: "ShowListTableViewCell", for: indexPath)
        let cell:ShowListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ShowListTableViewCell", for: indexPath) as! ShowListTableViewCell
        let dict:[String:Any] = arrList[indexPath.row] as! [String:Any]
        cell.labelUserName.text = dict["name"] as? String
        cell.imageView?.backgroundColor = .red
        cell.imageViewUser.imageFromServerURL(dict["profile_image"] as! String, placeHolder: UIImage.init())
        return cell
    }
    
    
    
}

extension ShowListTableViewController{
    //MARK: custom navigation
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        menuButton.setImage(UIImage.init(named: "cross"), for: .normal)
        menuButton.addTarget(self, action: #selector(ChangePasswordTableViewController.crossButtonClicked(_:)), for: .touchUpInside)
        let leftItem1 = UIBarButtonItem()
        leftItem1.customView = menuButton
        
        let labelTitle = UILabel()
        labelTitle.font = UIFont.PoppinsMedium(ofSize: 18)
        if isFollower{
            labelTitle.text = "Followers"
        }
        else{
            labelTitle.text = "Followings"
            
        }
        let leftItem2 = UIBarButtonItem()
        leftItem2.customView = labelTitle
        self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
        
        self.navigationController?.navigationBar.shouldRemoveShadow(false)
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    //MARK: Cross button action
    @objc func crossButtonClicked(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    func fetchList(){
        let isInternet = Reachability.isConnectedToNetwork()
        if isInternet{
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            var strUrl:String = String()
            if isFollower{
                strUrl = Constants.URLConstants.followersList
            }
            else{
                strUrl = Constants.URLConstants.followingList
            }
            
            
            activityIndicator.showActivityIndicator(uiView: self.view)
            APIManager.getRequestForURLString(true, strUrl, headerInfo, [:]) { (Done, error, Success, Result, message, isActiveSession) in
                DispatchQueue.main.async {
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if Success{
                        let resultDict:[String:Any] = Result as! [String:Any]
                        if self.isFollower{
                            self.arrList = resultDict["all_followers"] as! [Any]
                        }
                        else{
                            self.arrList = resultDict ["you_following"] as! [Any]
                        }
                        self.tableView.reloadData()
                        
                    }
                    else{
                        let result:[String:Any] = Result as! [String:Any]
                        let message:String =  result["message"] as! String
                        AppCustomClass.showToastAlert(Title: "Alert", Message: message, Controller: self, Time: 1)
                    }
                }
                
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
            
        }
    }
}
