//
//  ShowListTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 15/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ShowListTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewUser:UIImageView!
    @IBOutlet weak var labelUserName:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageViewUser.layer.cornerRadius = self.imageViewUser.frame.size.width/2
        self.imageViewUser.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
