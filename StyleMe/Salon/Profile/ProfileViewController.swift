//
//  ProfileViewController.swift
//  StyleMe
//
//  Created by EL INFO on 16/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import  SDWebImage

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var userImage:UIImageView!
    @IBOutlet weak var imageContainer:UIView!
    @IBOutlet weak var labelName:UILabel!
    @IBOutlet weak var buttonFollowersCount:UIButton!
    @IBOutlet weak var buttonFollowingCount:UIButton!
    @IBOutlet weak var buttonPostCount:UIButton!
    @IBOutlet weak var buttonSettings:UIButton!
    @IBOutlet weak var labelBio:UILabel!
    @IBOutlet weak var collectionViewPost:UICollectionView!
    @IBOutlet weak var topConstraintLC: NSLayoutConstraint!
    @IBOutlet weak var shadowContainerView:UIView!
    @IBOutlet weak var gridButton:UIButton!
    @IBOutlet weak var listButton:UIButton!
    @IBOutlet weak var containerViewQRCode:UIView!
    @IBOutlet weak var labelStylistCode:UILabel!
    @IBOutlet weak var buttonCalendarAppointment:UIButton!
    var dictUser:[String:Any] = [String:Any]()
    var activityIndicator: ActivityIndicator! = ActivityIndicator()
    var isGrid = true
    var isLoaded = false
    var topHeadMargin:CGFloat!
    var arrPost:[Any] = [Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageContainer.layer.cornerRadius = self.imageContainer.frame.size.width/2
        self.imageContainer.clipsToBounds = true
        
        self.userImage.layer.cornerRadius = self.userImage.frame.size.width/2
        self.userImage.clipsToBounds = true
        
        let nib = UINib.init(nibName: "GridPostCollectionViewCell", bundle: nil)
        self.collectionViewPost.register(nib, forCellWithReuseIdentifier: "GridPostCollectionViewCell")
        
        
        topHeadMargin = topConstraintLC.constant
        self.navigationController?.navigationBar.isHidden = true
        self.shadowContainerView.layer.cornerRadius = 5.0
        self.shadowContainerView.clipsToBounds = true
        self.shadowContainerView.dropShadow(scale: true, radius: 4.0, opacity: 0.4)
        
        let width:CGFloat = (UIScreen.main.bounds.size.width - 26)/3
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: width, height: width)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.headerReferenceSize = CGSize(width: 0, height: 0)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionViewPost.collectionViewLayout = layout
        self.containerViewQRCode.layer.cornerRadius = 5.0
        self.containerViewQRCode.dropShadow(radius: 3, opacity: 0.3)
        self.containerViewQRCode.isHidden = true
        self.labelStylistCode.isHidden = true
        self.buttonCalendarAppointment.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.collectionViewPost.setContentOffset( CGPoint(x: 0.0, y: 0.0), animated: false)
        self.collectionViewPost.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 45.0, right: 0.0)
        self.getProfile()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (timer) in
            self.isLoaded = true
        }
        
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
}

//MARK: Extension of Collection view Delegate and Datasource
extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPost.count
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isGrid{
            let cell:GridPostCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridPostCollectionViewCell", for: indexPath) as! GridPostCollectionViewCell
            let post = arrPost[indexPath.row] as! [String:Any]
            let postImage:[Any] = post["post_image"] as! [Any]
            if postImage.count > 0{
                let postImageUrlDictionary:[String:Any] = postImage[0] as! [String:Any]
                let postImageUrlString:String = postImageUrlDictionary["url"] as! String
                let imageUrlString:String = Constants.apiConstants.baseUrlImage + postImageUrlString
                var imageURL:URL = URL.init(string: imageUrlString)!
                let isVideo:Bool = post["is_video"] as! Bool
                if isVideo{
                    let thumb = postImageUrlDictionary["thumb"] as! [String:Any]
                    var thumbImageString:String = thumb["url"] as! String
                    thumbImageString = Constants.apiConstants.baseUrlImage + thumbImageString
                    imageURL = URL.init(string: thumbImageString)!
                    cell.imagePlay.isHidden = false
                    cell.imageViewPost.sd_setImage(with: imageURL, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
                        if (cacheType == SDImageCacheType.none && image != nil) {
                            cell.imageViewPost.alpha = 0;
                            UIView.animate(withDuration: 0.0, animations: { () -> Void in
                                cell.imageViewPost.alpha = 1
                            })
                        } else {
                            cell.imageViewPost.alpha = 1;
                        }
                    })
                }
                else{
                    cell.imagePlay.isHidden = true
                    cell.imageViewPost.sd_setImage(with: imageURL, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
                        if (cacheType == SDImageCacheType.none && image != nil) {
                            cell.imageViewPost.alpha = 0;
                            UIView.animate(withDuration: 0.0, animations: { () -> Void in
                                cell.imageViewPost.alpha = 1
                            })
                        } else {
                            cell.imageViewPost.alpha = 1;
                        }
                    })
                }
                
                // cell.imageViewPost.imageFromServerURL(imageUrlString, placeHolder: UIImage.init())
            }
            return cell
        }
        else{
            let cell:HorizonatlLayoutPosCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HorizonatlLayoutPosCollectionViewCell", for: indexPath) as! HorizonatlLayoutPosCollectionViewCell
            let post = arrPost[indexPath.row] as! [String:Any]
            let description:String = post["description"] as! String
            let postImage:[Any] = post["post_image"] as! [Any]
            let isLiked:Bool = post["is_liked"] as! Bool
            
            if isLiked{
                cell.buttonLike.setImage(UIImage.init(named: "Like"), for: .normal)
            }
            else{
                cell.buttonLike.setImage(UIImage.init(named: "dislike"), for: .normal)
            }
            
            if !AppCustomClass.isNsnullOrNil(object: post["like_count"] as AnyObject){
                let likeCount:Int = (post["like_count"] as! NSNumber).intValue
                cell.labelLike.text = "\(likeCount)"
            }
            if !AppCustomClass.isNsnullOrNil(object: post["comments_count"] as AnyObject){
                let commentCount:Int = (post["comments_count"] as! NSNumber).intValue
                cell.LabelCommentCount.text = "\(commentCount)"
            }
            if !AppCustomClass.isNsnullOrNil(object: post["share_counts"] as AnyObject){
                let shareCount:Int = (post["share_counts"] as! NSNumber).intValue
                cell.labelShareCount.text = "\(shareCount)"
            }
            cell.textViewDescription.text = description
            cell.textViewDescription.textColor = .lightGray
            cell.textViewDescription.resolveHashTags()
            //cell.labelNameUser.text = userName
            cell.buttonLike.tag = indexPath.row
            cell.buttonComment.tag = indexPath.row
            cell.buttonLike.addTarget(self, action: #selector(buttonLike(_sender:)), for: .touchUpInside)
            cell.buttonComment.addTarget(self, action: #selector(buttonCommentPressed( _sender:)), for: .touchUpInside)
            
            if postImage.count > 0{
                let postImageUrlDictionary:[String:Any] = postImage[0] as! [String:Any]
                let postImageUrlString:String = postImageUrlDictionary["url"] as! String
                let imageUrlString:String = Constants.apiConstants.baseUrlImage + postImageUrlString
                let imageURL:URL = URL.init(string: imageUrlString)!
                cell.imageViewPost.sd_setImage(with: imageURL, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
                    if (cacheType == SDImageCacheType.none && image != nil) {
                        cell.imageViewPost.alpha = 0;
                        UIView.animate(withDuration: 0.0, animations: { () -> Void in
                            cell.imageViewPost.alpha = 1
                        })
                    } else {
                        cell.imageViewPost.alpha = 1;
                    }
                })
                // cell.imageViewPost.imageFromServerURL(imageUrlString, placeHolder: UIImage.init())
            }
            return cell
        }
    }
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        let width:CGFloat = collectionViewPost.frame.size.width/3 - 0.1
    //        return CGSize(width: width, height: width)//collectionView.frame.size.height)
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0.0
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
    //        return 0.0
    //    }
    
    
}

extension ProfileViewController:UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !isLoaded{
            return
        }
        if scrollView == collectionViewPost {
            if scrollView.contentOffset.y > 40 && topConstraintLC.constant > 0 {
                
                scrollView.bounces = false
                UIView.animate(withDuration: 0.3, animations: {
                    
                    self.topConstraintLC.constant = 0
                    scrollView.contentOffset = CGPoint.init(x: 0, y: 0)
                    self.view.layoutIfNeeded()
                    
                }) { (done) in
                    scrollView.bounces = true
                }
            }
            else if scrollView.contentOffset.y < -40 && topConstraintLC.constant != topHeadMargin {
                
                scrollView.bounces = false
                UIView.animate(withDuration: 0.3, animations: {
                    
                    self.topConstraintLC.constant = self.topHeadMargin
                    self.view.layoutIfNeeded()
                }) { (done) in
                    
                    scrollView.bounces = true
                }
            }
        }
    }
    
}

//MARK: Extension for methods of ProfileViewController
extension ProfileViewController{
    
    //MARK: Method for changing layout in grid and horizontal
    @IBAction func changeLayoutPressed( _ sender: Any){
        if (sender as AnyObject).tag == 10{
            let nib = UINib.init(nibName: "GridPostCollectionViewCell", bundle: nil)
            self.collectionViewPost.register(nib, forCellWithReuseIdentifier: "GridPostCollectionViewCell")
            let width:CGFloat = (UIScreen.main.bounds.size.width - 26)/3
            let layout = UICollectionViewFlowLayout()
            layout.itemSize = CGSize(width: width, height: width)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.headerReferenceSize = CGSize(width: 0, height: 0)
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            collectionViewPost.collectionViewLayout = layout
            collectionViewPost.reloadData()
            gridButton.setImage(UIImage.init(named: "grid_Selected"), for: .normal)
            listButton.setImage(UIImage.init(named: "list_Unselected"), for: .normal)
            isGrid = true
        }
        else{
            isLoaded = false
            let nib = UINib.init(nibName: "HorizonatlLayoutPosCollectionViewCell", bundle: nil)
            self.collectionViewPost.register(nib, forCellWithReuseIdentifier: "HorizonatlLayoutPosCollectionViewCell")
            let width:CGFloat = (UIScreen.main.bounds.size.width - 26)
            let layout = UICollectionViewFlowLayout()
            layout.itemSize = CGSize(width: width, height: width)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.headerReferenceSize = CGSize(width: 0, height: 0)
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            collectionViewPost.collectionViewLayout = layout
            gridButton.setImage(UIImage.init(named: "grid_Unselected"), for: .normal)
            listButton.setImage(UIImage.init(named: "list_Selected"), for: .normal)
            collectionViewPost.reloadData()
            self.collectionViewPost?.scrollToItem(at: IndexPath(row: 0, section: 0),at: .top, animated: false)
            isGrid = false
            isLoaded = true
        }
        
    }
    //MARK: Follower list button action
    @IBAction func followersList( _ sender :Any){
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
        if #available(iOS 13.0, *) {
            let controller = story.instantiateViewController(identifier: "ShowListTableViewController") as! ShowListTableViewController
            controller.isFollower = true
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = story.instantiateViewController(withIdentifier:"ShowListTableViewController") as! ShowListTableViewController
            controller.isFollower = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    //MARK: Following button action
    @IBAction func followingList( _ sender : Any){
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
        if #available(iOS 13.0, *) {
            let controller = story.instantiateViewController(identifier: "ShowListTableViewController") as! ShowListTableViewController
            controller.isFollower = false
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = story.instantiateViewController(withIdentifier:"ShowListTableViewController") as! ShowListTableViewController
            controller.isFollower = false
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    //MARK: Setting button Action
    @IBAction func settingsButtonPressed( _ sender : Any){
        if dictUser.count > 0{
            let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
            if #available(iOS 13.0, *) {
                let controller = story.instantiateViewController(identifier: "SettingsTableViewController") as! SettingsTableViewController
                controller.dictUser = self.dictUser
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                let controller = story.instantiateViewController(withIdentifier:"SettingsTableViewController") as! SettingsTableViewController
                controller.dictUser = self.dictUser
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    //MARK: Api for user profile
    func getProfile(){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            self.activityIndicator.showActivityIndicator(uiView: self.view)
            APIManager.getRequestForURLString(true, Constants.URLConstants.getProfile, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    
                    if Success{
                        let resultDict = result as! [String:Any]
                        let dictUser = (resultDict["user"] as? [String:Any])!
                        self.dictUser = dictUser
                        let name:String = dictUser["user_name"] as! String
                        self.labelName.text = name
                        let postCount:Int = dictUser["total_post"] as! Int
                        self.buttonPostCount.setTitle("\(postCount)", for: .normal)
                        let followersCount:Int = dictUser["followers"] as!Int
                        self.buttonFollowersCount.setTitle("\(followersCount)", for: .normal)
                        let followingCount:Int = dictUser["following"] as! Int
                        self.buttonFollowingCount.setTitle("\(followingCount)", for:.normal)
                        let bio:String = dictUser["biography"] as! String
                        self.labelBio.text = bio
                        let isStylist:Bool = dictUser["is_stylist"] as! Bool
                        if isStylist{
                            self.containerViewQRCode.isHidden = false
                            self.labelStylistCode.isHidden = false
                            self.buttonCalendarAppointment.isHidden = false
                        }
                        let profileImage:String = dictUser["profile_image"] as! String
                        let profileImageUrl:URL = URL.init(string: profileImage)!
                        self.userImage.sd_setImage(with: profileImageUrl, placeholderImage:UIImage.init(), completed: { (image, error, cacheType, url) -> Void in
                            if (cacheType == SDImageCacheType.none && image != nil) {
                                self.userImage.alpha = 0;
                                UIView.animate(withDuration: 0.0, animations: { () -> Void in
                                    self.userImage.alpha = 1
                                })
                            } else {
                                self.userImage.alpha = 1;
                            }
                        })
                        
                    }
                    else{
                        AppCustomClass.showToastAlert(Title:"", Message: message!, Controller: self, Time: 1)
                        self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    }
                }
                self.getUserPosts()
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
            
        }
    }
    //MARK: Api For users post
    func getUserPosts(){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            APIManager.getRequestForURLString(true, Constants.URLConstants.userPosts, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    self.activityIndicator.hideActivityIndicator(uiView: self.view)
                    if Success{
                        let resultDict = result as! [String:Any]
                        self.arrPost = (resultDict["user_posts"] as? [Any])!
                        // = dictUser["user_posts"] as! [Any]
                        self.collectionViewPost.reloadData()
                    }
                    else{
                        AppCustomClass.showToastAlert(Title:"", Message: message!, Controller: self, Time: 1)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    
    //MARK: Button Like action
    @objc func buttonLike( _sender:Any){
        let isReachable = Reachability.isConnectedToNetwork()
        if isReachable{
            let tag:Int = (_sender as AnyObject).tag
            var dict:[String:Any] = arrPost[tag] as! [String:Any]
            let postId = dict["id"] as! Int
            
            var headerInfo:[String:String] = [String:String]()
            headerInfo["device-id"] = UIDevice.current.identifierForVendor!.uuidString
            headerInfo["device-type"] = "ios"
            headerInfo["auth-token"] = UserDefaults.standard.value(forKey: Constants.Login.authToken) as? String
            headerInfo["locale"] = "en"
            let strUrl:String = "posts/" + "\(postId)/like_posts"
            APIManager.postRequestForURLString(true, strUrl, headerInfo, [:]) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async {
                    if Success{
                        let resultDict = result as! [String:Any]
                        if !self.isGrid{
                            let postCell = self.getHorizontalCell(index: tag)
                            let isLiked:Bool = resultDict["is_liked"] as! Bool
                            if isLiked{
                                postCell?.buttonLike.setImage(UIImage.init(named: "Like"), for: .normal)
                            }
                            else{
                                postCell?.buttonLike.setImage(UIImage.init(named: "dislike"), for: .normal)
                            }
                            let likeCount:Int = resultDict["like_count"] as! Int
                            postCell?.labelLike.text = String(likeCount)
                            dict["like_count"] = resultDict["like_count"]
                            dict["is_liked"] = isLiked
                            self.arrPost.remove(at: tag)
                            self.arrPost.insert(dict, at: tag)
                        }
                    }
                    else{
                        AppCustomClass.showToastAlert(Title:"", Message: message!, Controller: self, Time: 1)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title:"", Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 1)
        }
    }
    
    func getGridCell(index: Int) -> GridPostCollectionViewCell? {
        guard let cell = self.collectionViewPost.cellForItem(at:  IndexPath(row : index, section : 0)) as? GridPostCollectionViewCell else {return nil}
        return cell
    }
    func getHorizontalCell(index: Int) -> HorizonatlLayoutPosCollectionViewCell? {
        guard let cell = self.collectionViewPost.cellForItem(at:  IndexPath(row : index, section : 0)) as? HorizonatlLayoutPosCollectionViewCell else {return nil}
        return cell
    }
    
    @objc func buttonCommentPressed( _sender:Any){
        let button:UIButton = _sender as! UIButton
        
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.PostTabControllerStoryBoard, bundle: nil)
        if #available(iOS 13.0, *) {
            let controller = story.instantiateViewController(identifier: "CommentViewController") as! CommentViewController
            controller.dictPost = arrPost[button.tag] as? [String:Any]
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = story.instantiateViewController(withIdentifier:"CommentViewController") as! CommentViewController
            controller.dictPost = arrPost[button.tag] as? [String:Any]
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        
    }
    @IBAction func buttonBookingsPressed(){
        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.StylistStoryBoard, bundle: nil)
        var controller:StylistBookingViewController
        if #available(iOS 13.0, *) {
           controller  = story.instantiateViewController(identifier: "StylistBookingViewController") as! StylistBookingViewController
        } else {
            controller = story.instantiateViewController(withIdentifier:"StylistBookingViewController") as! StylistBookingViewController
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
