//
//  ProfileInfoTableViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 06/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ProfileInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var userImage:UIImageView!
    @IBOutlet weak var imageContainer:UIView!
    @IBOutlet weak var labelName:UILabel!
    @IBOutlet weak var buttonFollowersCount:UIButton!
    @IBOutlet weak var buttonFollowingCount:UIButton!
    @IBOutlet weak var buttonPostCount:UIButton!
    @IBOutlet weak var buttonSettings:UIButton!
    @IBOutlet weak var labelBio:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
