//
//  GridPostCollectionViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 11/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class GridPostCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var baseView:UIView!
    @IBOutlet weak var imageViewPost:UIImageView!
    @IBOutlet weak var imagePlay:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageViewPost.layer.cornerRadius = 5.0
        imageViewPost.clipsToBounds = true
        baseView.layer.cornerRadius = 5.0
        baseView.clipsToBounds = true
        // Initialization code
    }

}
