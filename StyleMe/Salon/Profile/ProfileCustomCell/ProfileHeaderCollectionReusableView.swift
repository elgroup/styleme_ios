//
//  ProfileHeaderCollectionReusableView.swift
//  StyleMe
//
//  Created by EL INFO on 06/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ProfileHeaderCollectionReusableView: UICollectionReusableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.purple
        
        // Customize here
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
}
