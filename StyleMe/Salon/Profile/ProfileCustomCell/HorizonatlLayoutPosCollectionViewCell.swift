//
//  HorizonatlLayoutPosCollectionViewCell.swift
//  StyleMe
//
//  Created by EL INFO on 13/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class HorizonatlLayoutPosCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewPost: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var buttonLike: UIButton!
    @IBOutlet weak var labelLike: UILabel!
    @IBOutlet weak var buttonComment: UIButton!
    @IBOutlet weak var LabelCommentCount: UILabel!
    @IBOutlet weak var labelShareCount: UILabel!
    @IBOutlet weak var buttonShare: UIButton!
    @IBOutlet weak var textViewDescription: UITextView!
   
    @IBOutlet weak var shadowView:UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.cornerRadius = 10.0
        containerView.clipsToBounds = true
        shadowView.dropShadow(scale: true, radius: 4.0,opacity: 0.4)
    }
    
}
