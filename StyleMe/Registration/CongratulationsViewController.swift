//
//  CongratulationsViewController.swift
//  StyleMe
//
//  Created by EL INFO on 09/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class CongratulationsViewController: UIViewController {

    var headerText:String!
    var textDesc:String!
    @IBOutlet weak var buttonLetsGo: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        controller.hideButtonView(strMessage: textDesc, strMessageHeading:headerText)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        buttonLetsGo.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        
    }
    @IBAction func buttonLetsGoPressed(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
