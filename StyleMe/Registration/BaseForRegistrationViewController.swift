//
//  BaseForRegistrationViewController.swift
//  StyleMe
//
//  Created by Admin on 22/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class BaseForRegistrationViewController: UIViewController,SignInViewDelegate {
    
    @IBOutlet weak var basViewForButtons: UIView!
    @IBOutlet weak var baseViewForLogin: UIView!
    @IBOutlet weak var buttonSignUp: UIButton!
    @IBOutlet weak var buttonSignIn: UIButton!
    @IBOutlet weak var labelMessageHeading: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerForChildView: UIView!
    @IBOutlet weak var heightConstraintBaseView: NSLayoutConstraint!
    @IBOutlet weak var topConstraints:NSLayoutConstraint!
    var gradientLayer: CAGradientLayer!
    var navigation:UINavigationController!
    var heightBaseViewLogin:CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.heightConstraintBaseView.priority = .defaultLow
        if !AppDelegate.getAppDelegate().isCountryList{
            self.navigationController?.isNavigationBarHidden = true
            buttonSignUp.backgroundColor = UIColor.clear
            buttonSignUp.setTitleColor(UIColor.init(red: 243.0/255.0, green: 167.0/255.0, blue: 172.0/255.0, alpha: 1.0), for: .normal)
            buttonSignIn.isUserInteractionEnabled = false
            buttonSignUp.isUserInteractionEnabled = true
            if #available(iOS 13.0, *) {
                UIApplication.shared.statusBarUIView?.backgroundColor = UIColor.init(red: 255.0/255.0, green: 44.0/255.0, blue: 78.0/255.0, alpha: 1.0)
            }
            else{
                UIApplication.shared.statusBarUIView?.backgroundColor = UIColor.init(red: 255.0/255.0, green: 44.0/255.0, blue: 78.0/255.0, alpha: 1.0)
            }
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if !AppDelegate.getAppDelegate().isCountryList{
            let signInVC = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.MainStoryBoard, bundle: nil).instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.LoginTableViewController) as! LoginTableViewController
            signInVC.heightforCell = baseViewForLogin.frame.size.height
            navigation = UINavigationController.init(rootViewController:signInVC)
            navigation.isNavigationBarHidden = true
            navigation.view.frame = baseViewForLogin.bounds
            baseViewForLogin.addSubview(navigation.view)
            self.addChild(navigation)
            heightBaseViewLogin = baseViewForLogin.frame.size.height
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if !AppDelegate.getAppDelegate().isCountryList{
            if baseViewForLogin.frame.height != self.view.frame.height{
                baseViewForLogin.roundCorners([.topLeft, .topRight], radius: 10)
                containerForChildView.roundCorners([.topLeft, .topRight], radius: 10.0)
            }
            basViewForButtons.layer.cornerRadius = basViewForButtons.frame.size.height/2
            basViewForButtons.clipsToBounds = true
            buttonSignIn.layer.cornerRadius = buttonSignIn.frame.size.height/2
            buttonSignIn.clipsToBounds = true
        }
        
    }
    @IBAction func buttonSignUpPressed(_ sender: Any) {
        buttonSignUp.layer.cornerRadius = buttonSignUp.frame.size.height/2
        buttonSignUp.clipsToBounds = true
        buttonSignUp.backgroundColor = UIColor.white
        buttonSignUp.setTitleColor(UIColor.red, for: .normal)
        
        buttonSignIn.backgroundColor = UIColor.clear
        buttonSignIn.setTitleColor(UIColor.init(red: 243.0/255.0, green: 167.0/255.0, blue: 172.0/255.0, alpha: 1.0), for: .normal)
        
        buttonSignIn.isUserInteractionEnabled = true
        buttonSignUp.isUserInteractionEnabled = false
        let signUpTVC:SignUpTableViewController = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.MainStoryBoard, bundle: nil).instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.SignUpTableViewController) as! SignUpTableViewController
        signUpTVC.delegate = self
        signUpTVC.isSocialSignUp = false
        if navigation.topViewController == signUpTVC{
            return
        }
        else{
            navigation.pushViewController(signUpTVC, animated: false)
        }
    }
    @IBAction func buttonSignInPressed() {
        buttonSignIn.layer.cornerRadius = buttonSignIn.frame.size.height/2
        buttonSignIn.clipsToBounds = true
        buttonSignIn.backgroundColor = UIColor.white
        buttonSignIn.setTitleColor(UIColor.red, for: .normal)
        
        buttonSignUp.backgroundColor = UIColor.clear
        buttonSignUp.setTitleColor(UIColor.init(red: 243.0/255.0, green: 167.0/255.0, blue: 172.0/255.0, alpha: 1.0), for: .normal)
        
        buttonSignIn.isUserInteractionEnabled = false
        buttonSignUp.isUserInteractionEnabled = true
        
        self.navigation.popViewController(animated: false)
    }
    
    func SocialSignUp(socialDict:[String:Any]){
        buttonSignUp.layer.cornerRadius = buttonSignUp.frame.size.height/2
        buttonSignUp.clipsToBounds = true
        buttonSignUp.backgroundColor = UIColor.white
        buttonSignUp.setTitleColor(UIColor.red, for: .normal)
        
        buttonSignIn.backgroundColor = UIColor.clear
        buttonSignIn.setTitleColor(UIColor.init(red: 243.0/255.0, green: 167.0/255.0, blue: 172.0/255.0, alpha: 1.0), for: .normal)
        
        buttonSignIn.isUserInteractionEnabled = true
        buttonSignUp.isUserInteractionEnabled = false
        let signUpTVC:SignUpTableViewController = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.MainStoryBoard, bundle: nil).instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.SignUpTableViewController) as! SignUpTableViewController
        signUpTVC.delegate = self
        signUpTVC.isSocialSignUp = true
        signUpTVC.socialDict = socialDict
        if navigation.topViewController == signUpTVC{
            return
        }
        else{
            navigation.pushViewController(signUpTVC, animated: false)
        }
    }
    func callSignin() {
        self.buttonSignInPressed()
    }
    func fullViewAnimation(controller:UIViewController){
        self.topConstraints.priority = UILayoutPriority.defaultLow
        self.heightConstraintBaseView.priority = UILayoutPriority.defaultHigh
        self.heightConstraintBaseView.constant = 0
        //self.heightConstraintBaseView.constant = self.view.frame.height - self.baseViewForLogin.frame.size.height
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
            //           self.baseViewForLogin.frame = CGRect(x: 0.0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.baseViewForLogin.backgroundColor = .white
            
        }) { (completed) in
            self.baseViewForLogin.roundCorners([.topLeft, .topRight], radius: 0)
            
        }
    }
    
    func animateToOriginal(controller:UIViewController){
        containerView.backgroundColor = UIColor.init(red: 255.0/255.0, green: 44.0/255.0, blue: 78.0/255.0, alpha: 1.0)
        self.topConstraints.priority = UILayoutPriority.defaultHigh
        self.heightConstraintBaseView.priority = .defaultLow
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
            // self.heightConstraintBaseView.constant = self.heightBaseViewLogin
            //self.baseViewForLogin.frame = CGRect(x: 0.0, y: self.containerView.frame.size.height  - self.heightBaseViewLogin , width: UIScreen.main.bounds.size.width, height: self.heightBaseViewLogin)
        }) { (completed) in
            self.baseViewForLogin.roundCorners([.topLeft, .topRight], radius: 10)
        }
    }
    
    func hideButtonView(strMessage:String, strMessageHeading:String){
        let strHeading = strMessageHeading + "\n" + strMessage
        let myString:NSMutableAttributedString = NSMutableAttributedString.init(string: strHeading)
        let range = NSRange.init(location: strMessageHeading.count, length: strHeading.count - strMessageHeading.count)
        
        let modelName = UIDevice.modelName
        if modelName == "iPhone 8 Plus" || modelName == "iPhone XS Max" || modelName == "iPhone XR" || modelName == "iPhone 7 Plus"{
            myString.addAttribute(NSAttributedString.Key.font, value: UIFont.PoppinsLight(ofSize: 15), range: range)
        }
        else{
            myString.addAttribute(NSAttributedString.Key.font, value: UIFont.PoppinsLight(ofSize: 13), range: range)
        }
        basViewForButtons.isHidden = true
        labelMessage.isHidden = false
        //labelMessage.text = strMessage
        labelMessageHeading.attributedText = myString
    }
    
    func showButtonView(){
        basViewForButtons.isHidden = false
        labelMessage.isHidden = true
        labelMessageHeading.text = Constants.StaticTextHeaders.headerTextLogin
    }
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.view.bounds
        
        gradientLayer.colors = [UIColor.red.cgColor, UIColor.yellow.cgColor]
        
        containerView.layer.addSublayer(gradientLayer)
    }
}

