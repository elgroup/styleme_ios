
//
//  OTPVerificationTableViewController.swift
//  StyleMe
//
//  Created by Admin on 26/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
protocol VerificationStatusDelegate {
    func verificationStatus(status:Bool)
}
class OTPVerificationTableViewController: UITableViewController,UITextFieldDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var Texfield1 :UITextField!
    @IBOutlet weak var Texfield2 :UITextField!
    @IBOutlet weak var Texfield3 :UITextField!
    @IBOutlet weak var Texfield4 :UITextField!
    @IBOutlet weak var lowerView1 :UIView!
    @IBOutlet weak var lowerView2 :UIView!
    @IBOutlet weak var lowerView3 :UIView!
    @IBOutlet weak var lowerView4 :UIView!
    @IBOutlet weak var View1 :UIView!
    @IBOutlet weak var View2 :UIView!
    @IBOutlet weak var View3 :UIView!
    @IBOutlet weak var View4 :UIView!
    @IBOutlet weak var buttonResend: UIButton!
    @IBOutlet weak var buttonChangeNumber: UIButton!
    @IBOutlet weak var labelInstructions: UILabel!
    @IBOutlet weak var buttonSignIn: UIButton!
    @IBOutlet weak var viewMutipart: MultiPartTextField!
    @IBOutlet weak var viewTouch: UIView!
    var phoneNumber:String!
    var countryCode:String!
    var buttonTitle:String!
    var intOTP:String!
    var delegate:VerificationStatusDelegate?
    var isForgotPassword:Bool!
    var tfArr :[AnyObject] = []
    var viewArr :[AnyObject] = []
    var  otpText:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         tfArr = [Texfield1,Texfield2,Texfield3,Texfield4]
         viewArr = [View1,View2,View3,View4]
         setTextfieldBoarder()
        Texfield1.becomeFirstResponder()
        lowerView1.backgroundColor = UIColor.red
        let tap =  UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_ :)))
               tap.delegate = self
               view.addGestureRecognizer(tap)
               view.isUserInteractionEnabled = true
        let attributeButtonChangeNumber: [NSAttributedString.Key: Any] = [
            .font: UIFont.init(name: "Poppins-Regular", size: 13) as Any,
            .foregroundColor: UIColor.darkGray,
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributeButtonResend:[NSAttributedString.Key:Any] = [.font: UIFont.init(name: "Poppins-Regular", size: 13) as Any,             .foregroundColor: UIColor.darkGray, .underlineStyle: NSUnderlineStyle.single.rawValue]
        if isForgotPassword{
            let attributeString = NSMutableAttributedString(string: "Use different Mobile Number or Email",attributes: attributeButtonChangeNumber)
            self.buttonChangeNumber.setAttributedTitle(attributeString, for: .normal)
            self.labelInstructions.text = "Verification Code has been sent to your Number or Email"
        }
        else{
            let attributeString = NSMutableAttributedString(string: "Use different Mobile Number",attributes: attributeButtonChangeNumber)
            self.buttonChangeNumber.setAttributedTitle(attributeString, for: .normal)
            self.labelInstructions.text = "Verification Code has been sent to your Number"
        }
        let attributedStringResendButton = NSMutableAttributedString(string: "Resend verification code", attributes: attributeButtonResend)
        self.buttonResend.setAttributedTitle(attributedStringResendButton, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        controller.hideButtonView(strMessage: Constants.StaticTextHeaders.textVerification, strMessageHeading: Constants.StaticTextHeaders.headerTextOtpVerification)
        self.apiRequestForOtp()
        buttonSignIn.setTitle(buttonTitle, for: .normal)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        buttonSignIn.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)

    }
    @objc func handleTap (_ sender:UITapGestureRecognizer){
           Texfield1.resignFirstResponder()
           Texfield2.resignFirstResponder()
           Texfield3.resignFirstResponder()
           Texfield4.resignFirstResponder()
        
          }
    func setTextfieldBoarder(){
              for tf in viewArr{
                  if #available(iOS 13.0, *) {
                    tf.layer.borderColor = UIColor.black.cgColor
                    tf.layer.borderWidth = 2
                  } else {
                      // Fallback on earlier versions
                  }
              }
              
          }
    // ************************************* TEXT FIELD DELEGATE ************************
          @objc func textFieldDidChange(textField: UITextField){
              let text = textField.text
              if  text?.count == 1 {
                  switch textField{
                  case Texfield1:
                      Texfield2.becomeFirstResponder()
                  
                  case Texfield2:
                      Texfield3.becomeFirstResponder()
                   
                  case Texfield3:
                      Texfield4.becomeFirstResponder()
                    
                  case Texfield4:
                      Texfield4.resignFirstResponder()
             
                      
                 // requestForVerification()
                                                      
                  default:
                      break
                  }
              }
              if  text?.count == 0 {
                  switch textField{
                  case Texfield1:
                      Texfield1.becomeFirstResponder()
                  case Texfield2:
                      Texfield1.becomeFirstResponder()
                  case Texfield3:
                      Texfield2.becomeFirstResponder()
                  case Texfield4:
                      Texfield3.becomeFirstResponder()
                  
                  default:
                      break
                  }
              }
              else{
                  
              }
          }
          func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
              Texfield1.resignFirstResponder()
              Texfield2.resignFirstResponder()
              Texfield3.resignFirstResponder()
              Texfield4.resignFirstResponder()
              return true
          }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // On inputing value to textfield
        if ((textField.text?.count)! < 1  && string.count > 0){
            if(textField == Texfield1){
                Texfield2.becomeFirstResponder()
                lowerView2.backgroundColor = UIColor.red
                lowerView3.backgroundColor = UIColor.clear
                lowerView4.backgroundColor = UIColor.clear
                lowerView1.backgroundColor = UIColor.clear
                
            }
            if(textField == Texfield2){
                Texfield3.becomeFirstResponder()
                lowerView3.backgroundColor = UIColor.red
                lowerView2.backgroundColor = UIColor.clear
                lowerView4.backgroundColor = UIColor.clear
                lowerView1.backgroundColor = UIColor.clear
            }
            if(textField == Texfield3){
                Texfield4.becomeFirstResponder()
                lowerView4.backgroundColor = UIColor.red
                lowerView2.backgroundColor = UIColor.clear
                lowerView1.backgroundColor = UIColor.clear
                lowerView3.backgroundColor = UIColor.clear
            }
            
            textField.text = string
            return false
            
        }else if ((textField.text?.count)! >= 1  && string.count == 0){
            // on deleting value from Textfield
            if(textField == Texfield2){
                Texfield1.becomeFirstResponder()
                lowerView2.backgroundColor = UIColor.clear
                lowerView3.backgroundColor = UIColor.clear
                lowerView4.backgroundColor = UIColor.clear
                lowerView1.backgroundColor = UIColor.red
            }
            if(textField == Texfield3){
                Texfield2.becomeFirstResponder()
                lowerView2.backgroundColor = UIColor.red
                lowerView3.backgroundColor = UIColor.clear
                lowerView4.backgroundColor = UIColor.clear
                lowerView1.backgroundColor = UIColor.clear
            }
            if(textField == Texfield4) {
                Texfield3.becomeFirstResponder()
                lowerView3.backgroundColor = UIColor.red
                lowerView2.backgroundColor = UIColor.clear
                lowerView4.backgroundColor = UIColor.clear
                lowerView1.backgroundColor = UIColor.clear
            }
            
            textField.text = ""
            return false
        }else if ((textField.text?.count)! >= 1  ){
            textField.text = string
            return false
        }
        
        return true
    }
    func apiRequestForOtp(){
        
        let isReachable:Bool = Reachability.isConnectedToNetwork()
        if isReachable{
            var dictRequest:[String:Any] = [String:Any]()
            var dict:[String:Any] = [String:Any]()
            var url:String = String()
            if isForgotPassword{
                dict["mobile_number"] = phoneNumber
                url = Constants.URLConstants.forgotPassword
            }
            else{
                dict["mobile_number"] = phoneNumber
                dict["mobile_code"] = countryCode
                url = Constants.URLConstants.otpVerification
            }
            dictRequest["user"] = dict
            
            
            APIManager.postRequestForURLString(true, url, [:],dictRequest) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async{
                    if Success{
                        let dict:[String:Any] = result as! [String:Any]
                        self.intOTP = dict["otp"] as? String
                    }
                    else {
                        let dict:[String:Any] = result as! [String:Any]
                        AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message: dict["message"] as! String, Controller: self, Time: 2)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message:Constants.AppConstants.alertMessageCheckInternet , Controller: self, Time: 2)
        }
    }
    @IBAction func changePhoneNumberPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resendVerificationCodePressed(_ sender: Any) {
        self.apiRequestForOtp()
    }
//    @IBAction func tappedMultipartTextfieldView(_ sender: Any) {
//        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
//        controller.fullViewAnimation(controller: self)
//        viewTouch.isHidden  = true
//        for view in self.viewMutipart.subviews {
//            if view .isKind(of: UITextField.self){
//                let textFld = view as! UITextField
//                textFld.delegate = self
//                if textFld.tag == 0{
//                    textFld.delegate = self
//                    textFld.becomeFirstResponder()
//                    break
//                }
//            }
//        }
//    }
    
    @IBAction func signInButtonPressed(_ sender: Any) {
        //        let code:String = viewMutipart.currentValue
        //        if code.count < 4{
        //            AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message:Constants.OtpVerificatonConstants.messageValidCode , Controller: self, Time: 2)
        //        }
       // var  otpText:String!
        let otpVeriftStr = Texfield1.text! + Texfield2.text! + Texfield3.text! + Texfield4.text!
        otpText = (otpVeriftStr.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
        if otpText.count < 4 {
            DispatchQueue.main.async {
                AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message:Constants.OtpVerificatonConstants.messageValidCode , Controller: self, Time: 2)
            }
            return
        }
        if intOTP == otpText{
            self.view.endEditing(true)
            if isForgotPassword{
                let reset:ResetPasswordViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.ResetPasswordTableViewController) as! ResetPasswordViewController
                reset.intOtp = intOTP
                self.navigationController?.pushViewController(reset, animated: true)
            }
            else{
                AppCustomClass.showCustomAlert(alertTitle: Constants.apiConstants.alertSuccess, Message: Constants.OtpVerificatonConstants.messageSuccesVerification, buttonTitle: "OK", Controller: self) { (success) in
                    if success{
                        self.delegate?.verificationStatus(status: true)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else{
                        self.delegate?.verificationStatus(status: false)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
        else{
           DispatchQueue.main.async {
                AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message:Constants.OtpVerificatonConstants.messageWrongOTP , Controller: self, Time: 2)
            }
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        if indexPath.row == 1{
        }
        else{
            controller.animateToOriginal(controller: self)
            self.view.endEditing(true)
            viewTouch.isHidden = false
        }
    }
}
