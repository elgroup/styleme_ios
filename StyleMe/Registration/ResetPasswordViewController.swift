//
//  ReserPasswordViewController.swift
//  StyleMe
//
//  Created by EL INFO on 19/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UITableViewController,UITextFieldDelegate {
    
    @IBOutlet weak var textfieldConfirmPassword: ACFloatingTextfield!
    @IBOutlet weak var textfieldPassword: ACFloatingTextfield!
    @IBOutlet weak var viewBasePassword: UIView!
    @IBOutlet weak var viewBaseConfirmPassword: UIView!
    @IBOutlet weak var buttonResetPassword: UIButton!
    var intOtp:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        controller.hideButtonView(strMessage: Constants.StaticTextHeaders.textResetPassword, strMessageHeading:Constants.StaticTextHeaders.headerResetPassword)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        viewBaseConfirmPassword.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        viewBasePassword.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        buttonResetPassword.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
    }
        
    
    @IBAction func ResetButtonPressed(_ sender: Any){
        if textfieldPassword.text!.count == 0{
            AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message:Constants.ResetPasswordConstant.alertNoPassword , Controller: self, Time: 2)
            return
        }
        else if textfieldPassword.text!.count < 6 || textfieldPassword.text!.count > 16{
            AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message: Constants.ResetPasswordConstant.alertPasswordLength, Controller: self, Time: 3)
            return
        }
        else if textfieldConfirmPassword.text!.count == 0{
            AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message: Constants.ResetPasswordConstant.alertNoConfirmPassword, Controller: self, Time: 2)
            return
        }
        else if textfieldConfirmPassword.text != textfieldPassword.text{
            AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message: Constants.ResetPasswordConstant.alertMatchPasswordAndConfirmPassword, Controller: self, Time: 2)
            return
        }
        else{
            self.apiRequestForResetPassword()
        }
    }
    
    func apiRequestForResetPassword(){
        let isReachable:Bool = Reachability.isConnectedToNetwork()
        if isReachable{
            var dictRequest:[String:Any] = [String:Any]()
            dictRequest["password"] = textfieldPassword.text
            dictRequest["otp"] = intOtp
            var request:[String:Any] = [String:Any]()
            request["user"] = dictRequest
            var requestHeader:[String:String] = [String:String]()
            requestHeader["LOCALE"] = "en"
            APIManager.putRequestForURLString(true, Constants.URLConstants.forgotPassword, requestHeader, request) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async{
                    if Success{
                        let story = UIStoryboard.init(name: "Main", bundle: nil)
                        let Controller:CongratulationsViewController = story.instantiateViewController(withIdentifier: "CongratulationsViewController") as! CongratulationsViewController
                        Controller.headerText = "Congratulations"
                        Controller.textDesc = "Your Password has been reset. Just press the Let's Go button to Sign In."
                        self.navigationController?.pushViewController(Controller, animated: true)
                    }
                    else {
                        let dict:[String:Any] = result as! [String:Any]
                        AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message: dict["message"] as! String, Controller: self, Time: 2)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message: Constants.AppConstants.alertMessageCheckInternet, Controller: self, Time: 2)
        }
    }
}

extension ResetPasswordViewController{
    //Table View Delegate
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        controller.animateToOriginal(controller: self)
        self.view.endEditing(true)
    }
    
    //Textfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        controller.fullViewAnimation(controller: self)
        textField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
