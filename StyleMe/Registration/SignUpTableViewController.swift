//
//  SignUpTableViewController.swift
//  StyleMe
//
//  Created by Admin on 22/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

protocol SignInViewDelegate{
    func callSignin()
}
class SignUpTableViewController: UITableViewController,UITextFieldDelegate,CountryListDelegate,VerificationStatusDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    var delegate:SignInViewDelegate?
    @IBOutlet weak var textFieldName: ACFloatingTextfield!
    @IBOutlet weak var textFieldEmail: ACFloatingTextfield!
    @IBOutlet weak var textFieldPhone: ACFloatingTextfield!
    @IBOutlet weak var textFieldPassword: ACFloatingTextfield!
    @IBOutlet weak var buttonSignUp: UIButton!
    @IBOutlet weak var viewBaseName: UIView!
    @IBOutlet weak var viewBaseEmail: UIView!
    @IBOutlet weak var viewBasePhone: UIView!
    @IBOutlet weak var viewBasePassword: UIView!
    @IBOutlet weak var countryCodeTextfield: UITextField!
    @IBOutlet weak var textFieldConfirmPassword: ACFloatingTextfield!
    @IBOutlet weak var textFieldCountry: ACFloatingTextfield!
    @IBOutlet weak var viewBaseCountry: UIView!
    @IBOutlet weak var viewBaseConfirmPassword: UIView!
    
    var selectedTextField:UITextField!
    var countryList = CountryList()
    var countryCode : String = String()
    var countryFlagName = String()
    var phoneNumberCountryCode: String = String()
    var isCountryList:Bool!
    var buttonVerify:UIButton!
    var isPhoneVerified = false
    var dataResponse:[String:Any] = [String:Any]()
    var arrCountry:[Any]!
    var countryID:Int!
    var isSocialSignUp:Bool!
    var socialDict:[String:Any]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.rightButtonInMobileTextField()
        self.rightButtonInCountryCodeTextField()
        countryList.delegate = self
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            countryFlagName = (countryCode as NSString).lowercased
            let arr = countryList.countryList
            if let i = arr.firstIndex(where: { $0.countryCode == countryCode }) {
                let dict = arr[i] as Country
                countryCodeTextfield.text = String(format:"+%@",dict.phoneExtension)
                phoneNumberCountryCode = "+" + dict.phoneExtension
            }
        }
        else{
            countryFlagName = "us"
            countryCodeTextfield.text = "+1"
            phoneNumberCountryCode = "+1"
        }
        
        if let path = Bundle.main.path(forResource: "Country", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let country = jsonResult["country"] as? [Any] {
                    if country.count > 0{
                        arrCountry = [Any]()
                        arrCountry = country
                    }
                    // do stuff
                }
            } catch {
                // handle error
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        controller.showButtonView()
        self.textFieldCountry.inputAccessoryView = self.addToolBar()
        self.textFieldPhone.inputAccessoryView = self.addToolBar()
        self.textFieldCountry.inputView = self.addPickerView()
        if isSocialSignUp{
            textFieldName.text = socialDict["user_name"] as? String
            textFieldEmail.text = socialDict["email"] as? String
            textFieldPhone.becomeFirstResponder()
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        viewBaseName.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        viewBasePassword.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        buttonSignUp.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        viewBaseEmail.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        viewBasePhone.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        viewBaseCountry.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        viewBaseConfirmPassword.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
    }
    
    func rightButtonInMobileTextField(){
        buttonVerify = UIButton(type: .custom)
        buttonVerify.frame = CGRect.init(x: textFieldPhone.frame.size.width - 60, y: 0, width: 60, height: textFieldPhone.frame.size.height)
        buttonVerify.setTitle("Verify", for: .normal)
        buttonVerify.titleLabel?.font = UIFont.PoppinsRegular(ofSize: 14)
        buttonVerify.setTitleColor(.gray, for: .normal)
        buttonVerify.addTarget(self, action: #selector(SignUpTableViewController.verifyButtonPressed), for: .touchUpInside)
        textFieldPhone.rightView = buttonVerify
        textFieldPhone.rightViewMode = .always
    }
    func rightButtonInCountryCodeTextField(){
        let button = UIButton(type: .custom)
        button.frame = CGRect.init(x: countryCodeTextfield.frame.size.width - 5, y: 0, width: 10, height:10)
        button.setImage(UIImage.init(named: "drop-down-arrow"), for: .normal)
        countryCodeTextfield.rightView = button
        countryCodeTextfield.rightViewMode = .always
        
    }
    @objc func verifyButtonPressed(){
        if textFieldPhone.text?.count == 0 || textFieldPhone.text!.count < 8{
            AppCustomClass.showToastAlert(Title: "", Message: Constants.SignupConstants.inputMobileAlertText, Controller: self, Time: 2)
            return
        }
        else{
            let otpVC:OTPVerificationTableViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.OTPVerificationTableViewController) as! OTPVerificationTableViewController
            otpVC.countryCode = phoneNumberCountryCode
            otpVC.phoneNumber = textFieldPhone.text
            otpVC.buttonTitle = Constants.SignupConstants.buttonVerifyDefaultTitle
            otpVC.delegate = self
            otpVC.isForgotPassword = false
            self.navigationController?.pushViewController(otpVC, animated: true)
        }
    }
    
    func verificationStatus(status: Bool) {
        if status{
            isPhoneVerified = status
            buttonVerify.setTitle(Constants.SignupConstants.buttonVerifyTitleVerified, for: .normal)
        }
        else{
            isPhoneVerified = status
            buttonVerify.setTitle(Constants.SignupConstants.buttonVerifyDefaultTitle, for: .normal)
        }
    }
    
    func addToolBar()->UIToolbar{
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelNumberPad)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneWithNumberPad))]
        numberToolbar.sizeToFit()
        return numberToolbar
    }
    
    @objc func cancelNumberPad() {
        //Cancel with number pad
        self.view.endEditing(true)
    }
    
    @objc func doneWithNumberPad() {
        if selectedTextField == textFieldCountry as UITextField?{
            if countryID == nil{
                let dict:[String:Any] = arrCountry[0] as! [String:Any]
                let countryId:Int = dict["id"] as! Int
                countryID = countryId
            }
            else{
                selectedTextField.resignFirstResponder()
                
            }
        }
        else{
            selectedTextField.resignFirstResponder()
        }
    }
    
    func addPickerView()->UIPickerView{
        let picker: UIPickerView
        picker = UIPickerView.init()
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        return picker
    }
    
    
    
    
    @IBAction func signUpButtonPressed(){
        
        if textFieldName.text?.count == 0 {
            AppCustomClass.showToastAlert(Title: "", Message:Constants.SignupConstants.inputNameAlertText , Controller: self, Time: 2)
            return
        }
        else if textFieldEmail.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: Constants.SignupConstants.inputEmailAlertText, Controller: self, Time: 2)
            
            return
        }
        else if textFieldPhone.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message:Constants.SignupConstants.inputMobileAlertText , Controller: self, Time: 2)
            return
        }
        else if !isPhoneVerified{
            AppCustomClass.showToastAlert(Title: "", Message:Constants.SignupConstants.validateMobileAlertText, Controller: self, Time: 2)
            return
        }
        else if textFieldCountry.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message:Constants.SignupConstants.inputCountryAlertText , Controller: self, Time: 2)
            return
        }
        else if textFieldPassword.text?.count == 0 && !isSocialSignUp{
            AppCustomClass.showToastAlert(Title: "", Message:Constants.SignupConstants.inputPasswordAlertText , Controller: self, Time: 2)
            return
        }
        else if textFieldConfirmPassword.text?.count == 0 && !isSocialSignUp{
            AppCustomClass.showToastAlert(Title: "", Message:Constants.SignupConstants.inputConfirmPasswordAlertText , Controller: self, Time: 2)
            return
        }
        else if textFieldConfirmPassword.text != textFieldPassword.text && !isSocialSignUp{
            AppCustomClass.showToastAlert(Title: "", Message: Constants.SignupConstants.validatePasswordandConfirmPassword, Controller: self, Time: 2)
            return
        }
        else{
            self.dataRequestForSignUp()
        }
    }
    
    func dataRequestForSignUp(){
        let isReachable:Bool = Reachability.isConnectedToNetwork()
        if isReachable{
            var dictRequest:[String:Any] = [String:Any]()
            dictRequest["user_name"] = textFieldName.text
            dictRequest["email"] = textFieldEmail.text
            dictRequest["mobile_number"] = textFieldPhone.text
            dictRequest["password"] = textFieldPassword.text
            dictRequest["mobile_code"] = phoneNumberCountryCode
            dictRequest["country_id"] = countryID
            var request:[String:Any] = [String:Any]()
            request["user"] = dictRequest
            APIManager.postRequestForURLString(true, Constants.URLConstants.signup, [:], request) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async{
                    if Success{
                    
                        AppCustomClass.showCustomAlert(alertTitle: Constants.apiConstants.alertSuccess, Message: Constants.SignupConstants.successSignup, buttonTitle: "OK", Controller: self, completionForOk: { (success) in
                            if success{
                                self.delegate?.callSignin()
                            }
                        })
                    }
                    else {
                        let dict:[String:Any] = result as! [String:Any]
                        AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message: dict["message"] as! String, Controller: self, Time: 2)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message:Constants.AppConstants.alertMessageCheckInternet , Controller: self, Time: 2)
        }
    }
    
    
    func dataRequestForSocialSignUp(){
        let isReachable:Bool = Reachability.isConnectedToNetwork()
        if isReachable{
            var dictRequest:[String:Any] = [String:Any]()
            dictRequest["user_name"] = textFieldName.text
            dictRequest["email"] = textFieldEmail.text
            dictRequest["mobile_number"] = textFieldPhone.text
            dictRequest["country_id"] = countryID
            dictRequest["uid"] = socialDict["uid"]
            dictRequest["provider"] = socialDict["provider"]
            dictRequest["device_id"] = socialDict["device_id"]
            dictRequest["remote_profile_image_url"] = socialDict["remote_image_url"]
            dictRequest["device_type"] = Constants.apiConstants.deviceType
            var request:[String:Any] = [String:Any]()
            request["user"] = dictRequest
            APIManager.postRequestForURLString(true, Constants.URLConstants.signup, [:], request) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async{
                    if Success{
                        let story = UIStoryboard.init(name: "Salon", bundle: nil)
                        let tabBarController:SalonTabBarController = story.instantiateViewController(withIdentifier: "SalonTabBarController") as! SalonTabBarController
                        AppDelegate.getAppDelegate().window!.rootViewController = tabBarController
                        AppDelegate.getAppDelegate().window?.makeKeyAndVisible()
                    }
                    else {
                        let dict:[String:Any] = result as! [String:Any]
                        AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message: dict["message"] as! String, Controller: self, Time: 2)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message:Constants.AppConstants.alertMessageCheckInternet , Controller: self, Time: 2)
        }
    }
    func selectedCountry(dict: NSDictionary) {
        isCountryList = true
        countryFlagName = (dict["countryCode"] as! NSString).lowercased
        countryCode = dict["phoneExtension"]  as! String
        countryCodeTextfield.text = "+\(countryCode)"
        phoneNumberCountryCode = countryCode
    }
    
    func didCancel(){
        self.dismiss(animated: true, completion: nil)
        isCountryList = true
    }
}


extension SignUpTableViewController{
    // MARK: - Table view data source
    //    override func numberOfSections(in tableView: UITableView) -> Int {
    //        return 1
    //    }
    //
    //    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        return 8
    //    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isSocialSignUp{
            if indexPath.row == 6 || indexPath.row == 5{
                return 0
            }
            else{
                return UITableView.automaticDimension
            }
        }
        else{
            return UITableView.automaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        controller.animateToOriginal(controller: self)
        //        delegate?.animateToOriginal
        self.view.endEditing(true)
    }
    
    //MARK: Table view delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
        if textField == countryCodeTextfield {
        }
        else{
            let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
            controller.fullViewAnimation(controller: self)
            textField.becomeFirstResponder()
        }
    }
    //MARK: Textfield Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == countryCodeTextfield{
            let navController = UINavigationController(rootViewController: countryList)
            self.present(navController, animated: true, completion: nil)
            AppDelegate.getAppDelegate().isCountryList = true
            return false
        }
        else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: UIPickerView Delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCountry.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dict:[String:Any] = arrCountry[row] as! [String:Any]
        let countryName:String = dict["name"] as! String
        textFieldCountry.text = countryName
        return countryName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let dict:[String:Any] = arrCountry[row] as! [String:Any]
        let countryId:Int = dict["id"] as! Int
        countryID = countryId
    }
}

