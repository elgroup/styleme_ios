//
//  ForgotPasswordTableViewController.swift
//  StyleMe
//
//  Created by Admin on 26/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
//protocol AnimateForgotPasswordViewDelegate {
//    func fullViewAnimation()
//    func animateToOriginal()
//}
class ForgotPasswordTableViewController: UITableViewController,UITextFieldDelegate {
    
    @IBOutlet weak var buttonSend: UIButton!
    @IBOutlet weak var baseViewTextField: UIView!
    @IBOutlet weak var textFieldMobileNumber: ACFloatingTextfield!
    //var delegate:AnimateLoginViewDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        controller.hideButtonView(strMessage: Constants.StaticTextHeaders.textForgotPass, strMessageHeading:Constants.StaticTextHeaders.headerTextForgotPassword)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if (self.tableView.contentSize.height < self.tableView.frame.size.height) {
            self.tableView.isScrollEnabled = false
        }
        else {
            self.tableView.isScrollEnabled = true
        }
        
        baseViewTextField.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        buttonSend.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
    }
    @IBAction func buttonSignInPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSendOTPPressed(_ sender: Any) {
        if textFieldMobileNumber.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: "Please enter registered Email or Mobile Number", Controller: self, Time: 2)
        }
        else{
            self.view.endEditing(true)
            let forgotPasswordTVC:OTPVerificationTableViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OTPVerificationTableViewController") as! OTPVerificationTableViewController
            forgotPasswordTVC.phoneNumber = textFieldMobileNumber.text
            forgotPasswordTVC.isForgotPassword = true
            forgotPasswordTVC.buttonTitle = "Verify"
            self.navigationController?.pushViewController(forgotPasswordTVC, animated: true)
        }
    }
    
    // MARK: TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        controller.fullViewAnimation(controller: self)
        textField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        controller.animateToOriginal(controller: self)
        self.view.endEditing(true)
    }
}
