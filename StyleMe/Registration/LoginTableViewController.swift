//
//  LoginTableViewController.swift
//  StyleMe
//
//  Created by Admin on 22/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import GoogleSignIn
import UserNotifications
import FBSDKCoreKit
import FBSDKLoginKit
protocol SignUpViewDelegate{
    func callSignUp()
}
class LoginTableViewController: UITableViewController {
    @IBOutlet weak var textFieldUserName: ACFloatingTextfield!
    @IBOutlet weak var textFieldPassword: ACFloatingTextfield!
    @IBOutlet weak var buttonForgotPassword: UIButton!
    @IBOutlet weak var buttonSignIn: UIButton!
    @IBOutlet weak var buttonFaceBook: UIButton!
    @IBOutlet weak var buttonGoogle: UIButton!
    @IBOutlet weak var viewBaseUserName: UIView!
    @IBOutlet weak var viewBasePassword: UIView!
    var delegate:SignUpViewDelegate?
    
    var heightforCell:CGFloat!
    var googleUser:GIDGoogleUser!
    //var delegate:AnimateLoginViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        var notificationCenter: UNUserNotificationCenter?
        //        notificationCenter = UNUserNotificationCenter.current()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        controller.showButtonView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if (self.tableView.contentSize.height < self.tableView.frame.size.height) {
            self.tableView.isScrollEnabled = false
        }
        else {
            self.tableView.isScrollEnabled = true
        }
        
        viewBaseUserName.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        viewBasePassword.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
        buttonSignIn.roundCorners([.topRight, .topLeft, .bottomLeft, .bottomRight], radius: 5.0)
    }
    @IBAction func buttonSignInPressed(_ sender: Any) {
        if textFieldUserName.text?.count == 0 {
            AppCustomClass.showToastAlert(Title: "" ,Message:Constants.LoginConstants.inputUsernameAlert , Controller: self, Time: 2)
            return
        }
        else if textFieldPassword.text?.count == 0{
            AppCustomClass.showToastAlert(Title: "", Message: Constants.LoginConstants.inputPasswordAlert, Controller: self, Time: 2)
        }
        else{
            self.apiRequestLogin()
        }
    }
    
    @IBAction func buttonForgotPasswordPressed(_ sender: Any) {
        self.view.endEditing(true)
        let forgotPasswordTVC:ForgotPasswordTableViewController = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.MainStoryBoard, bundle: nil).instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.ForgotPasswordTableViewController) as! ForgotPasswordTableViewController
        self.navigationController?.pushViewController(forgotPasswordTVC, animated: true)
    }
    @IBAction func buttonGoogleSignInPressed(_ sender: Any) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.signIn()
        GIDSignIn.sharedInstance()?.delegate = self
    }
    
    @IBAction func buttonFaceBookSignInPressed(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile","email"], from: self) { (loginResult, error) in
            if (error != nil){
                print(error as Any)
            }
            else if loginResult!.isCancelled{
                print("cancelled")
            }
            else{
                self.fetchUserProfile()
                print("success")
            }
        }
    }
    
    func fetchUserProfile()
    {
        let graphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"id, email, name, picture.width(480).height(480)"])
        
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            let dict:[String:Any] = result as! [String:Any]
            if ((error) != nil)
            {
                print(error as Any)
            }
            else
            {
                print(result as Any)
                var user:[String:Any] = [String:Any]()
                let id : String = dict["id"] as! String
                user["uid"] = id
                if let userName = dict["name"] as? String
                {
                    user["user_name"] = userName
                }
                
                if let email =  dict["email"] as? String{
                    user["email"] = email
                }
                
                if let profilePictureObj = dict["picture"] as? NSDictionary
                {
                    let data = profilePictureObj.value(forKey: "data") as! NSDictionary
                    let pictureUrlString  = data.value(forKey: "url") as! String
                    let pictureUrl = NSURL(string: pictureUrlString)
                    print(pictureUrl as Any)
                    user["remote_image_url"] = pictureUrlString
                }
                user["provider"] = "facebook"
                self.faceBookSignIn(user: user)
            }
        })
    }
    
    func apiRequestLogin(){
        let isReachable:Bool = Reachability.isConnectedToNetwork()
        if isReachable{
            var dictRequest:[String:Any] = [String:Any]()
            dictRequest["auth"] = textFieldUserName.text
            dictRequest["password"] = textFieldPassword.text
            dictRequest["device_type"] = Constants.apiConstants.deviceType
            dictRequest["device_id"] = UIDevice.current.identifierForVendor!.uuidString
            var request:[String:Any] = [String:Any]()
            request["user"] = dictRequest
            var requestHeader:[String:String] = [String:String]()
            requestHeader[Constants.apiConstants.locale] = "en"
            APIManager.postRequestForURLString(true, Constants.URLConstants.login, requestHeader, request) { (Done, error, Success, result, message, isActiveSession) in
                DispatchQueue.main.async{
                    if Success{
                        let dict:[String:Any] = result as! [String:Any]
                        let user:[String:Any] = dict["user"] as! [String:Any]
                        let defaults:UserDefaults = UserDefaults.standard
                        defaults.set(user[Constants.Login.userId], forKey: Constants.Login.userId)
                        defaults.set(user[Constants.Login.mobileNumber], forKey: Constants.Login.mobileNumber)
                        defaults.set(user[Constants.Login.mobileCode], forKey: Constants.Login.mobileCode)
                        defaults.set(user[Constants.Login.countryId], forKey: Constants.Login.countryId)
                        defaults.set(user[Constants.Login.userName], forKey: Constants.Login.userName)
                        defaults.set(dict[Constants.Login.authToken], forKey: Constants.Login.authToken)
                        defaults.set(true, forKey: Constants.Login.isLogin)
                        let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
                        let tabBarController:SalonTabBarController = story.instantiateViewController(withIdentifier:Constants.UIViewControllerIdentifier.SalonTabbarController ) as! SalonTabBarController
                        AppDelegate.getAppDelegate().window!.rootViewController = tabBarController
                        AppDelegate.getAppDelegate().window?.makeKeyAndVisible()
                    }
                    else {
                        let dict:[String:Any] = result as! [String:Any]
                        AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message: dict["message"] as! String, Controller: self, Time: 2)
                    }
                }
            }
        }
        else{
            AppCustomClass.showToastAlert(Title: Constants.AppConstants.alert, Message:Constants.AppConstants.alertMessageCheckInternet , Controller: self, Time: 2)
        }
    }
    
}

extension LoginTableViewController:UITextFieldDelegate{
    //MARK: Uitextfield delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
        controller.fullViewAnimation(controller: self)
        //        delegate?.fullViewAnimation()
        //        textField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension LoginTableViewController:GIDSignInDelegate{
    // MARK: GIDSignIn Delegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        googleUser = user
        print(googleUser.userID as Any)
        print(googleUser.authentication.idToken as Any)
        print(googleUser.profile.name as Any)
        print(googleUser.profile.hasImage as Any)
        print(googleUser.profile.imageURL(withDimension: 200) as Any)
        var dictGoogleUser:[String:Any] = [String:Any]()
        dictGoogleUser["user_name"] = googleUser.profile.name
        dictGoogleUser["email"] = googleUser.profile.email
        dictGoogleUser["provider"] = "google"
        dictGoogleUser["remote_image_url"] = googleUser.profile.imageURL(withDimension: 500)
        
        var dictRequest:[String:Any] = [String:Any]()
        dictRequest["uid"] = googleUser.userID
        dictRequest["device_type"] = Constants.apiConstants.deviceType
        dictRequest["device_id"] = UIDevice.current.identifierForVendor!.uuidString
        APIManager.postRequestForURLString(true, Constants.URLConstants.checkUser, [:], dictRequest) { (Done, error, Success, result, message, isActiveSession) in
            DispatchQueue.main.async{
                if Success{
                    
                    let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
                    let tabBarController:SalonTabBarController = story.instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.SalonTabbarController) as! SalonTabBarController
                    AppDelegate.getAppDelegate().window!.rootViewController = tabBarController
                    AppDelegate.getAppDelegate().window?.makeKeyAndVisible()
                }
                else {
                    let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
                    dictGoogleUser["uid"] = self.googleUser.userID
                    dictGoogleUser["device_type"] = Constants.apiConstants.deviceType
                    dictGoogleUser["device_id"] = UIDevice.current.identifierForVendor!.uuidString
                    controller.SocialSignUp(socialDict:dictGoogleUser)
                    
                }
            }
        }
    }
    
}
extension LoginTableViewController{
    
    
    // MARK: - Table view Data source & Delegate
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller:BaseForRegistrationViewController =  self.navigationController?.parent as! BaseForRegistrationViewController
        print(controller)
        controller.animateToOriginal(controller: self)
        self.view.endEditing(true)
    }
    
    
    
    func faceBookSignIn(user:[String:Any]){
        var dictFacebookUser:[String:Any] = [String:Any]()
        dictFacebookUser = user
        var dictRequest:[String:Any] = [String:Any]()
        dictRequest["uid"] = user["uid"]
        dictRequest["device_type"] = Constants.apiConstants.deviceType
        dictRequest["device_id"] = UIDevice.current.identifierForVendor!.uuidString
        APIManager.postRequestForURLString(true, Constants.URLConstants.checkUser, [:], dictRequest) { (Done, error, Success, result, message, isActiveSession) in
            DispatchQueue.main.async{
                if Success{
                    
                    let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
                    let tabBarController:SalonTabBarController = story.instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.SalonTabbarController) as! SalonTabBarController
                    AppDelegate.getAppDelegate().window!.rootViewController = tabBarController
                    AppDelegate.getAppDelegate().window?.makeKeyAndVisible()
                }
                else {
                    let controller:BaseForRegistrationViewController = self.navigationController?.parent as! BaseForRegistrationViewController
                    dictFacebookUser["uid"] = user["uid"]
                    dictFacebookUser["device_type"] = Constants.apiConstants.deviceType
                    dictFacebookUser["device_id"] = UIDevice.current.identifierForVendor!.uuidString
                    controller.SocialSignUp(socialDict:dictFacebookUser)
                    
                }
            }
        }
    }
}

