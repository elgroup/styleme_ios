//
//  AppDelegate.swift
//  StyleMe
//
//  Created by Admin on 19/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import  GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleMaps
import GooglePlaces
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var isCountryList = false
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GIDSignIn.sharedInstance().clientID = "176593953124-vrhh3qfub9dva5ckqld21qvru18uli43.apps.googleusercontent.com"
        //PictiumKey
        //AIzaSyDiuSGYNI2AXmL77lPjTpfvDakQYrgHQ6s
        //StyleUpKey
        //AIzaSyBvxPNaAyoeGfhhbiU53riAQZQjR5OME8o
        GMSServices.provideAPIKey("AIzaSyCZxMQj4vJcai4JlpSBHhfL22E8vmw1I4Y")
        GMSPlacesClient.provideAPIKey("AIzaSyCZxMQj4vJcai4JlpSBHhfL22E8vmw1I4Y")
        let defaults:UserDefaults = UserDefaults.standard
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        let isLogin:Bool = defaults.bool(forKey: Constants.Login.isLogin)
        if isLogin{
            let story = UIStoryboard.init(name: Constants.UIViewControllerIdentifier.SalonTabControllerStoryBoard, bundle: nil)
            let tabBarController:SalonTabBarController = story.instantiateViewController(withIdentifier:Constants.UIViewControllerIdentifier.SalonTabbarController ) as! SalonTabBarController
            self.window!.rootViewController = tabBarController
            self.window?.makeKeyAndVisible()
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    class func getAppDelegate() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    func application(_ application: UIApplication,
                     open url: URL, options: [UIApplication.OpenURLOptionsKey : Any],sourceApplication: String?, annotation: Any) -> Bool {
        let appId: String = "925320677852175"//Settings.appId
        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" {
            return ApplicationDelegate.shared.application(application, open: url, options: options)
        }
        else{
            return GIDSignIn.sharedInstance().handle(url)
        }
    }
    
    
    //    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
    //        let appId: String = Settings.appId
    //        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" {
    //            return ApplicationDelegate.shared.application(app, open: url, options: options)
    //        }
    //        return false
    //    }
    //    - (BOOL)application:(UIApplication *)application
    //    openURL:(NSURL *)url
    //    sourceApplication:(NSString *)sourceApplication
    //    annotation:(id)annotation {
    //
    //    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
    //    openURL:url
    //    sourceApplication:sourceApplication
    //    annotation:annotation
    //    ];
    //    // Add any custom logic here.
    //    return handled;
    //    }
    
    
}

