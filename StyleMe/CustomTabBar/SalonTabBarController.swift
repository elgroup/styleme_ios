//
//  SalonTabBarController.swift
//  StyleMe
//
//  Created by EL INFO on 13/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SalonTabBarController: UITabBarController {
    
    @IBOutlet weak var buttonProfile: UIButton!
    @IBOutlet weak var buttonOffer: UIButton!
    @IBOutlet weak var buttonAppointment: UIButton!
    @IBOutlet weak var buttonSearch: UIButton!
    @IBOutlet weak var buttonHome: UIButton!
    @IBOutlet var viewBaseCustomTabbar: UIView!
    var selectedButton:UIButton!
    var arrButtonImages:[Any]!
    var buttonFloating:UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let buttonDefaultImage = ["home","search","appoint","offer","Profile"]
        let buttonSelectedImage = ["home_selected","search_selected","appoint_selected","offer_selected","Profile_selected"]
        self.arrButtonImages = [Any]()
        for n in 0..<5 {
            var dict:[String:Any] = [String:Any]()
            dict["default"] = buttonDefaultImage[n]
            dict["selected"] = buttonSelectedImage[n]
            arrButtonImages.append(dict)
        }
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Salon", bundle: nil)
        let homeController : SalonHomeViewController = mainStoryboard.instantiateViewController(withIdentifier:Constants.UIViewControllerIdentifier.SalonHomeViewController ) as! SalonHomeViewController
        
        let searchController : SearchViewController = mainStoryboard.instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.SearchViewController) as! SearchViewController
        let bookingView : BookingViewController = mainStoryboard.instantiateViewController(withIdentifier: "BookingViewController") as! BookingViewController
        let offerController:OfferViewController = mainStoryboard.instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.OfferViewController) as! OfferViewController
        let profileController:ProfileViewController = mainStoryboard.instantiateViewController(withIdentifier: Constants.UIViewControllerIdentifier.ProfileViewController) as! ProfileViewController
        let viewControllers: NSMutableArray  =  [homeController,searchController,bookingView,offerController,profileController]
        self.viewControllers = viewControllers.map { UINavigationController(rootViewController: $0 as! UIViewController)}
        self.selectedIndex = 0
        self.buttonHome.setImage(UIImage.init(named: "home_selected"), for: .normal)
        self.selectedButton = buttonHome
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBar.backgroundColor = UIColor.clear
        self.viewBaseCustomTabbar.backgroundColor = .black
        self.viewBaseCustomTabbar.frame = self.tabBar.bounds
        self.tabBar.isUserInteractionEnabled = false
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBar.backgroundColor = UIColor.clear
        self.viewBaseCustomTabbar.backgroundColor = .black
        self.viewBaseCustomTabbar.frame = CGRect.init(x: 0, y: self.tabBar.frame.origin.y , width: self.tabBar.frame.width  , height: self.tabBar.frame.height)
        self.view.addSubview(viewBaseCustomTabbar)
        self.view.bringSubviewToFront(viewBaseCustomTabbar)
        self.buttonFloating = UIButton.init()
        self.buttonFloating.frame = CGRect.init(x: 10, y: self.tabBar.frame.origin.y - 41.5, width: 105, height: 83)
        self.buttonFloating.setImage(UIImage.init(named: "logo for post"), for: .normal)
        self.buttonFloating.addTarget(self, action: #selector(buttonPostPressed), for: .touchUpInside)
        self.view.addSubview(buttonFloating)
        
    }
    
    @IBAction func buttonHomePressed(_ sender: Any) {
        let button:UIButton = sender as! UIButton
        if self.selectedIndex != button.tag{
        self.selectedIndex = button.tag
        let dictSelected:[String:Any] = arrButtonImages[selectedIndex] as! [String : Any]
        let dictDefault:[String:Any] = arrButtonImages[selectedButton.tag] as! [String:Any]
        button.setImage(UIImage.init(named: dictSelected["selected"] as! String), for: .normal)
        self.selectedButton.setImage(UIImage.init(named: dictDefault["default"] as! String), for: .normal)
        self.selectedButton = button
        }
        
    }
    
    func hideCustomTabBar(){
        UIView.animate(withDuration: 0.3, animations: {
            self.buttonFloating.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.size.height, width: self.buttonFloating.frame.width, height: self.buttonFloating.frame.height)
            self.viewBaseCustomTabbar.frame = CGRect.init(x: 0, y: UIScreen.main.bounds.size.height, width: self.tabBar.frame.size.width, height: self.tabBar.frame.size.height)
            self.tabBar.isHidden = true
            
        }) { (success) in
            self.viewBaseCustomTabbar.isHidden = true
            self.buttonFloating.isHidden = true
        }
        
    }
    func showCustomTabBar(){
        UIView.animate(withDuration: 0.3, animations: {
            self.viewBaseCustomTabbar.isHidden = false
            self.buttonFloating.isHidden = false
            self.viewBaseCustomTabbar.frame = CGRect.init(x: 0, y: self.tabBar.frame.origin.y, width: self.tabBar.frame.size.width, height: self.tabBar.frame.size.height)
            self.buttonFloating.frame = CGRect.init(x: 10, y: self.tabBar.frame.origin.y - 41.5, width: 105, height: 83)
            
        }) { (success) in
            self.tabBar.isHidden = false

        }
    }
    
    @objc func buttonPostPressed(){
        var mainView: UIStoryboard!
        mainView = UIStoryboard.init(name: "Post", bundle: nil)
        let tabBarController:PostTabBarController = mainView.instantiateViewController(withIdentifier: "PostTabBarController") as! PostTabBarController
        AppDelegate.getAppDelegate().window!.rootViewController = tabBarController
    }
}

