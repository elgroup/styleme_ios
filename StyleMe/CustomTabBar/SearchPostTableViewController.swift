//
//  SearchPostTableViewController.swift
//  StyleMe
//
//  Created by EL INFO on 26/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SearchPostTableViewController: UITableViewController {
    var containerView:UIView!
    @IBOutlet weak var viewContainerSearch:UIView!
    @IBOutlet weak var containerNavigationBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customNavigationBar()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        containerView.constraints.forEach { $0.isActive = false }
        containerView.removeFromSuperview()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
    
    
}

extension SearchPostTableViewController{
    func customNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationItem.hidesBackButton = true
        
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        backButton.setImage(UIImage.init(named: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(NewPostViewController.backButtonPressed(_:)), for: .touchUpInside)
        let leftItem = UIBarButtonItem()
        leftItem.customView = backButton
        self.navigationController?.navigationItem.leftBarButtonItem = leftItem
        
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.navigationController?.navigationBar.barTintColor = .white
        
        containerView = UIView.init(frame: CGRect.init(x: 0, y: UIApplication.shared.statusBarFrame.height, width: UIScreen.main.bounds.size.width, height: (self.navigationController?.navigationBar.frame.height)!))
        self.navigationController?.view.addSubview(containerView)
        containerNavigationBar.frame = containerView.bounds
        containerView.addSubview(containerNavigationBar)
        viewContainerSearch.layer.cornerRadius = self.viewContainerSearch.frame.size.height/2
        containerView.clipsToBounds = true
        
    }
    
    @IBAction func backButtonPressed(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
}
